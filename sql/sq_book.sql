/*
Navicat MySQL Data Transfer

Source Server         : MySql57
Source Server Version : 50727
Source Host           : localhost:3306
Source Database       : sq_book

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2022-10-30 17:07:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for asq_applaud
-- ----------------------------
DROP TABLE IF EXISTS `asq_applaud`;
CREATE TABLE `asq_applaud` (
  `applaud_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL COMMENT '用户',
  `book_id` int(20) DEFAULT NULL COMMENT '书籍',
  `cread_data` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`applaud_id`)
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8 COMMENT='用户喜欢文章列表';

-- ----------------------------
-- Records of asq_applaud
-- ----------------------------
INSERT INTO `asq_applaud` VALUES ('44', '1', '5', '2022-10-29 17:36:27');
INSERT INTO `asq_applaud` VALUES ('48', '1', '6', '2022-10-29 18:07:18');

-- ----------------------------
-- Table structure for asq_books
-- ----------------------------
DROP TABLE IF EXISTS `asq_books`;
CREATE TABLE `asq_books` (
  `book_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `book_writer_name` varchar(20) DEFAULT NULL COMMENT '书籍作者姓名',
  `book_name` varchar(20) DEFAULT NULL COMMENT '书籍名',
  `book_type` varchar(20) DEFAULT NULL COMMENT '书籍类型',
  `book_pub_date` datetime DEFAULT NULL COMMENT '出版日期',
  `book_up_date` datetime DEFAULT NULL COMMENT '上传日期',
  `book_press` varchar(20) DEFAULT NULL COMMENT '出版社',
  `book_cover` varchar(255) DEFAULT NULL COMMENT '书籍封面',
  `book_pdf_url` varchar(255) DEFAULT NULL COMMENT '书籍pdf',
  `book_audit` int(6) DEFAULT '0' COMMENT '0 待审核 1 审核成功 2 审核失败',
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COMMENT='书籍';

-- ----------------------------
-- Records of asq_books
-- ----------------------------
INSERT INTO `asq_books` VALUES ('1', 'liefox', '前后端分离', 'Java', '2022-03-22 00:00:00', '2022-03-22 00:00:00', '拾柒工作室', '/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg', '/profile/upload/2022/05/02/3c84954e-fcde-49af-ba28-338bdac2c5bb.pdf', '1');
INSERT INTO `asq_books` VALUES ('2', 'liefox', 'JAVA修炼手册', 'Java', '2022-07-29 00:00:00', '2022-07-26 00:00:00', '人民出版社', '/profile/upload/2022/07/26/b96ee2d7-500d-4ae9-b7f0-5aa470fca2cc.png', '/profile/upload/2022/07/26/386fb641-ca7f-45da-9b58-aeb62fc5bebc.pdf', '1');
INSERT INTO `asq_books` VALUES ('3', 'liefox', '让富婆爱上你', '生活', '2022-07-30 00:00:00', '2022-07-28 00:00:00', 'XX出版社', '/profile/upload/2022/07/28/5bec2273-b097-413a-bcc1-aebb7da06244.jpg', '/profile/upload/2022/07/28/3cc0a4f9-3eda-4d96-9caf-955e6722cc0c.pdf', '1');
INSERT INTO `asq_books` VALUES ('4', '拾柒图书1', '妇女之友', '生活', '2022-07-17 00:00:00', '2022-07-28 00:00:00', 'XX出版社', '/profile/upload/2022/07/28/e6a09893-b636-4cc0-9f98-60d56178710b.jpg', '/profile/upload/2022/07/28/50a27028-7de2-4126-baef-18ed02e35e62.pdf', '1');
INSERT INTO `asq_books` VALUES ('5', '3', '鱼道', '生活', '2022-07-28 00:00:00', '2022-07-29 00:00:00', 'XX出版社', '/profile/upload/2022/07/28/a864307c-c466-4fe4-8a13-cc1c846fb4be.jpg', '/profile/upload/2022/07/28/c6767c5e-304e-40a3-84fd-0d92bd1997bf.pdf', '1');
INSERT INTO `asq_books` VALUES ('6', '3', '萨克的数据', 'Java', '2022-07-27 00:00:00', '2022-06-30 00:00:00', 'XX出版社', '/profile/upload/2022/07/28/cc04b4bc-1929-4623-940c-05ab8972a2fa.jpg', '/profile/upload/2022/07/28/946612d8-7152-4b10-9f11-805cf474e6bb.pdf', '1');
INSERT INTO `asq_books` VALUES ('7', 'liefox', '数据结构', 'Java', '2022-03-22 00:00:00', '2022-03-22 00:00:00', '拾柒工作室', '/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg', '/profile/upload/2022/05/02/3c84954e-fcde-49af-ba28-338bdac2c5bb.pdf', '1');
INSERT INTO `asq_books` VALUES ('8', 'liefox', 'JAVA算法', 'Java', '2022-07-29 00:00:00', '2022-07-26 00:00:00', '人民出版社', '/profile/upload/2022/07/26/b96ee2d7-500d-4ae9-b7f0-5aa470fca2cc.png', '/profile/upload/2022/07/26/386fb641-ca7f-45da-9b58-aeb62fc5bebc.pdf', '1');
INSERT INTO `asq_books` VALUES ('9', 'liefox', '程序员软实力', '生活', '2022-07-30 00:00:00', '2022-07-28 00:00:00', 'XX出版社', '/profile/upload/2022/07/28/5bec2273-b097-413a-bcc1-aebb7da06244.jpg', '/profile/upload/2022/07/28/3cc0a4f9-3eda-4d96-9caf-955e6722cc0c.pdf', '1');
INSERT INTO `asq_books` VALUES ('10', '拾柒图书1', '程序员之友', '生活', '2022-07-17 00:00:00', '2022-07-28 00:00:00', 'XX出版社', '/profile/upload/2022/07/28/e6a09893-b636-4cc0-9f98-60d56178710b.jpg', '/profile/upload/2022/07/28/50a27028-7de2-4126-baef-18ed02e35e62.pdf', '1');
INSERT INTO `asq_books` VALUES ('11', '3', '布道者', '生活', '2022-07-28 00:00:00', '2022-07-29 00:00:00', 'XX出版社', '/profile/upload/2022/07/28/a864307c-c466-4fe4-8a13-cc1c846fb4be.jpg', '/profile/upload/2022/07/28/c6767c5e-304e-40a3-84fd-0d92bd1997bf.pdf', '1');
INSERT INTO `asq_books` VALUES ('12', '3', 'springboot', 'Java', '2022-07-27 00:00:00', '2022-06-30 00:00:00', 'XX出版社', '/profile/upload/2022/07/28/cc04b4bc-1929-4623-940c-05ab8972a2fa.jpg', '/profile/upload/2022/07/28/946612d8-7152-4b10-9f11-805cf474e6bb.pdf', '1');

-- ----------------------------
-- Table structure for asq_collect
-- ----------------------------
DROP TABLE IF EXISTS `asq_collect`;
CREATE TABLE `asq_collect` (
  `collect_id` int(20) NOT NULL AUTO_INCREMENT,
  `book_id` int(20) DEFAULT NULL COMMENT '书籍id',
  `user_id` int(20) DEFAULT NULL COMMENT '用户id',
  `create_date` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`collect_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='加入书架';

-- ----------------------------
-- Records of asq_collect
-- ----------------------------
INSERT INTO `asq_collect` VALUES ('11', '2', '1', '2022-10-27 18:27:18');

-- ----------------------------
-- Table structure for asq_comment
-- ----------------------------
DROP TABLE IF EXISTS `asq_comment`;
CREATE TABLE `asq_comment` (
  `comment_id` int(20) NOT NULL AUTO_INCREMENT,
  `comment_parent_id` int(20) DEFAULT '0' COMMENT '父级id',
  `user_id` int(20) DEFAULT NULL,
  `com_body_id` int(20) DEFAULT NULL COMMENT '评论体',
  `comment_text` varchar(255) DEFAULT NULL COMMENT '评论内容',
  `com_body_type` int(1) DEFAULT NULL COMMENT '评论主体类型，1书，2社区',
  `create_date` datetime DEFAULT NULL,
  `like_num` int(20) DEFAULT '0' COMMENT '点赞数',
  `is_vio` int(1) DEFAULT '1' COMMENT '是否违规',
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='评论表';

-- ----------------------------
-- Records of asq_comment
-- ----------------------------
INSERT INTO `asq_comment` VALUES ('32', '0', '1', '2', '0', '1', '2022-10-28 21:52:17', '1', '1');
INSERT INTO `asq_comment` VALUES ('33', '32', '1', '2', '1', '1', '2022-10-28 21:52:21', '2', '1');
INSERT INTO `asq_comment` VALUES ('34', '33', '1', '2', '2', '1', '2022-10-28 21:52:25', '1', '1');
INSERT INTO `asq_comment` VALUES ('35', '34', '1', '2', '3', '1', '2022-10-28 21:52:30', '0', '1');
INSERT INTO `asq_comment` VALUES ('36', '33', '1', '2', '1-0', '1', '2022-10-28 21:52:44', '0', '1');
INSERT INTO `asq_comment` VALUES ('37', '36', '1', '2', '1-1', '1', '2022-10-28 21:53:03', '0', '0');
INSERT INTO `asq_comment` VALUES ('38', '32', '1', '2', '11111', '1', '2022-10-28 22:05:19', '0', '1');
INSERT INTO `asq_comment` VALUES ('39', '0', '1', '2', '123', '1', '2022-10-28 22:29:43', '0', '1');
INSERT INTO `asq_comment` VALUES ('40', '0', '1', '2', '123', '1', '2022-10-28 22:29:45', '0', '1');
INSERT INTO `asq_comment` VALUES ('41', '0', '1', '2', '123', '1', '2022-10-28 22:29:46', '0', '1');
INSERT INTO `asq_comment` VALUES ('42', '0', '1', '2', '123', '1', '2022-10-28 22:29:46', '0', '1');
INSERT INTO `asq_comment` VALUES ('43', '0', '1', '2', '123', '1', '2022-10-28 22:29:46', '0', '1');
INSERT INTO `asq_comment` VALUES ('44', '0', '1', '2', '123', '1', '2022-10-28 22:29:46', '0', '1');
INSERT INTO `asq_comment` VALUES ('45', '0', '1', '2', '123', '1', '2022-10-28 22:29:46', '0', '1');
INSERT INTO `asq_comment` VALUES ('46', '0', '1', '2', '123', '1', '2022-10-28 22:29:47', '0', '1');
INSERT INTO `asq_comment` VALUES ('47', '0', '1', '2', '123', '1', '2022-10-28 22:29:47', '0', '1');
INSERT INTO `asq_comment` VALUES ('48', '0', '1', '7', '1', '1', '2022-10-29 15:49:33', '0', '1');
INSERT INTO `asq_comment` VALUES ('49', '48', '3', '7', '123', '1', '2022-10-29 16:06:30', '0', '1');
INSERT INTO `asq_comment` VALUES ('50', '49', '3', '7', '11', '1', '2022-10-29 16:06:34', '0', '1');
INSERT INTO `asq_comment` VALUES ('51', '0', '1', '1', '1', '2', '2022-10-29 23:48:54', '0', '1');
INSERT INTO `asq_comment` VALUES ('52', '51', '1', '1', '阿达', '2', '2022-10-29 23:49:40', '0', '1');
INSERT INTO `asq_comment` VALUES ('53', '0', '1', '1', '阿斯达', '2', '2022-10-29 23:57:17', '0', '1');
INSERT INTO `asq_comment` VALUES ('54', '0', '1', '2', '1', '2', '2022-10-30 00:01:23', '0', '1');
INSERT INTO `asq_comment` VALUES ('55', '0', '1', '1', '12', '2', '2022-10-30 12:31:36', '0', '1');
INSERT INTO `asq_comment` VALUES ('56', '0', '1', '1', '1', '2', '2022-10-30 16:57:45', '0', '1');

-- ----------------------------
-- Table structure for asq_ppt
-- ----------------------------
DROP TABLE IF EXISTS `asq_ppt`;
CREATE TABLE `asq_ppt` (
  `ppt_id` int(20) NOT NULL AUTO_INCREMENT,
  `ppt_name` varchar(50) DEFAULT NULL COMMENT '幻灯片名称',
  `ppt_pic` varchar(255) DEFAULT NULL COMMENT '图片',
  `ppt_url` varchar(255) DEFAULT NULL COMMENT '跳转路径',
  `ppt_desc` int(20) DEFAULT NULL COMMENT '排序',
  `ppt_type` int(20) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`ppt_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='首页幻灯片广告位';

-- ----------------------------
-- Records of asq_ppt
-- ----------------------------
INSERT INTO `asq_ppt` VALUES ('1', '梅立国宇航员的一天', '/profile/upload/2022/07/24/7235bfa2-d507-4f76-8ccc-fc569c1d3155.jpg', 'https://www.zhihu.com/', '1', '3');
INSERT INTO `asq_ppt` VALUES ('2', '著名作者哈登新书热卖', '/profile/upload/2022/07/24/e3b491f1-9fa8-4c57-9208-928d51e40b38.jpeg', '1', '2', '1');
INSERT INTO `asq_ppt` VALUES ('3', '空间站的文学——知乎文章', '/profile/upload/2022/07/24/beecd828-5063-4252-9b4a-b4970b516cdf.jpg', 'https://www.zhihu.com/question/543901181', '3', '2');

-- ----------------------------
-- Table structure for asq_rank_parm
-- ----------------------------
DROP TABLE IF EXISTS `asq_rank_parm`;
CREATE TABLE `asq_rank_parm` (
  `rank_parm_id` int(20) NOT NULL AUTO_INCREMENT,
  `book_id` int(20) DEFAULT NULL COMMENT '书籍id',
  `read_num` int(255) DEFAULT '0' COMMENT '阅读数',
  `applaud_num` int(255) DEFAULT '0' COMMENT '赞同数',
  `comment_num` int(255) DEFAULT '0' COMMENT '评论数',
  `collect_num` int(255) DEFAULT '0' COMMENT '加入书架数',
  PRIMARY KEY (`rank_parm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8 COMMENT='排名参数';

-- ----------------------------
-- Records of asq_rank_parm
-- ----------------------------
INSERT INTO `asq_rank_parm` VALUES ('11', '4', '6', '0', '0', '0');
INSERT INTO `asq_rank_parm` VALUES ('12', '1', '8', '0', '0', '0');
INSERT INTO `asq_rank_parm` VALUES ('13', '2', '1', '0', '10', '1');
INSERT INTO `asq_rank_parm` VALUES ('14', '3', '1', '0', '0', '0');
INSERT INTO `asq_rank_parm` VALUES ('21', '7', '0', '0', '3', '0');
INSERT INTO `asq_rank_parm` VALUES ('22', '5', '0', '1', '0', '0');
INSERT INTO `asq_rank_parm` VALUES ('23', '6', '0', '1', '0', '0');

-- ----------------------------
-- Table structure for asq_topic
-- ----------------------------
DROP TABLE IF EXISTS `asq_topic`;
CREATE TABLE `asq_topic` (
  `topic_id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) DEFAULT NULL,
  `topic_name` varchar(255) DEFAULT NULL COMMENT '话题',
  `topic_content` text COMMENT '文章',
  `topic_cover` varchar(255) DEFAULT NULL COMMENT '封面',
  `topic_createdate` datetime DEFAULT NULL COMMENT '发布时间',
  PRIMARY KEY (`topic_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='话题表';

-- ----------------------------
-- Records of asq_topic
-- ----------------------------
INSERT INTO `asq_topic` VALUES ('1', '1', '那一本数据结构的数好？', '<p>不知道提问者现在是具体是什么基础，不过没关系，正好回答的全一点，方便浏览到这个问题的其他人。</p><p>有的同学还在读大学，代码还没写过几行；有的同学已经工作数十年，这之间的差别还是挺大的。而不同基础的人，适宜看的书是完全不一样的。因此，针对<strong>不同层次、不同语言</strong>的同学，分别推荐了不同的书。</p><p>不管是不是科班出身，每一个程序员都应该花时间了解和学习计算机科学相关的基础知识，因为<strong>所有关于如何编程的底层逻辑和原理都在那里了</strong>。</p><p>计算机基础知识汇总了包括<strong>数据结构与算法、数学、操作系统、计算机组成原理、</strong><a href=\"https://www.zhihu.com/search?q=%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>计算机网络</strong></a><strong>、</strong><a href=\"https://www.zhihu.com/search?q=%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>软件工程</strong></a><strong>在内的常用计算机科学知识</strong>，以帮你快速建立对计算机科学的大局观。</p><p>有很多同学问到，这些基础到底该怎么补充，顺序是怎样的，有什么资料，我索性抽时间把我这些年压箱底的学习资料倒腾了一下，精选了几本我认为最优质的，<strong>学习资料在于精，不在于多，多反而不是好事，最为一名程序员，大家的学习时间都太宝贵了，我们要用有限的时间学习最有价值的内容</strong>，具体内容可以这篇资料贴。</p><p class=\"ql-indent-1 ql-align-center\"><span style=\"background-color: transparent;\"><img src=\"https://pic2.zhimg.com/80/v2-cee3efd664ce1503a914bb103dd44317_720w.webp\" height=\"528\" width=\"720\"></span></p><h2>针对入门的趣味书</h2><p>入门的同学，我建议你不要过度追求上去就看经典书。像《算法导论》《算法》这些书，虽然比较经典、比较权威，但是非常厚。初学就去啃这些书肯定会很费劲。而一旦啃不下来，挫败感就会很强。所以，入门的同学，我建议你找一些比较容易看的书来看，比如《大话数据结构》和《<a href=\"https://www.zhihu.com/search?q=%E7%AE%97%E6%B3%95%E5%9B%BE%E8%A7%A3&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">算法图解</a>》。你不要太在意书写得深浅，重要的是能不能坚持看完。</p><p>《大话数据结构》这本书最大的特点是，它把理论讲得很有趣，不枯燥。而且每个数据结构和算法，作者都结合生活中的例子进行了讲解， 能让你有非常直观的感受。虽然这本书有400多页，但是花两天时间读完，应该是没问题的。如果你之前完全不懂数据结构和算法，可以先从这本书看起。</p><p>《算法图解》跟《大话数据结构》走的是同样的路线，就像这本书副标题写的那样，“像小说一样有趣的算法入门书”，主打“图解”，通俗易懂。它只有不到200页，所以内容比较少。作为入门，看看这本书，能让你对数据结构和算法有个大概的认识。</p><p>这些入门书共同的问题是，缺少细节，不够系统，也不够严谨。所以，如果你想要系统地学数据结构和算法，看这两本书肯定是不够的。</p><p><br></p><h2>针对特定编程语言的教科书</h2><p>讲数据结构和算法，肯定会跟代码实现挂钩。所以，很多人就很关心，某某书籍是用什么语言实现的，是不是自己熟悉的语言。</p><p>市面大部分数据结构和算法书籍都是用C、C++、Java语言实现的，还有些是用伪代码。而使用Python、Go、PHP、JavaScript、Objective-C这些编程语言实现的就更少了。</p><p>我这里推荐《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E5%92%8C%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构和算法分析</a>》。国内外很多大学都拿这本书当作教材。这本书非常系统、全面、严谨，而且又不是特别难，适合对数据结构和算法有些了解，并且掌握了至少一门编程语言的同学。而且，这个作者也很用心。他用了三种语言，写了三个版本，分别是：《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E4%B8%8E%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构与算法分析</a>&nbsp;: C语言描述》《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E4%B8%8E%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90+%3A+C%2B%2B%E6%8F%8F%E8%BF%B0&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构与算法分析 : C++描述</a>》《数据结构与算法分析 : Java语言描述》。</p><p>如果你熟悉的是Python或者JavaScript，可以参考《数据结构与算法JavaScript描述》《数据结构与算法：Python语言描述》。至于其他语言的算法书籍，确实比较少。如果你有推荐，可以在留言区补充一下。</p><p><br></p><h2>面试必刷的宝典</h2><p>算法对面试很重要，很多人也很关心。我这里推荐几本有益于面试的书籍，分别是：《<a href=\"https://www.zhihu.com/search?q=%E5%89%91%E6%8C%87offer&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">剑指offer</a>》《编程珠玑》《编程之美》。</p><p>从《剑指offer》这本书的名字就可以看出，作者的写作目的非常明确，就是为了面试。这本书几乎包含了所有常见的、经典的面试题。如果能搞懂这本书里的内容，应付一般公司的面试应该不成问题。</p><p>《编程珠玑》这本书的豆瓣评分非常高，有9分。这本书最大的特色就是讲了很多针对海量数据的处理技巧。这个可能是其他算法书籍很少涉及的。面试的时候，海量数据处理的问题也是经常会问的，特别是校招面试。不管是开拓眼界，还是应付面试，这本书都很值得一看。</p><p><br></p><p><br></p><p>《编程之美》这本书有多位作者，其中绝大部分是微软的工程师，所以书的质量很有保证。不过，这里面的算法题目稍微有点难，也不是很系统，这也是我把它归到面试这一部分的原因。如果你有一定基础，也喜欢钻研些算法问题，或者要面试Google、Facebook这样的公司，可以拿这本书里的题，先来自测一下。</p>', null, '2022-10-29 00:00:00');
INSERT INTO `asq_topic` VALUES ('2', '1', 'kfc疯狂星期四怎么越来越拉跨了？', '<p><span class=\"ql-size-huge\" style=\"color: rgb(230, 0, 0);\">KFC，你拿什么和麦当劳比？</span></p><p class=\"ql-align-center\"><span class=\"ql-size-small\"><img src=\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\" width=\"480\"></span></p>', '/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg', '2022-10-29 00:00:00');
INSERT INTO `asq_topic` VALUES ('3', '1', '那一本数据结构的数好？', '<p>不知道提问者现在是具体是什么基础，不过没关系，正好回答的全一点，方便浏览到这个问题的其他人。</p><p>有的同学还在读大学，代码还没写过几行；有的同学已经工作数十年，这之间的差别还是挺大的。而不同基础的人，适宜看的书是完全不一样的。因此，针对<strong>不同层次、不同语言</strong>的同学，分别推荐了不同的书。</p><p>不管是不是科班出身，每一个程序员都应该花时间了解和学习计算机科学相关的基础知识，因为<strong>所有关于如何编程的底层逻辑和原理都在那里了</strong>。</p><p>计算机基础知识汇总了包括<strong>数据结构与算法、数学、操作系统、计算机组成原理、</strong><a href=\"https://www.zhihu.com/search?q=%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>计算机网络</strong></a><strong>、</strong><a href=\"https://www.zhihu.com/search?q=%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>软件工程</strong></a><strong>在内的常用计算机科学知识</strong>，以帮你快速建立对计算机科学的大局观。</p><p>有很多同学问到，这些基础到底该怎么补充，顺序是怎样的，有什么资料，我索性抽时间把我这些年压箱底的学习资料倒腾了一下，精选了几本我认为最优质的，<strong>学习资料在于精，不在于多，多反而不是好事，最为一名程序员，大家的学习时间都太宝贵了，我们要用有限的时间学习最有价值的内容</strong>，具体内容可以这篇资料贴。</p><p class=\"ql-indent-1 ql-align-center\"><span style=\"background-color: transparent;\"><img src=\"https://pic2.zhimg.com/80/v2-cee3efd664ce1503a914bb103dd44317_720w.webp\" height=\"528\" width=\"720\"></span></p><h2>针对入门的趣味书</h2><p>入门的同学，我建议你不要过度追求上去就看经典书。像《算法导论》《算法》这些书，虽然比较经典、比较权威，但是非常厚。初学就去啃这些书肯定会很费劲。而一旦啃不下来，挫败感就会很强。所以，入门的同学，我建议你找一些比较容易看的书来看，比如《大话数据结构》和《<a href=\"https://www.zhihu.com/search?q=%E7%AE%97%E6%B3%95%E5%9B%BE%E8%A7%A3&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">算法图解</a>》。你不要太在意书写得深浅，重要的是能不能坚持看完。</p><p>《大话数据结构》这本书最大的特点是，它把理论讲得很有趣，不枯燥。而且每个数据结构和算法，作者都结合生活中的例子进行了讲解， 能让你有非常直观的感受。虽然这本书有400多页，但是花两天时间读完，应该是没问题的。如果你之前完全不懂数据结构和算法，可以先从这本书看起。</p><p>《算法图解》跟《大话数据结构》走的是同样的路线，就像这本书副标题写的那样，“像小说一样有趣的算法入门书”，主打“图解”，通俗易懂。它只有不到200页，所以内容比较少。作为入门，看看这本书，能让你对数据结构和算法有个大概的认识。</p><p>这些入门书共同的问题是，缺少细节，不够系统，也不够严谨。所以，如果你想要系统地学数据结构和算法，看这两本书肯定是不够的。</p><p><br></p><h2>针对特定编程语言的教科书</h2><p>讲数据结构和算法，肯定会跟代码实现挂钩。所以，很多人就很关心，某某书籍是用什么语言实现的，是不是自己熟悉的语言。</p><p>市面大部分数据结构和算法书籍都是用C、C++、Java语言实现的，还有些是用伪代码。而使用Python、Go、PHP、JavaScript、Objective-C这些编程语言实现的就更少了。</p><p>我这里推荐《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E5%92%8C%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构和算法分析</a>》。国内外很多大学都拿这本书当作教材。这本书非常系统、全面、严谨，而且又不是特别难，适合对数据结构和算法有些了解，并且掌握了至少一门编程语言的同学。而且，这个作者也很用心。他用了三种语言，写了三个版本，分别是：《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E4%B8%8E%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构与算法分析</a>&nbsp;: C语言描述》《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E4%B8%8E%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90+%3A+C%2B%2B%E6%8F%8F%E8%BF%B0&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构与算法分析 : C++描述</a>》《数据结构与算法分析 : Java语言描述》。</p><p>如果你熟悉的是Python或者JavaScript，可以参考《数据结构与算法JavaScript描述》《数据结构与算法：Python语言描述》。至于其他语言的算法书籍，确实比较少。如果你有推荐，可以在留言区补充一下。</p><p><br></p><h2>面试必刷的宝典</h2><p>算法对面试很重要，很多人也很关心。我这里推荐几本有益于面试的书籍，分别是：《<a href=\"https://www.zhihu.com/search?q=%E5%89%91%E6%8C%87offer&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">剑指offer</a>》《编程珠玑》《编程之美》。</p><p>从《剑指offer》这本书的名字就可以看出，作者的写作目的非常明确，就是为了面试。这本书几乎包含了所有常见的、经典的面试题。如果能搞懂这本书里的内容，应付一般公司的面试应该不成问题。</p><p>《编程珠玑》这本书的豆瓣评分非常高，有9分。这本书最大的特色就是讲了很多针对海量数据的处理技巧。这个可能是其他算法书籍很少涉及的。面试的时候，海量数据处理的问题也是经常会问的，特别是校招面试。不管是开拓眼界，还是应付面试，这本书都很值得一看。</p><p><br></p><p><br></p><p>《编程之美》这本书有多位作者，其中绝大部分是微软的工程师，所以书的质量很有保证。不过，这里面的算法题目稍微有点难，也不是很系统，这也是我把它归到面试这一部分的原因。如果你有一定基础，也喜欢钻研些算法问题，或者要面试Google、Facebook这样的公司，可以拿这本书里的题，先来自测一下。</p>', null, '2022-10-29 00:00:00');
INSERT INTO `asq_topic` VALUES ('4', '1', 'kfc疯狂星期四怎么越来越拉跨了？', '<p><span class=\"ql-size-huge\" style=\"color: rgb(230, 0, 0);\">KFC，你拿什么和麦当劳比？</span></p><p class=\"ql-align-center\"><span class=\"ql-size-small\"><img src=\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\" width=\"480\"></span></p>', '/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg', '2022-10-29 00:00:00');
INSERT INTO `asq_topic` VALUES ('5', '1', '那一本数据结构的数好？', '<p>不知道提问者现在是具体是什么基础，不过没关系，正好回答的全一点，方便浏览到这个问题的其他人。</p><p>有的同学还在读大学，代码还没写过几行；有的同学已经工作数十年，这之间的差别还是挺大的。而不同基础的人，适宜看的书是完全不一样的。因此，针对<strong>不同层次、不同语言</strong>的同学，分别推荐了不同的书。</p><p>不管是不是科班出身，每一个程序员都应该花时间了解和学习计算机科学相关的基础知识，因为<strong>所有关于如何编程的底层逻辑和原理都在那里了</strong>。</p><p>计算机基础知识汇总了包括<strong>数据结构与算法、数学、操作系统、计算机组成原理、</strong><a href=\"https://www.zhihu.com/search?q=%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>计算机网络</strong></a><strong>、</strong><a href=\"https://www.zhihu.com/search?q=%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>软件工程</strong></a><strong>在内的常用计算机科学知识</strong>，以帮你快速建立对计算机科学的大局观。</p><p>有很多同学问到，这些基础到底该怎么补充，顺序是怎样的，有什么资料，我索性抽时间把我这些年压箱底的学习资料倒腾了一下，精选了几本我认为最优质的，<strong>学习资料在于精，不在于多，多反而不是好事，最为一名程序员，大家的学习时间都太宝贵了，我们要用有限的时间学习最有价值的内容</strong>，具体内容可以这篇资料贴。</p><p class=\"ql-indent-1 ql-align-center\"><span style=\"background-color: transparent;\"><img src=\"https://pic2.zhimg.com/80/v2-cee3efd664ce1503a914bb103dd44317_720w.webp\" height=\"528\" width=\"720\"></span></p><h2>针对入门的趣味书</h2><p>入门的同学，我建议你不要过度追求上去就看经典书。像《算法导论》《算法》这些书，虽然比较经典、比较权威，但是非常厚。初学就去啃这些书肯定会很费劲。而一旦啃不下来，挫败感就会很强。所以，入门的同学，我建议你找一些比较容易看的书来看，比如《大话数据结构》和《<a href=\"https://www.zhihu.com/search?q=%E7%AE%97%E6%B3%95%E5%9B%BE%E8%A7%A3&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">算法图解</a>》。你不要太在意书写得深浅，重要的是能不能坚持看完。</p><p>《大话数据结构》这本书最大的特点是，它把理论讲得很有趣，不枯燥。而且每个数据结构和算法，作者都结合生活中的例子进行了讲解， 能让你有非常直观的感受。虽然这本书有400多页，但是花两天时间读完，应该是没问题的。如果你之前完全不懂数据结构和算法，可以先从这本书看起。</p><p>《算法图解》跟《大话数据结构》走的是同样的路线，就像这本书副标题写的那样，“像小说一样有趣的算法入门书”，主打“图解”，通俗易懂。它只有不到200页，所以内容比较少。作为入门，看看这本书，能让你对数据结构和算法有个大概的认识。</p><p>这些入门书共同的问题是，缺少细节，不够系统，也不够严谨。所以，如果你想要系统地学数据结构和算法，看这两本书肯定是不够的。</p><p><br></p><h2>针对特定编程语言的教科书</h2><p>讲数据结构和算法，肯定会跟代码实现挂钩。所以，很多人就很关心，某某书籍是用什么语言实现的，是不是自己熟悉的语言。</p><p>市面大部分数据结构和算法书籍都是用C、C++、Java语言实现的，还有些是用伪代码。而使用Python、Go、PHP、JavaScript、Objective-C这些编程语言实现的就更少了。</p><p>我这里推荐《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E5%92%8C%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构和算法分析</a>》。国内外很多大学都拿这本书当作教材。这本书非常系统、全面、严谨，而且又不是特别难，适合对数据结构和算法有些了解，并且掌握了至少一门编程语言的同学。而且，这个作者也很用心。他用了三种语言，写了三个版本，分别是：《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E4%B8%8E%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构与算法分析</a>&nbsp;: C语言描述》《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E4%B8%8E%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90+%3A+C%2B%2B%E6%8F%8F%E8%BF%B0&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构与算法分析 : C++描述</a>》《数据结构与算法分析 : Java语言描述》。</p><p>如果你熟悉的是Python或者JavaScript，可以参考《数据结构与算法JavaScript描述》《数据结构与算法：Python语言描述》。至于其他语言的算法书籍，确实比较少。如果你有推荐，可以在留言区补充一下。</p><p><br></p><h2>面试必刷的宝典</h2><p>算法对面试很重要，很多人也很关心。我这里推荐几本有益于面试的书籍，分别是：《<a href=\"https://www.zhihu.com/search?q=%E5%89%91%E6%8C%87offer&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">剑指offer</a>》《编程珠玑》《编程之美》。</p><p>从《剑指offer》这本书的名字就可以看出，作者的写作目的非常明确，就是为了面试。这本书几乎包含了所有常见的、经典的面试题。如果能搞懂这本书里的内容，应付一般公司的面试应该不成问题。</p><p>《编程珠玑》这本书的豆瓣评分非常高，有9分。这本书最大的特色就是讲了很多针对海量数据的处理技巧。这个可能是其他算法书籍很少涉及的。面试的时候，海量数据处理的问题也是经常会问的，特别是校招面试。不管是开拓眼界，还是应付面试，这本书都很值得一看。</p><p><br></p><p><br></p><p>《编程之美》这本书有多位作者，其中绝大部分是微软的工程师，所以书的质量很有保证。不过，这里面的算法题目稍微有点难，也不是很系统，这也是我把它归到面试这一部分的原因。如果你有一定基础，也喜欢钻研些算法问题，或者要面试Google、Facebook这样的公司，可以拿这本书里的题，先来自测一下。</p>', null, '2022-10-29 00:00:00');
INSERT INTO `asq_topic` VALUES ('6', '1', 'kfc疯狂星期四怎么越来越拉跨了？', '<p><span class=\"ql-size-huge\" style=\"color: rgb(230, 0, 0);\">KFC，你拿什么和麦当劳比？</span></p><p class=\"ql-align-center\"><span class=\"ql-size-small\"><img src=\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\" width=\"480\"></span></p>', '/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg', '2022-10-29 00:00:00');
INSERT INTO `asq_topic` VALUES ('7', '1', '那一本数据结构的数好？', '<p>不知道提问者现在是具体是什么基础，不过没关系，正好回答的全一点，方便浏览到这个问题的其他人。</p><p>有的同学还在读大学，代码还没写过几行；有的同学已经工作数十年，这之间的差别还是挺大的。而不同基础的人，适宜看的书是完全不一样的。因此，针对<strong>不同层次、不同语言</strong>的同学，分别推荐了不同的书。</p><p>不管是不是科班出身，每一个程序员都应该花时间了解和学习计算机科学相关的基础知识，因为<strong>所有关于如何编程的底层逻辑和原理都在那里了</strong>。</p><p>计算机基础知识汇总了包括<strong>数据结构与算法、数学、操作系统、计算机组成原理、</strong><a href=\"https://www.zhihu.com/search?q=%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>计算机网络</strong></a><strong>、</strong><a href=\"https://www.zhihu.com/search?q=%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\"><strong>软件工程</strong></a><strong>在内的常用计算机科学知识</strong>，以帮你快速建立对计算机科学的大局观。</p><p>有很多同学问到，这些基础到底该怎么补充，顺序是怎样的，有什么资料，我索性抽时间把我这些年压箱底的学习资料倒腾了一下，精选了几本我认为最优质的，<strong>学习资料在于精，不在于多，多反而不是好事，最为一名程序员，大家的学习时间都太宝贵了，我们要用有限的时间学习最有价值的内容</strong>，具体内容可以这篇资料贴。</p><p class=\"ql-indent-1 ql-align-center\"><span style=\"background-color: transparent;\"><img src=\"https://pic2.zhimg.com/80/v2-cee3efd664ce1503a914bb103dd44317_720w.webp\" height=\"528\" width=\"720\"></span></p><h2>针对入门的趣味书</h2><p>入门的同学，我建议你不要过度追求上去就看经典书。像《算法导论》《算法》这些书，虽然比较经典、比较权威，但是非常厚。初学就去啃这些书肯定会很费劲。而一旦啃不下来，挫败感就会很强。所以，入门的同学，我建议你找一些比较容易看的书来看，比如《大话数据结构》和《<a href=\"https://www.zhihu.com/search?q=%E7%AE%97%E6%B3%95%E5%9B%BE%E8%A7%A3&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">算法图解</a>》。你不要太在意书写得深浅，重要的是能不能坚持看完。</p><p>《大话数据结构》这本书最大的特点是，它把理论讲得很有趣，不枯燥。而且每个数据结构和算法，作者都结合生活中的例子进行了讲解， 能让你有非常直观的感受。虽然这本书有400多页，但是花两天时间读完，应该是没问题的。如果你之前完全不懂数据结构和算法，可以先从这本书看起。</p><p>《算法图解》跟《大话数据结构》走的是同样的路线，就像这本书副标题写的那样，“像小说一样有趣的算法入门书”，主打“图解”，通俗易懂。它只有不到200页，所以内容比较少。作为入门，看看这本书，能让你对数据结构和算法有个大概的认识。</p><p>这些入门书共同的问题是，缺少细节，不够系统，也不够严谨。所以，如果你想要系统地学数据结构和算法，看这两本书肯定是不够的。</p><p><br></p><h2>针对特定编程语言的教科书</h2><p>讲数据结构和算法，肯定会跟代码实现挂钩。所以，很多人就很关心，某某书籍是用什么语言实现的，是不是自己熟悉的语言。</p><p>市面大部分数据结构和算法书籍都是用C、C++、Java语言实现的，还有些是用伪代码。而使用Python、Go、PHP、JavaScript、Objective-C这些编程语言实现的就更少了。</p><p>我这里推荐《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E5%92%8C%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构和算法分析</a>》。国内外很多大学都拿这本书当作教材。这本书非常系统、全面、严谨，而且又不是特别难，适合对数据结构和算法有些了解，并且掌握了至少一门编程语言的同学。而且，这个作者也很用心。他用了三种语言，写了三个版本，分别是：《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E4%B8%8E%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构与算法分析</a>&nbsp;: C语言描述》《<a href=\"https://www.zhihu.com/search?q=%E6%95%B0%E6%8D%AE%E7%BB%93%E6%9E%84%E4%B8%8E%E7%AE%97%E6%B3%95%E5%88%86%E6%9E%90+%3A+C%2B%2B%E6%8F%8F%E8%BF%B0&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">数据结构与算法分析 : C++描述</a>》《数据结构与算法分析 : Java语言描述》。</p><p>如果你熟悉的是Python或者JavaScript，可以参考《数据结构与算法JavaScript描述》《数据结构与算法：Python语言描述》。至于其他语言的算法书籍，确实比较少。如果你有推荐，可以在留言区补充一下。</p><p><br></p><h2>面试必刷的宝典</h2><p>算法对面试很重要，很多人也很关心。我这里推荐几本有益于面试的书籍，分别是：《<a href=\"https://www.zhihu.com/search?q=%E5%89%91%E6%8C%87offer&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(23, 81, 153);\">剑指offer</a>》《编程珠玑》《编程之美》。</p><p>从《剑指offer》这本书的名字就可以看出，作者的写作目的非常明确，就是为了面试。这本书几乎包含了所有常见的、经典的面试题。如果能搞懂这本书里的内容，应付一般公司的面试应该不成问题。</p><p>《编程珠玑》这本书的豆瓣评分非常高，有9分。这本书最大的特色就是讲了很多针对海量数据的处理技巧。这个可能是其他算法书籍很少涉及的。面试的时候，海量数据处理的问题也是经常会问的，特别是校招面试。不管是开拓眼界，还是应付面试，这本书都很值得一看。</p><p><br></p><p><br></p><p>《编程之美》这本书有多位作者，其中绝大部分是微软的工程师，所以书的质量很有保证。不过，这里面的算法题目稍微有点难，也不是很系统，这也是我把它归到面试这一部分的原因。如果你有一定基础，也喜欢钻研些算法问题，或者要面试Google、Facebook这样的公司，可以拿这本书里的题，先来自测一下。</p>', null, '2022-10-29 00:00:00');
INSERT INTO `asq_topic` VALUES ('8', '1', 'kfc疯狂星期四怎么越来越拉跨了？', '<p><span class=\"ql-size-huge\" style=\"color: rgb(230, 0, 0);\">KFC，你拿什么和麦当劳比？</span></p><p class=\"ql-align-center\"><span class=\"ql-size-small\"><img src=\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\" width=\"480\"></span></p>', '/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg', '2022-10-29 00:00:00');
INSERT INTO `asq_topic` VALUES ('10', '3', '阿斯达', '<p>12321321</p>', null, '2022-10-30 15:43:19');

-- ----------------------------
-- Table structure for asq_writer
-- ----------------------------
DROP TABLE IF EXISTS `asq_writer`;
CREATE TABLE `asq_writer` (
  `writer_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `writer_name` varchar(20) DEFAULT NULL COMMENT '作者姓名',
  `writer_info` varchar(255) DEFAULT NULL COMMENT '作者详情',
  `writer_intro` varchar(255) DEFAULT NULL COMMENT '作者座右铭',
  `writer_pic` varchar(255) DEFAULT NULL COMMENT '作者头像',
  PRIMARY KEY (`writer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='作者';

-- ----------------------------
-- Records of asq_writer
-- ----------------------------
INSERT INTO `asq_writer` VALUES ('1', '拾柒图书1', '<p><img src=\"/dev-api/profile/upload/2022/04/25/c716b837-2e1c-4b16-8c67-4510c652111f.jpg\"></p><p>我是拾柒图书1111</p>', '千里马常有，而伯乐。。。。', '/profile/upload/2022/04/25/4124bc61-dfc5-42d3-8bab-c52b3b549966.jpeg');
INSERT INTO `asq_writer` VALUES ('7', '3', null, null, '/profile/upload/2022/05/02/8d4e9a34-ae54-47e4-a564-9c3d27d320f4.jpg');
INSERT INTO `asq_writer` VALUES ('8', 'liefox', '<p>拾柒爱阅读百大创作人</p>', '逝者如斯夫', '/profile/upload/2022/07/26/73e7835a-8f32-4c0f-9e25-154ca69b0f43.jpeg');

-- ----------------------------
-- Table structure for gen_table
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES ('3', 'asq_books', '图书管理', null, null, 'AsqBooks', 'crud', 'com.ruoyi', 'bookM', 'books', '图书管理', 'ruoyi', '0', '/', '{\"parentMenuId\":\"2015\"}', 'admin', '2022-03-22 10:54:19', '', '2022-05-02 17:21:38', null);
INSERT INTO `gen_table` VALUES ('4', 'asq_writer', '作者', null, null, 'AsqWriter', 'crud', 'com.ruoyi', 'writer', 'writer', '作者管理', 'liefox', '0', '/', '{\"parentMenuId\":\"2022\"}', 'admin', '2022-04-25 21:22:42', '', '2022-04-25 21:47:32', null);
INSERT INTO `gen_table` VALUES ('5', 'asq_ppt', '前台首页幻灯片', null, null, 'AsqPpt', 'crud', 'com.ruoyi', 'ppt', 'ppt', '前台首页幻灯片', 'ruoyi', '0', '/', '{\"parentMenuId\":2029}', 'admin', '2022-07-24 18:17:52', '', '2022-07-24 18:25:03', null);
INSERT INTO `gen_table` VALUES ('6', 'asq_rank_parm', '排名参数', null, null, 'AsqRankParm', 'crud', 'com.ruoyi', 'rankParm', 'rankParm', '排名参数', 'ruoyi', '0', '/', '{}', 'admin', '2022-07-28 08:57:08', '', '2022-07-28 09:08:00', null);
INSERT INTO `gen_table` VALUES ('7', 'asq_applaud', '用户喜欢文章列表', null, null, 'AsqApplaud', 'crud', 'com.ruoyi', 'applaud', 'applaud', '用户喜欢', 'zjh', '0', '/', '{}', 'admin', '2022-10-24 21:32:51', '', '2022-10-25 19:27:07', null);
INSERT INTO `gen_table` VALUES ('9', 'asq_collect', '加入书架', null, null, 'AsqCollect', 'crud', 'com.ruoyi', 'collect', 'collect', '加入书架', 'zjh', '0', '/', '{}', 'admin', '2022-10-26 19:28:56', '', '2022-10-26 19:29:31', null);
INSERT INTO `gen_table` VALUES ('10', 'asq_comment', '评论表', null, null, 'AsqComment', 'crud', 'com.ruoyi', 'comment', 'comment', '评论', 'zjh', '0', '/', '{\"parentMenuId\":\"2036\"}', 'admin', '2022-10-27 21:58:47', '', '2022-10-27 22:39:22', null);
INSERT INTO `gen_table` VALUES ('11', 'asq_topic', '话题表', '', '', 'AsqTopic', 'crud', 'com.ruoyi', 'topic', 'topic', '话题', 'zjh', '0', '/', '{\"parentMenuId\":2043}', 'admin', '2022-10-29 19:54:57', '', '2022-10-29 20:00:11', null);

-- ----------------------------
-- Table structure for gen_table_column
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=utf8 COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES ('14', '3', 'book_id', null, 'bigint(20)', 'Long', 'bookId', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-03-22 10:54:20', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('15', '3', 'book_writer_name', '书籍作者姓名', 'varchar(20)', 'String', 'bookWriterName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '2', 'admin', '2022-03-22 10:54:20', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('16', '3', 'book_name', '书籍名', 'varchar(20)', 'String', 'bookName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '3', 'admin', '2022-03-22 10:54:20', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('17', '3', 'book_type', '书籍类型', 'varchar(20)', 'String', 'bookType', '0', '0', null, '1', '1', '1', '1', 'EQ', 'select', 'book_type', '4', 'admin', '2022-03-22 10:54:20', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('18', '3', 'book_pub_date', '出版日期', 'datetime', 'Date', 'bookPubDate', '0', '0', null, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', '5', 'admin', '2022-03-22 10:54:20', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('19', '3', 'book_up_date', '上传日期', 'datetime', 'Date', 'bookUpDate', '0', '0', null, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', '6', 'admin', '2022-03-22 10:54:20', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('20', '3', 'book_press', '出版社', 'varchar(20)', 'String', 'bookPress', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '7', 'admin', '2022-03-22 10:54:20', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('21', '3', 'book_cover', '书籍封面', 'varchar(255)', 'String', 'bookCover', '0', '0', null, '1', '1', '1', null, 'EQ', 'imageUpload', '', '8', 'admin', '2022-03-22 10:54:20', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('22', '4', 'writer_id', null, 'bigint(20)', 'Long', 'writerId', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-04-25 21:22:42', '', '2022-04-25 21:47:32');
INSERT INTO `gen_table_column` VALUES ('23', '4', 'writer_name', '作者姓名', 'varchar(20)', 'String', 'writerName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '2', 'admin', '2022-04-25 21:22:42', '', '2022-04-25 21:47:32');
INSERT INTO `gen_table_column` VALUES ('24', '4', 'writer_info', '作者详情', 'varchar(255)', 'String', 'writerInfo', '0', '0', null, '1', '1', '1', null, 'EQ', 'editor', '', '3', 'admin', '2022-04-25 21:22:42', '', '2022-04-25 21:47:32');
INSERT INTO `gen_table_column` VALUES ('25', '4', 'writer_intro', '作者座右铭', 'varchar(255)', 'String', 'writerIntro', '0', '0', null, '1', '1', '1', null, 'EQ', 'input', '', '4', 'admin', '2022-04-25 21:22:42', '', '2022-04-25 21:47:32');
INSERT INTO `gen_table_column` VALUES ('26', '4', 'writer_pic', '作者头像', 'varchar(255)', 'String', 'writerPic', '0', '0', null, '1', '1', '1', null, 'EQ', 'imageUpload', '', '5', 'admin', '2022-04-25 21:22:42', '', '2022-04-25 21:47:32');
INSERT INTO `gen_table_column` VALUES ('27', '3', 'book_pdf_url', '书籍pdf', 'varchar(255)', 'String', 'bookPdfUrl', '0', '0', null, '1', '1', '1', null, 'EQ', 'fileUpload', '', '9', '', '2022-05-02 17:01:49', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('28', '3', 'book_audit', '审核状态', 'int(6)', 'Integer', 'bookAudit', '0', '0', null, '1', '1', '1', '1', 'EQ', 'select', 'book_audit', '10', '', '2022-05-02 17:01:49', '', '2022-05-02 17:21:38');
INSERT INTO `gen_table_column` VALUES ('29', '5', 'ppt_id', 'ID', 'int(20)', 'Long', 'pptId', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-07-24 18:17:53', '', '2022-07-24 18:25:03');
INSERT INTO `gen_table_column` VALUES ('30', '5', 'ppt_name', '名称', 'varchar(50)', 'String', 'pptName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '2', 'admin', '2022-07-24 18:17:53', '', '2022-07-24 18:25:03');
INSERT INTO `gen_table_column` VALUES ('31', '5', 'ppt_pic', '图片', 'varchar(255)', 'String', 'pptPic', '0', '0', null, '1', '1', '1', null, 'EQ', 'imageUpload', '', '3', 'admin', '2022-07-24 18:17:53', '', '2022-07-24 18:25:03');
INSERT INTO `gen_table_column` VALUES ('32', '5', 'ppt_url', '跳转路径', 'varchar(255)', 'String', 'pptUrl', '0', '0', null, '1', '1', '1', null, 'EQ', 'input', '', '4', 'admin', '2022-07-24 18:17:53', '', '2022-07-24 18:25:03');
INSERT INTO `gen_table_column` VALUES ('33', '5', 'ppt_desc', '排序', 'int(20)', 'Long', 'pptDesc', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2022-07-24 18:17:53', '', '2022-07-24 18:25:03');
INSERT INTO `gen_table_column` VALUES ('34', '5', 'ppt_type', '类型', 'int(20)', 'Long', 'pptType', '0', '0', null, '1', '1', '1', '1', 'EQ', 'select', 'ppt_type', '6', 'admin', '2022-07-24 18:17:53', '', '2022-07-24 18:25:03');
INSERT INTO `gen_table_column` VALUES ('35', '6', 'rank_parm_id', null, 'int(20)', 'Long', 'rankParmId', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-07-28 08:57:08', '', '2022-07-28 09:08:00');
INSERT INTO `gen_table_column` VALUES ('36', '6', 'book_id', '书籍id', 'int(20)', 'Long', 'bookId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2022-07-28 08:57:08', '', '2022-07-28 09:08:00');
INSERT INTO `gen_table_column` VALUES ('37', '6', 'read_num', '阅读数', 'int(255)', 'Long', 'readNum', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2022-07-28 08:57:08', '', '2022-07-28 09:08:00');
INSERT INTO `gen_table_column` VALUES ('38', '6', 'applaud_num', '赞同数', 'int(255)', 'Long', 'applaudNum', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2022-07-28 08:57:08', '', '2022-07-28 09:08:00');
INSERT INTO `gen_table_column` VALUES ('39', '6', 'comment_num', '评论数', 'int(255)', 'Long', 'commentNum', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2022-07-28 08:57:08', '', '2022-07-28 09:08:00');
INSERT INTO `gen_table_column` VALUES ('40', '6', 'collect_num', '加入书架数', 'int(255)', 'Long', 'collectNum', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2022-07-28 08:57:08', '', '2022-07-28 09:08:00');
INSERT INTO `gen_table_column` VALUES ('41', '7', 'applaud_id', null, 'int(20)', 'Long', 'applaudId', '1', '0', null, '1', null, null, '1', 'EQ', 'input', '', '1', 'admin', '2022-10-24 21:32:51', '', '2022-10-25 19:27:07');
INSERT INTO `gen_table_column` VALUES ('42', '7', 'user_id', '用户', 'int(20)', 'Long', 'userId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2022-10-24 21:32:51', '', '2022-10-25 19:27:07');
INSERT INTO `gen_table_column` VALUES ('43', '7', 'book_id', '书籍', 'int(20)', 'Long', 'bookId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2022-10-24 21:32:51', '', '2022-10-25 19:27:07');
INSERT INTO `gen_table_column` VALUES ('44', '7', 'cread_data', '创建时间', 'datetime', 'Date', 'creadData', '0', '0', null, '1', '1', '1', '1', 'EQ', 'datetime', '', '4', 'admin', '2022-10-24 21:32:51', '', '2022-10-25 19:27:07');
INSERT INTO `gen_table_column` VALUES ('49', '9', 'collect_id', null, 'int(20)', 'Long', 'collectId', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-10-26 19:28:56', '', '2022-10-26 19:29:31');
INSERT INTO `gen_table_column` VALUES ('50', '9', 'book_id', '书籍id', 'int(20)', 'Long', 'bookId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2022-10-26 19:28:56', '', '2022-10-26 19:29:31');
INSERT INTO `gen_table_column` VALUES ('51', '9', 'user_id', '用户id', 'int(20)', 'Long', 'userId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2022-10-26 19:28:56', '', '2022-10-26 19:29:31');
INSERT INTO `gen_table_column` VALUES ('52', '9', 'create_date', '创建时间', 'datetime', 'Date', 'createDate', '0', '0', null, '1', '1', '1', '1', 'EQ', 'datetime', '', '4', 'admin', '2022-10-26 19:28:56', '', '2022-10-26 19:29:31');
INSERT INTO `gen_table_column` VALUES ('53', '10', 'comment_id', null, 'int(20)', 'Long', 'commentId', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-10-27 21:58:47', '', '2022-10-27 22:39:22');
INSERT INTO `gen_table_column` VALUES ('54', '10', 'comment_parent_id', '父级id', 'int(20)', 'Long', 'commentParentId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2022-10-27 21:58:47', '', '2022-10-27 22:39:22');
INSERT INTO `gen_table_column` VALUES ('55', '10', 'user_id', '用户id', 'int(20)', 'Long', 'userId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2022-10-27 21:58:47', '', '2022-10-27 22:39:22');
INSERT INTO `gen_table_column` VALUES ('56', '10', 'com_body_id', '评论体', 'int(20)', 'Long', 'comBodyId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2022-10-27 21:58:47', '', '2022-10-27 22:39:22');
INSERT INTO `gen_table_column` VALUES ('57', '10', 'comment_text', '评论内容', 'varchar(255)', 'String', 'commentText', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '5', 'admin', '2022-10-27 21:58:47', '', '2022-10-27 22:39:22');
INSERT INTO `gen_table_column` VALUES ('58', '10', 'com_body_type', '评论主体类型，1书，2社区', 'int(1)', 'Integer', 'comBodyType', '0', '0', null, '1', '1', '1', '1', 'EQ', 'select', '', '6', 'admin', '2022-10-27 21:58:47', '', '2022-10-27 22:39:22');
INSERT INTO `gen_table_column` VALUES ('59', '10', 'create_date', '创建时间', 'datetime', 'Date', 'createDate', '0', '0', null, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', '7', 'admin', '2022-10-27 21:58:47', '', '2022-10-27 22:39:23');
INSERT INTO `gen_table_column` VALUES ('61', '10', 'is_vio', '是否违规', 'int(1)', 'Integer', 'isVio', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '9', '', '2022-10-27 22:04:14', '', '2022-10-27 22:39:23');
INSERT INTO `gen_table_column` VALUES ('62', '10', 'like_num', '点赞数', 'int(20)', 'Long', 'likeNum', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '8', '', '2022-10-27 22:37:32', '', '2022-10-27 22:39:23');
INSERT INTO `gen_table_column` VALUES ('63', '11', 'topic_id', null, 'int(20)', 'Long', 'topicId', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-10-29 19:54:57', '', '2022-10-29 20:00:11');
INSERT INTO `gen_table_column` VALUES ('64', '11', 'user_id', null, 'int(20)', 'Long', 'userId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2022-10-29 19:54:57', '', '2022-10-29 20:00:11');
INSERT INTO `gen_table_column` VALUES ('65', '11', 'topic_content', '文章', 'text', 'String', 'topicContent', '0', '0', null, '1', '1', '1', null, 'EQ', 'editor', '', '3', 'admin', '2022-10-29 19:54:57', '', '2022-10-29 20:00:11');
INSERT INTO `gen_table_column` VALUES ('66', '11', 'topic_cover', '封面', 'varchar(255)', 'String', 'topicCover', '0', '0', null, '1', '1', '1', null, 'EQ', 'imageUpload', '', '4', 'admin', '2022-10-29 19:54:57', '', '2022-10-29 20:00:11');
INSERT INTO `gen_table_column` VALUES ('67', '11', 'topic_createdate', '发布时间', 'datetime', 'Date', 'topicCreatedate', '0', '0', null, '1', '1', '1', '1', 'BETWEEN', 'datetime', '', '5', 'admin', '2022-10-29 19:54:57', '', '2022-10-29 20:00:11');
INSERT INTO `gen_table_column` VALUES ('68', '11', 'topic_name', '话题', 'varchar(255)', 'String', 'topicName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '3', '', '2022-10-29 19:58:14', '', '2022-10-29 20:00:11');

-- ----------------------------
-- Table structure for qrtz_blob_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_blob_triggers`;
CREATE TABLE `qrtz_blob_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `blob_data` blob COMMENT '存放持久化Trigger对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_blob_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Blob类型的触发器表';

-- ----------------------------
-- Records of qrtz_blob_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_calendars
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_calendars`;
CREATE TABLE `qrtz_calendars` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `calendar_name` varchar(200) NOT NULL COMMENT '日历名称',
  `calendar` blob NOT NULL COMMENT '存放持久化calendar对象',
  PRIMARY KEY (`sched_name`,`calendar_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='日历信息表';

-- ----------------------------
-- Records of qrtz_calendars
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_cron_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_cron_triggers`;
CREATE TABLE `qrtz_cron_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `cron_expression` varchar(200) NOT NULL COMMENT 'cron表达式',
  `time_zone_id` varchar(80) DEFAULT NULL COMMENT '时区',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_cron_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Cron类型的触发器表';

-- ----------------------------
-- Records of qrtz_cron_triggers
-- ----------------------------
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', '0/10 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', '0/15 * * * * ?', 'Asia/Shanghai');
INSERT INTO `qrtz_cron_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', '0/20 * * * * ?', 'Asia/Shanghai');

-- ----------------------------
-- Table structure for qrtz_fired_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_fired_triggers`;
CREATE TABLE `qrtz_fired_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `entry_id` varchar(95) NOT NULL COMMENT '调度器实例id',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `instance_name` varchar(200) NOT NULL COMMENT '调度器实例名',
  `fired_time` bigint(13) NOT NULL COMMENT '触发的时间',
  `sched_time` bigint(13) NOT NULL COMMENT '定时器制定的时间',
  `priority` int(11) NOT NULL COMMENT '优先级',
  `state` varchar(16) NOT NULL COMMENT '状态',
  `job_name` varchar(200) DEFAULT NULL COMMENT '任务名称',
  `job_group` varchar(200) DEFAULT NULL COMMENT '任务组名',
  `is_nonconcurrent` varchar(1) DEFAULT NULL COMMENT '是否并发',
  `requests_recovery` varchar(1) DEFAULT NULL COMMENT '是否接受恢复执行',
  PRIMARY KEY (`sched_name`,`entry_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='已触发的触发器表';

-- ----------------------------
-- Records of qrtz_fired_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_job_details
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_job_details`;
CREATE TABLE `qrtz_job_details` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `job_name` varchar(200) NOT NULL COMMENT '任务名称',
  `job_group` varchar(200) NOT NULL COMMENT '任务组名',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `job_class_name` varchar(250) NOT NULL COMMENT '执行任务类名称',
  `is_durable` varchar(1) NOT NULL COMMENT '是否持久化',
  `is_nonconcurrent` varchar(1) NOT NULL COMMENT '是否并发',
  `is_update_data` varchar(1) NOT NULL COMMENT '是否更新数据',
  `requests_recovery` varchar(1) NOT NULL COMMENT '是否接受恢复执行',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`job_name`,`job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='任务详细信息表';

-- ----------------------------
-- Records of qrtz_job_details
-- ----------------------------
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017FACAE85A878707400007070707400013174000E302F3130202A202A202A202A203F74001172795461736B2E72794E6F506172616D7374000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000001740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E697A0E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017FACAE85A878707400007070707400013174000E302F3135202A202A202A202A203F74001572795461736B2E7279506172616D7328277279272974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000002740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E69C89E58F82EFBC8974000133740001317800);
INSERT INTO `qrtz_job_details` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', null, 'com.ruoyi.quartz.util.QuartzDisallowConcurrentExecution', '0', '1', '0', '0', 0xACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A6176612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F724900097468726573686F6C6478703F4000000000000C7708000000100000000174000F5441534B5F50524F504552544945537372001E636F6D2E72756F79692E71756172747A2E646F6D61696E2E5379734A6F6200000000000000010200084C000A636F6E63757272656E747400124C6A6176612F6C616E672F537472696E673B4C000E63726F6E45787072657373696F6E71007E00094C000C696E766F6B6554617267657471007E00094C00086A6F6247726F757071007E00094C00056A6F6249647400104C6A6176612F6C616E672F4C6F6E673B4C00076A6F624E616D6571007E00094C000D6D697366697265506F6C69637971007E00094C000673746174757371007E000978720027636F6D2E72756F79692E636F6D6D6F6E2E636F72652E646F6D61696E2E42617365456E7469747900000000000000010200074C0008637265617465427971007E00094C000A63726561746554696D657400104C6A6176612F7574696C2F446174653B4C0006706172616D7371007E00034C000672656D61726B71007E00094C000B73656172636856616C756571007E00094C0008757064617465427971007E00094C000A75706461746554696D6571007E000C787074000561646D696E7372000E6A6176612E7574696C2E44617465686A81014B597419030000787077080000017FACAE85A878707400007070707400013174000E302F3230202A202A202A202A203F74003872795461736B2E72794D756C7469706C65506172616D7328277279272C20747275652C20323030304C2C203331362E3530442C203130302974000744454641554C547372000E6A6176612E6C616E672E4C6F6E673B8BE490CC8F23DF0200014A000576616C7565787200106A6176612E6C616E672E4E756D62657286AC951D0B94E08B02000078700000000000000003740018E7B3BBE7BB9FE9BB98E8AEA4EFBC88E5A49AE58F82EFBC8974000133740001317800);

-- ----------------------------
-- Table structure for qrtz_locks
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_locks`;
CREATE TABLE `qrtz_locks` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `lock_name` varchar(40) NOT NULL COMMENT '悲观锁名称',
  PRIMARY KEY (`sched_name`,`lock_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='存储的悲观锁信息表';

-- ----------------------------
-- Records of qrtz_locks
-- ----------------------------
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'STATE_ACCESS');
INSERT INTO `qrtz_locks` VALUES ('RuoyiScheduler', 'TRIGGER_ACCESS');

-- ----------------------------
-- Table structure for qrtz_paused_trigger_grps
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_paused_trigger_grps`;
CREATE TABLE `qrtz_paused_trigger_grps` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  PRIMARY KEY (`sched_name`,`trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='暂停的触发器表';

-- ----------------------------
-- Records of qrtz_paused_trigger_grps
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_scheduler_state
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_scheduler_state`;
CREATE TABLE `qrtz_scheduler_state` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `instance_name` varchar(200) NOT NULL COMMENT '实例名称',
  `last_checkin_time` bigint(13) NOT NULL COMMENT '上次检查时间',
  `checkin_interval` bigint(13) NOT NULL COMMENT '检查间隔时间',
  PRIMARY KEY (`sched_name`,`instance_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='调度器状态表';

-- ----------------------------
-- Records of qrtz_scheduler_state
-- ----------------------------
INSERT INTO `qrtz_scheduler_state` VALUES ('RuoyiScheduler', 'zjh1667116020576', '1667120808494', '15000');

-- ----------------------------
-- Table structure for qrtz_simple_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simple_triggers`;
CREATE TABLE `qrtz_simple_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `repeat_count` bigint(7) NOT NULL COMMENT '重复的次数统计',
  `repeat_interval` bigint(12) NOT NULL COMMENT '重复的间隔时间',
  `times_triggered` bigint(10) NOT NULL COMMENT '已经触发的次数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simple_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='简单触发器的信息表';

-- ----------------------------
-- Records of qrtz_simple_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_simprop_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_simprop_triggers`;
CREATE TABLE `qrtz_simprop_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_name的外键',
  `trigger_group` varchar(200) NOT NULL COMMENT 'qrtz_triggers表trigger_group的外键',
  `str_prop_1` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第一个参数',
  `str_prop_2` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第二个参数',
  `str_prop_3` varchar(512) DEFAULT NULL COMMENT 'String类型的trigger的第三个参数',
  `int_prop_1` int(11) DEFAULT NULL COMMENT 'int类型的trigger的第一个参数',
  `int_prop_2` int(11) DEFAULT NULL COMMENT 'int类型的trigger的第二个参数',
  `long_prop_1` bigint(20) DEFAULT NULL COMMENT 'long类型的trigger的第一个参数',
  `long_prop_2` bigint(20) DEFAULT NULL COMMENT 'long类型的trigger的第二个参数',
  `dec_prop_1` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第一个参数',
  `dec_prop_2` decimal(13,4) DEFAULT NULL COMMENT 'decimal类型的trigger的第二个参数',
  `bool_prop_1` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第一个参数',
  `bool_prop_2` varchar(1) DEFAULT NULL COMMENT 'Boolean类型的trigger的第二个参数',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  CONSTRAINT `qrtz_simprop_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `trigger_name`, `trigger_group`) REFERENCES `qrtz_triggers` (`sched_name`, `trigger_name`, `trigger_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='同步机制的行锁表';

-- ----------------------------
-- Records of qrtz_simprop_triggers
-- ----------------------------

-- ----------------------------
-- Table structure for qrtz_triggers
-- ----------------------------
DROP TABLE IF EXISTS `qrtz_triggers`;
CREATE TABLE `qrtz_triggers` (
  `sched_name` varchar(120) NOT NULL COMMENT '调度名称',
  `trigger_name` varchar(200) NOT NULL COMMENT '触发器的名字',
  `trigger_group` varchar(200) NOT NULL COMMENT '触发器所属组的名字',
  `job_name` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_name的外键',
  `job_group` varchar(200) NOT NULL COMMENT 'qrtz_job_details表job_group的外键',
  `description` varchar(250) DEFAULT NULL COMMENT '相关介绍',
  `next_fire_time` bigint(13) DEFAULT NULL COMMENT '上一次触发时间（毫秒）',
  `prev_fire_time` bigint(13) DEFAULT NULL COMMENT '下一次触发时间（默认为-1表示不触发）',
  `priority` int(11) DEFAULT NULL COMMENT '优先级',
  `trigger_state` varchar(16) NOT NULL COMMENT '触发器状态',
  `trigger_type` varchar(8) NOT NULL COMMENT '触发器的类型',
  `start_time` bigint(13) NOT NULL COMMENT '开始时间',
  `end_time` bigint(13) DEFAULT NULL COMMENT '结束时间',
  `calendar_name` varchar(200) DEFAULT NULL COMMENT '日程表名称',
  `misfire_instr` smallint(2) DEFAULT NULL COMMENT '补偿执行的策略',
  `job_data` blob COMMENT '存放持久化job对象',
  PRIMARY KEY (`sched_name`,`trigger_name`,`trigger_group`),
  KEY `sched_name` (`sched_name`,`job_name`,`job_group`),
  CONSTRAINT `qrtz_triggers_ibfk_1` FOREIGN KEY (`sched_name`, `job_name`, `job_group`) REFERENCES `qrtz_job_details` (`sched_name`, `job_name`, `job_group`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='触发器详细信息表';

-- ----------------------------
-- Records of qrtz_triggers
-- ----------------------------
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME1', 'DEFAULT', 'TASK_CLASS_NAME1', 'DEFAULT', null, '1667116020000', '-1', '5', 'PAUSED', 'CRON', '1667116020000', '0', null, '2', '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME2', 'DEFAULT', 'TASK_CLASS_NAME2', 'DEFAULT', null, '1667116020000', '-1', '5', 'PAUSED', 'CRON', '1667116020000', '0', null, '2', '');
INSERT INTO `qrtz_triggers` VALUES ('RuoyiScheduler', 'TASK_CLASS_NAME3', 'DEFAULT', 'TASK_CLASS_NAME3', 'DEFAULT', null, '1667116020000', '-1', '5', 'PAUSED', 'CRON', '1667116020000', '0', null, '2', '');

-- ----------------------------
-- Table structure for sys_config
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-03-21 21:33:13', '', null, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-03-21 21:33:13', '', null, '初始化密码 123456');
INSERT INTO `sys_config` VALUES ('3', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-03-21 21:33:13', '', null, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES ('4', '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', '2022-03-21 21:33:13', '', null, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES ('5', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'true', 'Y', 'admin', '2022-03-21 21:33:13', 'admin', '2022-05-02 18:10:14', '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for sys_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(50) DEFAULT NULL COMMENT '邮箱',
  `status` char(1) DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB AUTO_INCREMENT=110 DEFAULT CHARSET=utf8 COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('100', '0', '0', '拾柒科技', '0', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('101', '100', '0,100', '深圳总公司', '1', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('102', '100', '0,100', '长沙分公司', '2', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('103', '101', '0,100,101', '研发部门', '1', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('104', '101', '0,100,101', '市场部门', '2', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('105', '101', '0,100,101', '测试部门', '3', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('106', '101', '0,100,101', '财务部门', '4', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('107', '101', '0,100,101', '运维部门', '5', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('108', '102', '0,100,102', '市场部门', '1', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);
INSERT INTO `sys_dept` VALUES ('109', '102', '0,100,102', '财务部门', '2', 'liefox', '15888888888', 'ry@qq.com', '0', '0', 'admin', '2022-03-21 21:33:12', '', null);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=utf8 COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-03-21 21:33:13', '', null, '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '性别女');
INSERT INTO `sys_dict_data` VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-03-21 21:33:13', '', null, '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-03-21 21:33:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-03-21 21:33:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-03-21 21:33:13', '', null, '默认分组');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '系统分组');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-03-21 21:33:13', '', null, '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '通知', '1', 'sys_notice_type', '', 'warning', 'Y', '0', 'admin', '2022-03-21 21:33:13', '', null, '通知');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '公告', '2', 'sys_notice_type', '', 'success', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '公告');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-03-21 21:33:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('18', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '新增操作');
INSERT INTO `sys_dict_data` VALUES ('19', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '修改操作');
INSERT INTO `sys_dict_data` VALUES ('20', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '删除操作');
INSERT INTO `sys_dict_data` VALUES ('21', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '授权操作');
INSERT INTO `sys_dict_data` VALUES ('22', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '导出操作');
INSERT INTO `sys_dict_data` VALUES ('23', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '导入操作');
INSERT INTO `sys_dict_data` VALUES ('24', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '强退操作');
INSERT INTO `sys_dict_data` VALUES ('25', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '生成操作');
INSERT INTO `sys_dict_data` VALUES ('26', '9', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '清空操作');
INSERT INTO `sys_dict_data` VALUES ('27', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('28', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-03-21 21:33:13', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('100', '0', 'Java', 'Java', 'book_type', null, 'success', 'N', '0', 'admin', '2022-03-22 09:13:22', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('101', '0', 'Vue', 'Vue', 'book_type', null, 'success', 'N', '0', 'admin', '2022-03-22 09:13:39', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('102', '0', '生活', '生活', 'book_type', null, 'success', 'N', '0', 'admin', '2022-03-22 09:13:55', 'admin', '2022-03-22 09:14:01', null);
INSERT INTO `sys_dict_data` VALUES ('103', '0', '待审核', '0', 'book_audit', null, 'warning', 'N', '0', 'admin', '2022-05-02 17:03:53', 'admin', '2022-05-02 18:30:21', null);
INSERT INTO `sys_dict_data` VALUES ('104', '0', '审核成功', '1', 'book_audit', null, 'success', 'N', '0', 'admin', '2022-05-02 17:04:03', 'admin', '2022-05-02 18:30:29', null);
INSERT INTO `sys_dict_data` VALUES ('105', '0', '审核失败', '2', 'book_audit', null, 'danger', 'N', '0', 'admin', '2022-05-02 17:04:13', 'admin', '2022-05-02 18:30:35', null);
INSERT INTO `sys_dict_data` VALUES ('106', '0', '书籍', '1', 'ppt_type', null, 'default', 'N', '0', 'admin', '2022-07-24 18:23:22', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('107', '0', '文章', '2', 'ppt_type', null, 'default', 'N', '0', 'admin', '2022-07-24 18:23:37', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('108', '0', '第三方', '3', 'ppt_type', null, 'default', 'N', '0', 'admin', '2022-07-24 18:23:45', '', null, null);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT '' COMMENT '字典类型',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `dict_type` (`dict_type`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8 COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2022-03-21 21:33:13', '', null, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2022-03-21 21:33:13', '', null, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2022-03-21 21:33:13', '', null, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2022-03-21 21:33:13', '', null, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', '2022-03-21 21:33:13', '', null, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', '2022-03-21 21:33:13', '', null, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知类型', 'sys_notice_type', '0', 'admin', '2022-03-21 21:33:13', '', null, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', '2022-03-21 21:33:13', '', null, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', '2022-03-21 21:33:13', '', null, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', '2022-03-21 21:33:13', '', null, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('100', '书籍类型', 'book_type', '0', 'admin', '2022-03-22 09:12:42', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('101', '书籍审核状态', 'book_audit', '0', 'admin', '2022-05-02 17:03:26', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('102', '幻灯片类型', 'ppt_type', '0', 'admin', '2022-07-24 18:22:44', 'admin', '2022-07-24 18:22:56', null);

-- ----------------------------
-- Table structure for sys_job
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-03-21 21:33:13', '', null, '');
INSERT INTO `sys_job` VALUES ('2', '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-03-21 21:33:13', '', null, '');
INSERT INTO `sys_job` VALUES ('3', '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-03-21 21:33:13', '', null, '');

-- ----------------------------
-- Table structure for sys_job_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) DEFAULT NULL COMMENT '日志信息',
  `status` char(1) DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_logininfor
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) DEFAULT '' COMMENT '操作系统',
  `status` char(1) DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`)
) ENGINE=InnoDB AUTO_INCREMENT=383 DEFAULT CHARSET=utf8 COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('100', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-21 22:00:29');
INSERT INTO `sys_logininfor` VALUES ('101', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 22:00:57');
INSERT INTO `sys_logininfor` VALUES ('102', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-22 09:06:33');
INSERT INTO `sys_logininfor` VALUES ('103', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 09:06:43');
INSERT INTO `sys_logininfor` VALUES ('104', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-22 09:54:22');
INSERT INTO `sys_logininfor` VALUES ('105', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-22 09:54:28');
INSERT INTO `sys_logininfor` VALUES ('106', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 09:54:34');
INSERT INTO `sys_logininfor` VALUES ('107', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-22 10:48:38');
INSERT INTO `sys_logininfor` VALUES ('108', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-22 10:48:44');
INSERT INTO `sys_logininfor` VALUES ('109', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 10:48:49');
INSERT INTO `sys_logininfor` VALUES ('110', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-22 10:52:57');
INSERT INTO `sys_logininfor` VALUES ('111', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 10:53:05');
INSERT INTO `sys_logininfor` VALUES ('112', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-22 13:01:09');
INSERT INTO `sys_logininfor` VALUES ('113', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 13:01:14');
INSERT INTO `sys_logininfor` VALUES ('114', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 13:01:53');
INSERT INTO `sys_logininfor` VALUES ('115', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-22 13:01:58');
INSERT INTO `sys_logininfor` VALUES ('116', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 13:02:05');
INSERT INTO `sys_logininfor` VALUES ('117', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码已失效', '2022-03-22 13:05:31');
INSERT INTO `sys_logininfor` VALUES ('118', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 13:05:37');
INSERT INTO `sys_logininfor` VALUES ('119', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 15:36:08');
INSERT INTO `sys_logininfor` VALUES ('120', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 21:03:19');
INSERT INTO `sys_logininfor` VALUES ('121', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 22:17:29');
INSERT INTO `sys_logininfor` VALUES ('122', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 22:49:20');
INSERT INTO `sys_logininfor` VALUES ('123', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 23:52:52');
INSERT INTO `sys_logininfor` VALUES ('124', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-25 23:59:28');
INSERT INTO `sys_logininfor` VALUES ('125', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-26 00:05:47');
INSERT INTO `sys_logininfor` VALUES ('126', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-26 00:09:37');
INSERT INTO `sys_logininfor` VALUES ('127', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '验证码已失效', '2022-04-26 00:12:19');
INSERT INTO `sys_logininfor` VALUES ('128', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '用户不存在/密码错误', '2022-04-26 00:12:23');
INSERT INTO `sys_logininfor` VALUES ('129', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '用户不存在/密码错误', '2022-04-26 00:12:33');
INSERT INTO `sys_logininfor` VALUES ('130', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '用户不存在/密码错误', '2022-04-26 00:12:47');
INSERT INTO `sys_logininfor` VALUES ('131', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '用户不存在/密码错误', '2022-04-26 00:13:02');
INSERT INTO `sys_logininfor` VALUES ('132', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-26 00:13:20');
INSERT INTO `sys_logininfor` VALUES ('133', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '验证码错误', '2022-04-26 00:13:45');
INSERT INTO `sys_logininfor` VALUES ('134', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '0', '登录成功', '2022-04-26 00:13:50');
INSERT INTO `sys_logininfor` VALUES ('135', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-26 00:14:54');
INSERT INTO `sys_logininfor` VALUES ('136', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-04-27 11:27:19');
INSERT INTO `sys_logininfor` VALUES ('137', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-02 14:45:26');
INSERT INTO `sys_logininfor` VALUES ('138', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-05-02 14:53:36');
INSERT INTO `sys_logininfor` VALUES ('139', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '用户不存在/密码错误', '2022-05-02 14:56:48');
INSERT INTO `sys_logininfor` VALUES ('140', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '用户不存在/密码错误', '2022-05-02 14:57:01');
INSERT INTO `sys_logininfor` VALUES ('141', 'admin', '127.0.0.1', '内网IP', 'Chrome 64', 'Windows 8', '1', '用户不存在/密码错误', '2022-05-02 14:57:19');
INSERT INTO `sys_logininfor` VALUES ('142', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-02 14:57:35');
INSERT INTO `sys_logininfor` VALUES ('143', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-05-02 18:07:56');
INSERT INTO `sys_logininfor` VALUES ('144', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-02 18:09:50');
INSERT INTO `sys_logininfor` VALUES ('145', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-05-02 18:10:19');
INSERT INTO `sys_logininfor` VALUES ('146', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-05-02 18:19:26');
INSERT INTO `sys_logininfor` VALUES ('147', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-02 18:19:30');
INSERT INTO `sys_logininfor` VALUES ('148', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-02 19:04:28');
INSERT INTO `sys_logininfor` VALUES ('149', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-05-02 19:39:10');
INSERT INTO `sys_logininfor` VALUES ('150', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-05-02 19:42:35');
INSERT INTO `sys_logininfor` VALUES ('151', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-02 19:42:39');
INSERT INTO `sys_logininfor` VALUES ('152', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-05-02 19:52:22');
INSERT INTO `sys_logininfor` VALUES ('153', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-05-02 19:58:16');
INSERT INTO `sys_logininfor` VALUES ('154', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-02 19:58:21');
INSERT INTO `sys_logininfor` VALUES ('155', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-02 20:04:51');
INSERT INTO `sys_logininfor` VALUES ('156', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-03 07:57:26');
INSERT INTO `sys_logininfor` VALUES ('157', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-05-03 07:57:48');
INSERT INTO `sys_logininfor` VALUES ('158', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-03 07:57:56');
INSERT INTO `sys_logininfor` VALUES ('159', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-05-03 07:59:40');
INSERT INTO `sys_logininfor` VALUES ('160', 'dev', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '注册成功', '2022-05-03 08:00:06');
INSERT INTO `sys_logininfor` VALUES ('161', 'dev', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-05-03 08:00:34');
INSERT INTO `sys_logininfor` VALUES ('162', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-07-24 15:39:24');
INSERT INTO `sys_logininfor` VALUES ('163', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-07-24 15:39:28');
INSERT INTO `sys_logininfor` VALUES ('164', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-07-24 15:39:32');
INSERT INTO `sys_logininfor` VALUES ('165', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 15:39:39');
INSERT INTO `sys_logininfor` VALUES ('166', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-07-24 16:22:27');
INSERT INTO `sys_logininfor` VALUES ('167', 'zjh', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '注册成功', '2022-07-24 16:23:06');
INSERT INTO `sys_logininfor` VALUES ('168', 'zjh', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 16:23:20');
INSERT INTO `sys_logininfor` VALUES ('169', 'zjh', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-07-24 16:23:38');
INSERT INTO `sys_logininfor` VALUES ('170', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 16:23:43');
INSERT INTO `sys_logininfor` VALUES ('171', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-07-24 16:31:10');
INSERT INTO `sys_logininfor` VALUES ('172', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-07-24 16:32:34');
INSERT INTO `sys_logininfor` VALUES ('173', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-07-24 16:32:39');
INSERT INTO `sys_logininfor` VALUES ('174', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 16:32:42');
INSERT INTO `sys_logininfor` VALUES ('175', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-07-24 16:33:26');
INSERT INTO `sys_logininfor` VALUES ('176', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-07-24 16:53:59');
INSERT INTO `sys_logininfor` VALUES ('177', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 16:54:02');
INSERT INTO `sys_logininfor` VALUES ('178', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-07-24 17:00:12');
INSERT INTO `sys_logininfor` VALUES ('179', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 17:28:18');
INSERT INTO `sys_logininfor` VALUES ('180', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-07-24 17:28:37');
INSERT INTO `sys_logininfor` VALUES ('181', '测试', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '注册成功', '2022-07-24 17:29:39');
INSERT INTO `sys_logininfor` VALUES ('182', '测试', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 17:29:51');
INSERT INTO `sys_logininfor` VALUES ('183', '测试', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-07-24 17:34:23');
INSERT INTO `sys_logininfor` VALUES ('184', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 17:34:42');
INSERT INTO `sys_logininfor` VALUES ('185', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 18:17:29');
INSERT INTO `sys_logininfor` VALUES ('186', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-24 19:07:06');
INSERT INTO `sys_logininfor` VALUES ('187', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-25 10:57:03');
INSERT INTO `sys_logininfor` VALUES ('188', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-25 12:35:57');
INSERT INTO `sys_logininfor` VALUES ('189', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-26 08:45:33');
INSERT INTO `sys_logininfor` VALUES ('190', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-26 13:51:18');
INSERT INTO `sys_logininfor` VALUES ('191', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-28 08:56:44');
INSERT INTO `sys_logininfor` VALUES ('192', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-28 11:24:12');
INSERT INTO `sys_logininfor` VALUES ('193', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-28 12:03:55');
INSERT INTO `sys_logininfor` VALUES ('194', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-07-28 14:49:27');
INSERT INTO `sys_logininfor` VALUES ('195', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-09-12 19:37:29');
INSERT INTO `sys_logininfor` VALUES ('196', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-09-12 19:37:33');
INSERT INTO `sys_logininfor` VALUES ('197', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-09-12 20:01:23');
INSERT INTO `sys_logininfor` VALUES ('198', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-09-12 20:20:58');
INSERT INTO `sys_logininfor` VALUES ('199', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码已失效', '2022-09-12 20:23:16');
INSERT INTO `sys_logininfor` VALUES ('200', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-09-12 20:23:19');
INSERT INTO `sys_logininfor` VALUES ('201', '', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码已失效', '2022-09-12 21:01:14');
INSERT INTO `sys_logininfor` VALUES ('202', '', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码已失效', '2022-09-12 21:01:34');
INSERT INTO `sys_logininfor` VALUES ('203', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-12 22:19:08');
INSERT INTO `sys_logininfor` VALUES ('204', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-09-12 22:19:39');
INSERT INTO `sys_logininfor` VALUES ('205', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 10 (iPhone)', '0', '登录成功', '2022-09-12 22:43:19');
INSERT INTO `sys_logininfor` VALUES ('206', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-12 22:45:32');
INSERT INTO `sys_logininfor` VALUES ('207', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 10:08:20');
INSERT INTO `sys_logininfor` VALUES ('208', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 10:09:31');
INSERT INTO `sys_logininfor` VALUES ('209', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 10:11:34');
INSERT INTO `sys_logininfor` VALUES ('210', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 10:18:59');
INSERT INTO `sys_logininfor` VALUES ('211', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 10:38:39');
INSERT INTO `sys_logininfor` VALUES ('212', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 10:41:02');
INSERT INTO `sys_logininfor` VALUES ('213', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 10:41:30');
INSERT INTO `sys_logininfor` VALUES ('214', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 12:33:16');
INSERT INTO `sys_logininfor` VALUES ('215', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 12:43:23');
INSERT INTO `sys_logininfor` VALUES ('216', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 12:44:16');
INSERT INTO `sys_logininfor` VALUES ('217', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 12:49:08');
INSERT INTO `sys_logininfor` VALUES ('218', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 12:57:56');
INSERT INTO `sys_logininfor` VALUES ('219', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:00:54');
INSERT INTO `sys_logininfor` VALUES ('220', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:01:47');
INSERT INTO `sys_logininfor` VALUES ('221', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:02:19');
INSERT INTO `sys_logininfor` VALUES ('222', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:02:38');
INSERT INTO `sys_logininfor` VALUES ('223', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:03:06');
INSERT INTO `sys_logininfor` VALUES ('224', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:03:52');
INSERT INTO `sys_logininfor` VALUES ('225', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:08:10');
INSERT INTO `sys_logininfor` VALUES ('226', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:08:34');
INSERT INTO `sys_logininfor` VALUES ('227', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:09:02');
INSERT INTO `sys_logininfor` VALUES ('228', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:11:06');
INSERT INTO `sys_logininfor` VALUES ('229', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:12:27');
INSERT INTO `sys_logininfor` VALUES ('230', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:13:18');
INSERT INTO `sys_logininfor` VALUES ('231', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:13:36');
INSERT INTO `sys_logininfor` VALUES ('232', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:15:22');
INSERT INTO `sys_logininfor` VALUES ('233', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:18:52');
INSERT INTO `sys_logininfor` VALUES ('234', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:19:18');
INSERT INTO `sys_logininfor` VALUES ('235', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:19:35');
INSERT INTO `sys_logininfor` VALUES ('236', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:20:14');
INSERT INTO `sys_logininfor` VALUES ('237', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:21:00');
INSERT INTO `sys_logininfor` VALUES ('238', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:21:23');
INSERT INTO `sys_logininfor` VALUES ('239', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-13 13:22:59');
INSERT INTO `sys_logininfor` VALUES ('240', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 13:23:02');
INSERT INTO `sys_logininfor` VALUES ('241', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:17:11');
INSERT INTO `sys_logininfor` VALUES ('242', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:26:05');
INSERT INTO `sys_logininfor` VALUES ('243', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:28:02');
INSERT INTO `sys_logininfor` VALUES ('244', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:28:57');
INSERT INTO `sys_logininfor` VALUES ('245', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:29:52');
INSERT INTO `sys_logininfor` VALUES ('246', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:32:27');
INSERT INTO `sys_logininfor` VALUES ('247', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:34:15');
INSERT INTO `sys_logininfor` VALUES ('248', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:34:30');
INSERT INTO `sys_logininfor` VALUES ('249', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:34:49');
INSERT INTO `sys_logininfor` VALUES ('250', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码已失效', '2022-09-13 16:34:49');
INSERT INTO `sys_logininfor` VALUES ('251', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:39:45');
INSERT INTO `sys_logininfor` VALUES ('252', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:48:39');
INSERT INTO `sys_logininfor` VALUES ('253', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:48:56');
INSERT INTO `sys_logininfor` VALUES ('254', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 16:49:44');
INSERT INTO `sys_logininfor` VALUES ('255', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 18:54:59');
INSERT INTO `sys_logininfor` VALUES ('256', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 18:55:18');
INSERT INTO `sys_logininfor` VALUES ('257', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 18:57:14');
INSERT INTO `sys_logininfor` VALUES ('258', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:01:00');
INSERT INTO `sys_logininfor` VALUES ('259', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:02:12');
INSERT INTO `sys_logininfor` VALUES ('260', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-13 19:05:10');
INSERT INTO `sys_logininfor` VALUES ('261', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-13 19:05:15');
INSERT INTO `sys_logininfor` VALUES ('262', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:05:19');
INSERT INTO `sys_logininfor` VALUES ('263', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:07:37');
INSERT INTO `sys_logininfor` VALUES ('264', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:11:09');
INSERT INTO `sys_logininfor` VALUES ('265', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:12:51');
INSERT INTO `sys_logininfor` VALUES ('266', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:13:27');
INSERT INTO `sys_logininfor` VALUES ('267', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:13:54');
INSERT INTO `sys_logininfor` VALUES ('268', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:14:26');
INSERT INTO `sys_logininfor` VALUES ('269', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:17:08');
INSERT INTO `sys_logininfor` VALUES ('270', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:17:43');
INSERT INTO `sys_logininfor` VALUES ('271', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:18:19');
INSERT INTO `sys_logininfor` VALUES ('272', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:20:13');
INSERT INTO `sys_logininfor` VALUES ('273', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:24:06');
INSERT INTO `sys_logininfor` VALUES ('274', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:27:48');
INSERT INTO `sys_logininfor` VALUES ('275', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:28:58');
INSERT INTO `sys_logininfor` VALUES ('276', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:29:49');
INSERT INTO `sys_logininfor` VALUES ('277', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:30:10');
INSERT INTO `sys_logininfor` VALUES ('278', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:33:03');
INSERT INTO `sys_logininfor` VALUES ('279', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:33:27');
INSERT INTO `sys_logininfor` VALUES ('280', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:34:48');
INSERT INTO `sys_logininfor` VALUES ('281', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:37:22');
INSERT INTO `sys_logininfor` VALUES ('282', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:39:57');
INSERT INTO `sys_logininfor` VALUES ('283', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:40:24');
INSERT INTO `sys_logininfor` VALUES ('284', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:45:25');
INSERT INTO `sys_logininfor` VALUES ('285', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:50:11');
INSERT INTO `sys_logininfor` VALUES ('286', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:53:33');
INSERT INTO `sys_logininfor` VALUES ('287', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 19:56:15');
INSERT INTO `sys_logininfor` VALUES ('288', 'admin', '127.0.0.1', '内网IP', 'Chrome 8', 'Windows 7', '0', '登录成功', '2022-09-13 20:19:14');
INSERT INTO `sys_logininfor` VALUES ('289', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码已失效', '2022-09-13 20:30:08');
INSERT INTO `sys_logininfor` VALUES ('290', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-13 20:30:13');
INSERT INTO `sys_logininfor` VALUES ('291', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 20:30:21');
INSERT INTO `sys_logininfor` VALUES ('292', 'admin', '192.168.43.1', '内网IP', 'Chrome Mobile', 'Android Mobile', '0', '登录成功', '2022-09-13 20:42:38');
INSERT INTO `sys_logininfor` VALUES ('293', 'admin', '192.168.43.1', '内网IP', 'Chrome Mobile', 'Android Mobile', '0', '登录成功', '2022-09-13 20:42:57');
INSERT INTO `sys_logininfor` VALUES ('294', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 20:46:30');
INSERT INTO `sys_logininfor` VALUES ('295', 'admin', '192.168.43.1', '内网IP', 'Chrome Mobile', 'Android Mobile', '0', '登录成功', '2022-09-13 21:01:59');
INSERT INTO `sys_logininfor` VALUES ('296', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-09-13 21:28:10');
INSERT INTO `sys_logininfor` VALUES ('297', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 22:14:46');
INSERT INTO `sys_logininfor` VALUES ('298', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 22:35:48');
INSERT INTO `sys_logininfor` VALUES ('299', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-13 22:36:44');
INSERT INTO `sys_logininfor` VALUES ('300', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 22:36:50');
INSERT INTO `sys_logininfor` VALUES ('301', 'admin', '192.168.0.103', '内网IP', 'Chrome Mobile', 'Android Mobile', '0', '登录成功', '2022-09-13 22:40:05');
INSERT INTO `sys_logininfor` VALUES ('302', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码已失效', '2022-09-13 22:40:28');
INSERT INTO `sys_logininfor` VALUES ('303', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 22:40:32');
INSERT INTO `sys_logininfor` VALUES ('304', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 22:41:13');
INSERT INTO `sys_logininfor` VALUES ('305', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 22:42:46');
INSERT INTO `sys_logininfor` VALUES ('306', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 22:48:42');
INSERT INTO `sys_logininfor` VALUES ('307', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 23:38:57');
INSERT INTO `sys_logininfor` VALUES ('308', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 23:41:55');
INSERT INTO `sys_logininfor` VALUES ('309', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 23:42:15');
INSERT INTO `sys_logininfor` VALUES ('310', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 23:43:43');
INSERT INTO `sys_logininfor` VALUES ('311', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 23:44:22');
INSERT INTO `sys_logininfor` VALUES ('312', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 23:45:21');
INSERT INTO `sys_logininfor` VALUES ('313', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 23:46:05');
INSERT INTO `sys_logininfor` VALUES ('314', 'admin', '192.168.0.102', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-13 23:47:03');
INSERT INTO `sys_logininfor` VALUES ('315', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码已失效', '2022-09-14 23:31:32');
INSERT INTO `sys_logininfor` VALUES ('316', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-14 23:31:41');
INSERT INTO `sys_logininfor` VALUES ('317', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-14 23:33:09');
INSERT INTO `sys_logininfor` VALUES ('318', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-14 23:33:14');
INSERT INTO `sys_logininfor` VALUES ('319', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-14 23:34:01');
INSERT INTO `sys_logininfor` VALUES ('320', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 19:03:03');
INSERT INTO `sys_logininfor` VALUES ('321', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 19:04:10');
INSERT INTO `sys_logininfor` VALUES ('322', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 19:04:32');
INSERT INTO `sys_logininfor` VALUES ('323', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 19:04:44');
INSERT INTO `sys_logininfor` VALUES ('324', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-15 19:06:25');
INSERT INTO `sys_logininfor` VALUES ('325', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 19:06:31');
INSERT INTO `sys_logininfor` VALUES ('326', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 19:06:41');
INSERT INTO `sys_logininfor` VALUES ('327', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-15 19:09:05');
INSERT INTO `sys_logininfor` VALUES ('328', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 19:09:10');
INSERT INTO `sys_logininfor` VALUES ('329', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '1', '验证码错误', '2022-09-15 19:10:02');
INSERT INTO `sys_logininfor` VALUES ('330', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 19:10:08');
INSERT INTO `sys_logininfor` VALUES ('331', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 20:34:27');
INSERT INTO `sys_logininfor` VALUES ('332', 'admin', '192.168.43.1', '内网IP', 'Chrome Mobile', 'Android Mobile', '0', '登录成功', '2022-09-15 20:58:10');
INSERT INTO `sys_logininfor` VALUES ('333', 'admin', '192.168.43.1', '内网IP', 'Chrome Mobile', 'Android Mobile', '0', '登录成功', '2022-09-15 20:58:54');
INSERT INTO `sys_logininfor` VALUES ('334', 'admin', '192.168.43.1', '内网IP', 'Chrome Mobile', 'Android Mobile', '0', '登录成功', '2022-09-15 20:59:05');
INSERT INTO `sys_logininfor` VALUES ('335', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 23:01:17');
INSERT INTO `sys_logininfor` VALUES ('336', 'admin', '127.0.0.1', '内网IP', 'Mobile Safari', 'iOS 11 (iPhone)', '0', '登录成功', '2022-09-15 23:02:05');
INSERT INTO `sys_logininfor` VALUES ('337', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-15 12:50:59');
INSERT INTO `sys_logininfor` VALUES ('338', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-24 21:08:59');
INSERT INTO `sys_logininfor` VALUES ('339', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-25 17:06:39');
INSERT INTO `sys_logininfor` VALUES ('340', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '1', '验证码错误', '2022-10-25 18:48:46');
INSERT INTO `sys_logininfor` VALUES ('341', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-25 18:48:50');
INSERT INTO `sys_logininfor` VALUES ('342', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-25 20:44:48');
INSERT INTO `sys_logininfor` VALUES ('343', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-25 20:44:56');
INSERT INTO `sys_logininfor` VALUES ('344', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-25 21:33:10');
INSERT INTO `sys_logininfor` VALUES ('345', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-25 21:33:15');
INSERT INTO `sys_logininfor` VALUES ('346', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-26 14:03:38');
INSERT INTO `sys_logininfor` VALUES ('347', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-26 14:06:09');
INSERT INTO `sys_logininfor` VALUES ('348', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-26 14:13:01');
INSERT INTO `sys_logininfor` VALUES ('349', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-26 17:15:45');
INSERT INTO `sys_logininfor` VALUES ('350', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-26 19:02:40');
INSERT INTO `sys_logininfor` VALUES ('351', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-26 20:07:48');
INSERT INTO `sys_logininfor` VALUES ('352', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-26 21:04:54');
INSERT INTO `sys_logininfor` VALUES ('353', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-26 21:05:16');
INSERT INTO `sys_logininfor` VALUES ('354', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-26 23:44:27');
INSERT INTO `sys_logininfor` VALUES ('355', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-27 17:05:49');
INSERT INTO `sys_logininfor` VALUES ('356', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-27 18:27:05');
INSERT INTO `sys_logininfor` VALUES ('357', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-27 18:50:03');
INSERT INTO `sys_logininfor` VALUES ('358', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-27 18:55:44');
INSERT INTO `sys_logininfor` VALUES ('359', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-27 19:58:28');
INSERT INTO `sys_logininfor` VALUES ('360', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-27 21:58:36');
INSERT INTO `sys_logininfor` VALUES ('361', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-28 12:44:04');
INSERT INTO `sys_logininfor` VALUES ('362', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-28 21:14:41');
INSERT INTO `sys_logininfor` VALUES ('363', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-28 23:47:39');
INSERT INTO `sys_logininfor` VALUES ('364', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-29 00:41:06');
INSERT INTO `sys_logininfor` VALUES ('365', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-29 11:55:30');
INSERT INTO `sys_logininfor` VALUES ('366', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-29 13:55:43');
INSERT INTO `sys_logininfor` VALUES ('367', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-29 15:14:32');
INSERT INTO `sys_logininfor` VALUES ('368', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-29 16:32:27');
INSERT INTO `sys_logininfor` VALUES ('369', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-29 16:33:49');
INSERT INTO `sys_logininfor` VALUES ('370', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-29 17:36:15');
INSERT INTO `sys_logininfor` VALUES ('371', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-29 18:27:58');
INSERT INTO `sys_logininfor` VALUES ('372', 'zjh', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-29 18:28:07');
INSERT INTO `sys_logininfor` VALUES ('373', 'zjh', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-29 18:28:16');
INSERT INTO `sys_logininfor` VALUES ('374', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-29 18:28:21');
INSERT INTO `sys_logininfor` VALUES ('375', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-30 12:31:22');
INSERT INTO `sys_logininfor` VALUES ('376', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-30 14:23:42');
INSERT INTO `sys_logininfor` VALUES ('377', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-30 15:29:19');
INSERT INTO `sys_logininfor` VALUES ('378', 'dev', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-30 15:29:24');
INSERT INTO `sys_logininfor` VALUES ('379', 'dev', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-30 16:32:42');
INSERT INTO `sys_logininfor` VALUES ('380', 'dev', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-30 16:32:50');
INSERT INTO `sys_logininfor` VALUES ('381', 'dev', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '退出成功', '2022-10-30 16:32:54');
INSERT INTO `sys_logininfor` VALUES ('382', 'admin', '127.0.0.1', '内网IP', 'Chrome 10', 'Windows 10', '0', '登录成功', '2022-10-30 16:33:03');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2050 DEFAULT CHARSET=utf8 COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '1', 'system', null, '', '1', '0', 'M', '0', '0', '', 'system', 'admin', '2022-03-21 21:33:12', '', null, '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '2', 'monitor', null, '', '1', '0', 'M', '0', '0', '', 'monitor', 'admin', '2022-03-21 21:33:12', '', null, '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '3', 'tool', null, '', '1', '0', 'M', '0', '0', '', 'tool', 'admin', '2022-03-21 21:33:12', '', null, '系统工具目录');
INSERT INTO `sys_menu` VALUES ('4', 'liefox官网', '0', '1024', 'http://cxq21.gitee.io', null, '', '0', '0', 'M', '0', '0', '', 'guide', 'admin', '2022-03-21 21:33:12', 'admin', '2022-10-27 22:41:04', 'liefox官网地址');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', '1', 'user', 'system/user/index', '', '1', '0', 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-03-21 21:33:12', '', null, '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1', '2', 'role', 'system/role/index', '', '1', '0', 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-03-21 21:33:12', '', null, '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '1', '3', 'menu', 'system/menu/index', '', '1', '0', 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-03-21 21:33:12', '', null, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', '4', 'dept', 'system/dept/index', '', '1', '0', 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-03-21 21:33:12', '', null, '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('104', '岗位管理', '1', '5', 'post', 'system/post/index', '', '1', '0', 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-03-21 21:33:12', '', null, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '字典管理', '1', '6', 'dict', 'system/dict/index', '', '1', '0', 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-03-21 21:33:12', '', null, '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '参数设置', '1', '7', 'config', 'system/config/index', '', '1', '0', 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-03-21 21:33:12', '', null, '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', '8', 'notice', 'system/notice/index', '', '1', '0', 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-03-21 21:33:12', '', null, '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '日志管理', '1', '9', 'log', '', '', '1', '0', 'M', '0', '0', '', 'log', 'admin', '2022-03-21 21:33:12', '', null, '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '2', '1', 'online', 'monitor/online/index', '', '1', '0', 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-03-21 21:33:12', '', null, '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('110', '定时任务', '2', '2', 'job', 'monitor/job/index', '', '1', '0', 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-03-21 21:33:12', '', null, '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('111', '数据监控', '2', '3', 'druid', 'monitor/druid/index', '', '1', '0', 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-03-21 21:33:12', '', null, '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('112', '服务监控', '2', '4', 'server', 'monitor/server/index', '', '1', '0', 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-03-21 21:33:12', '', null, '服务监控菜单');
INSERT INTO `sys_menu` VALUES ('113', '缓存监控', '2', '5', 'cache', 'monitor/cache/index', '', '1', '0', 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-03-21 21:33:12', '', null, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES ('114', '表单构建', '3', '1', 'build', 'tool/build/index', '', '1', '0', 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-03-21 21:33:12', '', null, '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('115', '代码生成', '3', '2', 'gen', 'tool/gen/index', '', '1', '0', 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-03-21 21:33:12', '', null, '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('116', '系统接口', '3', '3', 'swagger', 'tool/swagger/index', '', '1', '0', 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-03-21 21:33:12', '', null, '系统接口菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', '1', 'operlog', 'monitor/operlog/index', '', '1', '0', 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-03-21 21:33:12', '', null, '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', '2', 'logininfor', 'monitor/logininfor/index', '', '1', '0', 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-03-21 21:33:12', '', null, '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1001', '用户查询', '100', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1002', '用户新增', '100', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1003', '用户修改', '100', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1004', '用户删除', '100', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1005', '用户导出', '100', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1006', '用户导入', '100', '6', '', '', '', '1', '0', 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1007', '重置密码', '100', '7', '', '', '', '1', '0', 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1008', '角色查询', '101', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1009', '角色新增', '101', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1010', '角色修改', '101', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1011', '角色删除', '101', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1012', '角色导出', '101', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单查询', '102', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单新增', '102', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1015', '菜单修改', '102', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1016', '菜单删除', '102', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1017', '部门查询', '103', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1018', '部门新增', '103', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1019', '部门修改', '103', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1020', '部门删除', '103', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位查询', '104', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位新增', '104', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位修改', '104', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1024', '岗位删除', '104', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1025', '岗位导出', '104', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1026', '字典查询', '105', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1027', '字典新增', '105', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1028', '字典修改', '105', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1029', '字典删除', '105', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1030', '字典导出', '105', '5', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1031', '参数查询', '106', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1032', '参数新增', '106', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1033', '参数修改', '106', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1034', '参数删除', '106', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1035', '参数导出', '106', '5', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1036', '公告查询', '107', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1037', '公告新增', '107', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1038', '公告修改', '107', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1039', '公告删除', '107', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1040', '操作查询', '500', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1041', '操作删除', '500', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1042', '日志导出', '500', '4', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1043', '登录查询', '501', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1044', '登录删除', '501', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1045', '日志导出', '501', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1046', '在线查询', '109', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1047', '批量强退', '109', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1048', '单条强退', '109', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1049', '任务查询', '110', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1050', '任务新增', '110', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1051', '任务修改', '110', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1052', '任务删除', '110', '4', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1053', '状态修改', '110', '5', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1054', '任务导出', '110', '7', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1055', '生成查询', '115', '1', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1056', '生成修改', '115', '2', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1057', '生成删除', '115', '3', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1058', '导入代码', '115', '2', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1059', '预览代码', '115', '4', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('1060', '生成代码', '115', '5', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('2015', '图书管理', '0', '3', 'bookM', null, null, '1', '0', 'M', '0', '0', '', 'education', 'admin', '2022-03-22 15:43:42', 'admin', '2022-05-02 15:49:29', '');
INSERT INTO `sys_menu` VALUES ('2016', '图书管理', '2015', '1', 'books', 'bookM/books/index', null, '1', '1', 'C', '0', '0', 'bookM:books:list', '#', 'admin', '2022-03-22 15:49:34', 'admin', '2022-05-02 19:12:55', '图书管理菜单');
INSERT INTO `sys_menu` VALUES ('2017', '图书管理查询', '2016', '1', '#', '', null, '1', '0', 'F', '0', '0', 'bookM:books:query', '#', 'admin', '2022-03-22 15:49:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('2018', '图书管理新增', '2016', '2', '#', '', null, '1', '0', 'F', '0', '0', 'bookM:books:add', '#', 'admin', '2022-03-22 15:49:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('2019', '图书管理修改', '2016', '3', '#', '', null, '1', '0', 'F', '0', '0', 'bookM:books:edit', '#', 'admin', '2022-03-22 15:49:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('2020', '图书管理删除', '2016', '4', '#', '', null, '1', '0', 'F', '0', '0', 'bookM:books:remove', '#', 'admin', '2022-03-22 15:49:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('2021', '图书管理导出', '2016', '5', '#', '', null, '1', '0', 'F', '0', '0', 'bookM:books:export', '#', 'admin', '2022-03-22 15:49:34', '', null, '');
INSERT INTO `sys_menu` VALUES ('2022', '作者管理', '0', '3', 'writerM', null, null, '1', '0', 'M', '0', '0', '', 'peoples', 'admin', '2022-04-25 21:26:24', 'admin', '2022-05-02 15:49:39', '');
INSERT INTO `sys_menu` VALUES ('2023', '作者管理', '2022', '1', 'writer', 'writer/writer/index', null, '1', '0', 'C', '0', '0', 'writer:writer:list', '#', 'admin', '2022-04-25 21:28:35', '', null, '作者管理菜单');
INSERT INTO `sys_menu` VALUES ('2024', '作者管理查询', '2023', '1', '#', '', null, '1', '0', 'F', '0', '0', 'writer:writer:query', '#', 'admin', '2022-04-25 21:28:36', '', null, '');
INSERT INTO `sys_menu` VALUES ('2025', '作者管理新增', '2023', '2', '#', '', null, '1', '0', 'F', '0', '0', 'writer:writer:add', '#', 'admin', '2022-04-25 21:28:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2026', '作者管理修改', '2023', '3', '#', '', null, '1', '0', 'F', '0', '0', 'writer:writer:edit', '#', 'admin', '2022-04-25 21:28:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2027', '作者管理删除', '2023', '4', '#', '', null, '1', '0', 'F', '0', '0', 'writer:writer:remove', '#', 'admin', '2022-04-25 21:28:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2028', '作者管理导出', '2023', '5', '#', '', null, '1', '0', 'F', '0', '0', 'writer:writer:export', '#', 'admin', '2022-04-25 21:28:38', '', null, '');
INSERT INTO `sys_menu` VALUES ('2029', '前台幻灯片管理', '0', '3', 'ppt', null, null, '1', '0', 'M', '0', '0', '', 'clipboard', 'admin', '2022-07-24 18:22:14', 'admin', '2022-07-24 19:24:03', '');
INSERT INTO `sys_menu` VALUES ('2030', '前台幻灯片', '2029', '1', 'ppt', 'ppt/ppt/index', null, '1', '0', 'C', '0', '0', 'ppt:ppt:list', '#', 'admin', '2022-07-24 18:28:30', 'admin', '2022-07-24 19:24:18', '前台首页幻灯片菜单');
INSERT INTO `sys_menu` VALUES ('2031', '前台首页幻灯片查询', '2030', '1', '#', '', null, '1', '0', 'F', '0', '0', 'ppt:ppt:query', '#', 'admin', '2022-07-24 18:28:41', '', null, '');
INSERT INTO `sys_menu` VALUES ('2032', '前台首页幻灯片新增', '2030', '2', '#', '', null, '1', '0', 'F', '0', '0', 'ppt:ppt:add', '#', 'admin', '2022-07-24 18:28:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2033', '前台首页幻灯片修改', '2030', '3', '#', '', null, '1', '0', 'F', '0', '0', 'ppt:ppt:edit', '#', 'admin', '2022-07-24 18:28:48', '', null, '');
INSERT INTO `sys_menu` VALUES ('2034', '前台首页幻灯片删除', '2030', '4', '#', '', null, '1', '0', 'F', '0', '0', 'ppt:ppt:remove', '#', 'admin', '2022-07-24 18:28:51', '', null, '');
INSERT INTO `sys_menu` VALUES ('2035', '前台首页幻灯片导出', '2030', '5', '#', '', null, '1', '0', 'F', '0', '0', 'ppt:ppt:export', '#', 'admin', '2022-07-24 18:28:54', '', null, '');
INSERT INTO `sys_menu` VALUES ('2036', '评论管理', '0', '124', 'comment', null, null, '1', '0', 'M', '0', '0', '', 'message', 'admin', '2022-10-27 22:07:41', 'admin', '2022-10-27 22:41:14', '');
INSERT INTO `sys_menu` VALUES ('2037', '评论', '2036', '1', 'comment', 'comment/comment/index', null, '1', '0', 'C', '0', '0', 'comment:comment:list', '#', 'admin', '2022-10-27 22:08:45', '', null, '评论菜单');
INSERT INTO `sys_menu` VALUES ('2038', '评论查询', '2037', '1', '#', '', null, '1', '0', 'F', '0', '0', 'comment:comment:query', '#', 'admin', '2022-10-27 22:08:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('2039', '评论新增', '2037', '2', '#', '', null, '1', '0', 'F', '0', '0', 'comment:comment:add', '#', 'admin', '2022-10-27 22:08:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('2040', '评论修改', '2037', '3', '#', '', null, '1', '0', 'F', '0', '0', 'comment:comment:edit', '#', 'admin', '2022-10-27 22:08:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('2041', '评论删除', '2037', '4', '#', '', null, '1', '0', 'F', '0', '0', 'comment:comment:remove', '#', 'admin', '2022-10-27 22:08:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('2042', '评论导出', '2037', '5', '#', '', null, '1', '0', 'F', '0', '0', 'comment:comment:export', '#', 'admin', '2022-10-27 22:08:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('2043', '话题管理', '0', '4', 'topic', null, null, '1', '0', 'M', '0', '0', null, 'documentation', 'admin', '2022-10-29 19:59:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2044', '话题', '2043', '1', 'topic', 'topic/topic/index', null, '1', '0', 'C', '0', '0', 'topic:topic:list', '#', 'admin', '2022-10-29 20:01:23', '', null, '话题菜单');
INSERT INTO `sys_menu` VALUES ('2045', '话题查询', '2044', '1', '#', '', null, '1', '0', 'F', '0', '0', 'topic:topic:query', '#', 'admin', '2022-10-29 20:01:23', '', null, '');
INSERT INTO `sys_menu` VALUES ('2046', '话题新增', '2044', '2', '#', '', null, '1', '0', 'F', '0', '0', 'topic:topic:add', '#', 'admin', '2022-10-29 20:01:23', '', null, '');
INSERT INTO `sys_menu` VALUES ('2047', '话题修改', '2044', '3', '#', '', null, '1', '0', 'F', '0', '0', 'topic:topic:edit', '#', 'admin', '2022-10-29 20:01:23', '', null, '');
INSERT INTO `sys_menu` VALUES ('2048', '话题删除', '2044', '4', '#', '', null, '1', '0', 'F', '0', '0', 'topic:topic:remove', '#', 'admin', '2022-10-29 20:01:23', '', null, '');
INSERT INTO `sys_menu` VALUES ('2049', '话题导出', '2044', '5', '#', '', null, '1', '0', 'F', '0', '0', 'topic:topic:export', '#', 'admin', '2022-10-29 20:01:23', '', null, '');

-- ----------------------------
-- Table structure for sys_notice
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(50) NOT NULL COMMENT '公告标题',
  `notice_type` char(1) NOT NULL COMMENT '公告类型（1通知 2公告）',
  `notice_content` longblob COMMENT '公告内容',
  `status` char(1) DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`notice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('1', '温馨提醒：2018-07-01 liefox新版本发布啦', '2', 0xE696B0E78988E69CACE58685E5AEB9, '0', 'admin', '2022-03-21 21:33:13', '', null, '管理员');
INSERT INTO `sys_notice` VALUES ('2', '维护通知：2018-07-01 liefox系统凌晨维护', '1', 0xE7BBB4E68AA4E58685E5AEB9, '0', 'admin', '2022-03-21 21:33:13', '', null, '管理员');

-- ----------------------------
-- Table structure for sys_oper_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`)
) ENGINE=InnoDB AUTO_INCREMENT=499 DEFAULT CHARSET=utf8 COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('100', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"1\",\"menuName\":\"书籍管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"bookm\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:08:03');
INSERT INTO `sys_oper_log` VALUES ('101', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"build\",\"orderNum\":\"1\",\"menuName\":\"书籍管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"books\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:08:51');
INSERT INTO `sys_oper_log` VALUES ('102', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_books,asq_writer', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:09:06');
INSERT INTO `sys_oper_log` VALUES ('103', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":true,\"subTableName\":\"asq_writer\",\"functionAuthor\":\"拾柒\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647911346000,\"tableId\":1,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterId\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者id\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647911346000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"book_writer_id\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647911346000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"create', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:11:54');
INSERT INTO `sys_oper_log` VALUES ('104', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', '1', 'admin', null, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"书籍类型\",\"params\":{},\"dictType\":\"book_type\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:12:42');
INSERT INTO `sys_oper_log` VALUES ('105', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"Java\",\"listClass\":\"success\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_type\",\"dictLabel\":\"Java\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:13:22');
INSERT INTO `sys_oper_log` VALUES ('106', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"Vue\",\"listClass\":\"success\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_type\",\"dictLabel\":\"Vue\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:13:39');
INSERT INTO `sys_oper_log` VALUES ('107', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"生活\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_type\",\"dictLabel\":\"生活\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:13:55');
INSERT INTO `sys_oper_log` VALUES ('108', '字典数据', '2', 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"生活\",\"listClass\":\"success\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_type\",\"dictLabel\":\"生活\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1647911635000,\"dictCode\":102,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:14:01');
INSERT INTO `sys_oper_log` VALUES ('109', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":true,\"subTableName\":\"asq_writer\",\"functionAuthor\":\"拾柒\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1647911514000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647911346000,\"tableId\":1,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterId\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者id\",\"isQuery\":\"1\",\"updateTime\":1647911514000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647911346000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"book_writer_id\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"updateTime\":1647911514000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647911346000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"book_type\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"updateTime\":1', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:14:23');
INSERT INTO `sys_oper_log` VALUES ('110', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"shiqi\",\"columns\":[{\"capJavaField\":\"WriterId\",\"usableColumn\":false,\"columnId\":9,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647911346000,\"tableId\":2,\"pk\":true,\"columnName\":\"writer_id\"},{\"capJavaField\":\"WriterName\",\"usableColumn\":false,\"columnId\":10,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"作者姓名\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647911346000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"writer_name\"},{\"capJavaField\":\"WriterInfo\",\"usableColumn\":false,\"columnId\":11,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerInfo\",\"htmlType\":\"editor\",\"edit\":true,\"query\":false,\"columnComment\":\"作者详情\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647911346000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"writer_info\"},{\"capJavaField\":\"WriterIntro\",\"usableColumn\":false,\"columnId\":12,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerIntro\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"作者座右铭\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createT', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:15:30');
INSERT INTO `sys_oper_log` VALUES ('111', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":true,\"subTableName\":\"asq_writer\",\"functionAuthor\":\"拾柒\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1647911663000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647911346000,\"tableId\":1,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterId\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者id\",\"isQuery\":\"1\",\"updateTime\":1647911663000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647911346000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"book_writer_id\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"updateTime\":1647911663000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647911346000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"book_type\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"updateTime\":1', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:17:10');
INSERT INTO `sys_oper_log` VALUES ('112', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-03-22 09:17:15');
INSERT INTO `sys_oper_log` VALUES ('113', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-03-22 09:17:22');
INSERT INTO `sys_oper_log` VALUES ('114', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2002', '127.0.0.1', '内网IP', '{menuId=2002}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', '0', null, '2022-03-22 09:55:22');
INSERT INTO `sys_oper_log` VALUES ('115', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2002', '127.0.0.1', '内网IP', '{menuId=2002}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', '0', null, '2022-03-22 09:55:29');
INSERT INTO `sys_oper_log` VALUES ('116', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2007', '127.0.0.1', '内网IP', '{menuId=2007}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:55:33');
INSERT INTO `sys_oper_log` VALUES ('117', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2006', '127.0.0.1', '内网IP', '{menuId=2006}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:55:36');
INSERT INTO `sys_oper_log` VALUES ('118', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2005', '127.0.0.1', '内网IP', '{menuId=2005}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:55:38');
INSERT INTO `sys_oper_log` VALUES ('119', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2004', '127.0.0.1', '内网IP', '{menuId=2004}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:55:41');
INSERT INTO `sys_oper_log` VALUES ('120', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2003', '127.0.0.1', '内网IP', '{menuId=2003}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:55:46');
INSERT INTO `sys_oper_log` VALUES ('121', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2002', '127.0.0.1', '内网IP', '{menuId=2002}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:55:49');
INSERT INTO `sys_oper_log` VALUES ('122', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2001', '127.0.0.1', '内网IP', '{menuId=2001}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:55:53');
INSERT INTO `sys_oper_log` VALUES ('123', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2000', '127.0.0.1', '内网IP', '{menuId=2000}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 09:56:29');
INSERT INTO `sys_oper_log` VALUES ('124', '代码生成', '3', 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', '1', 'admin', null, '/tool/gen/2', '127.0.0.1', '内网IP', '{tableIds=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:02:42');
INSERT INTO `sys_oper_log` VALUES ('125', '代码生成', '3', 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', '1', 'admin', null, '/tool/gen/1', '127.0.0.1', '内网IP', '{tableIds=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:02:49');
INSERT INTO `sys_oper_log` VALUES ('126', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', '1', 'admin', null, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', 'admin123 199513gG', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:52:48');
INSERT INTO `sys_oper_log` VALUES ('127', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_books', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:54:20');
INSERT INTO `sys_oper_log` VALUES ('128', '字典类型', '3', 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', '1', 'admin', null, '/system/dict/type/100', '127.0.0.1', '内网IP', '{dictIds=100}', 'null', '1', '书籍类型已分配,不能删除', '2022-03-22 10:55:01');
INSERT INTO `sys_oper_log` VALUES ('129', '字典类型', '3', 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', '1', 'admin', null, '/system/dict/type/100', '127.0.0.1', '内网IP', '{dictIds=100}', 'null', '1', '书籍类型已分配,不能删除', '2022-03-22 10:55:09');
INSERT INTO `sys_oper_log` VALUES ('130', '字典类型', '9', 'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()', 'DELETE', '1', 'admin', null, '/system/dict/type/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:55:12');
INSERT INTO `sys_oper_log` VALUES ('131', '字典类型', '9', 'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()', 'DELETE', '1', 'admin', null, '/system/dict/type/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:55:13');
INSERT INTO `sys_oper_log` VALUES ('132', '字典类型', '3', 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', '1', 'admin', null, '/system/dict/type/100', '127.0.0.1', '内网IP', '{dictIds=100}', 'null', '1', '书籍类型已分配,不能删除', '2022-03-22 10:55:15');
INSERT INTO `sys_oper_log` VALUES ('133', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":14,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647917660000,\"tableId\":3,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterName\",\"usableColumn\":false,\"columnId\":15,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者姓名\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_writer_name\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":16,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":17,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(20)\",\"createBy\":\"admi', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:56:06');
INSERT INTO `sys_oper_log` VALUES ('134', '字典类型', '3', 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', '1', 'admin', null, '/system/dict/type/100', '127.0.0.1', '内网IP', '{dictIds=100}', 'null', '1', '书籍类型已分配,不能删除', '2022-03-22 10:56:12');
INSERT INTO `sys_oper_log` VALUES ('135', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":14,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1647917766000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647917660000,\"tableId\":3,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterName\",\"usableColumn\":false,\"columnId\":15,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者姓名\",\"isQuery\":\"1\",\"updateTime\":1647917766000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_writer_name\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":16,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"updateTime\":1647917766000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":17,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"book_type\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"updateTime\":1647917766', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:57:49');
INSERT INTO `sys_oper_log` VALUES ('136', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"1\",\"menuName\":\"图书管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"bookm\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:58:28');
INSERT INTO `sys_oper_log` VALUES ('137', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":14,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1647917869000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647917660000,\"tableId\":3,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterName\",\"usableColumn\":false,\"columnId\":15,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者姓名\",\"isQuery\":\"1\",\"updateTime\":1647917869000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_writer_name\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":16,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"updateTime\":1647917869000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":17,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"book_type\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"updateTime\":1647917869', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 10:58:43');
INSERT INTO `sys_oper_log` VALUES ('138', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-03-22 10:58:46');
INSERT INTO `sys_oper_log` VALUES ('139', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2008', '127.0.0.1', '内网IP', '{menuId=2008}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', '0', null, '2022-03-22 13:05:58');
INSERT INTO `sys_oper_log` VALUES ('140', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2009', '127.0.0.1', '内网IP', '{menuId=2009}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', '0', null, '2022-03-22 13:06:02');
INSERT INTO `sys_oper_log` VALUES ('141', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2010', '127.0.0.1', '内网IP', '{menuId=2010}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 13:06:08');
INSERT INTO `sys_oper_log` VALUES ('142', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2011', '127.0.0.1', '内网IP', '{menuId=2011}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 13:06:10');
INSERT INTO `sys_oper_log` VALUES ('143', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2012', '127.0.0.1', '内网IP', '{menuId=2012}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 13:06:12');
INSERT INTO `sys_oper_log` VALUES ('144', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2013', '127.0.0.1', '内网IP', '{menuId=2013}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 13:06:14');
INSERT INTO `sys_oper_log` VALUES ('145', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2014', '127.0.0.1', '内网IP', '{menuId=2014}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 13:06:17');
INSERT INTO `sys_oper_log` VALUES ('146', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2009', '127.0.0.1', '内网IP', '{menuId=2009}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 13:06:20');
INSERT INTO `sys_oper_log` VALUES ('147', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2008', '127.0.0.1', '内网IP', '{menuId=2008}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 13:06:22');
INSERT INTO `sys_oper_log` VALUES ('148', '字典类型', '3', 'com.ruoyi.web.controller.system.SysDictTypeController.remove()', 'DELETE', '1', 'admin', null, '/system/dict/type/100', '127.0.0.1', '内网IP', '{dictIds=100}', 'null', '1', '书籍类型已分配,不能删除', '2022-03-22 13:06:32');
INSERT INTO `sys_oper_log` VALUES ('149', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"1\",\"menuName\":\"图书管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"bookM\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 15:43:42');
INSERT INTO `sys_oper_log` VALUES ('150', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":14,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1647917923000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647917660000,\"tableId\":3,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterName\",\"usableColumn\":false,\"columnId\":15,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者姓名\",\"isQuery\":\"1\",\"updateTime\":1647917923000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_writer_name\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":16,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"updateTime\":1647917923000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":17,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"book_type\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"updateTime\":1647917923', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 15:45:52');
INSERT INTO `sys_oper_log` VALUES ('151', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-03-22 15:46:16');
INSERT INTO `sys_oper_log` VALUES ('152', '图书管理', '1', 'com.ruoyi.controller.AsqBooksController.add()', 'POST', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"拾柒工作室\",\"params\":{},\"bookCover\":\"/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg\",\"bookName\":\"前后端分离\",\"bookId\":1,\"bookPubDate\":1647878400000,\"bookUpDate\":1647878400000,\"bookWriterName\":\"拾柒图书\",\"bookType\":\"Java\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 16:12:03');
INSERT INTO `sys_oper_log` VALUES ('153', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_writer', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 21:22:42');
INSERT INTO `sys_oper_log` VALUES ('154', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"liefox\",\"columns\":[{\"capJavaField\":\"WriterId\",\"usableColumn\":false,\"columnId\":22,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1650892962000,\"tableId\":4,\"pk\":true,\"columnName\":\"writer_id\"},{\"capJavaField\":\"WriterName\",\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"作者姓名\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1650892962000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"writer_name\"},{\"capJavaField\":\"WriterInfo\",\"usableColumn\":false,\"columnId\":24,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerInfo\",\"htmlType\":\"editor\",\"edit\":true,\"query\":false,\"columnComment\":\"作者详情\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1650892962000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"writer_info\"},{\"capJavaField\":\"WriterIntro\",\"usableColumn\":false,\"columnId\":25,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerIntro\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"作者座右铭\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"creat', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 21:24:51');
INSERT INTO `sys_oper_log` VALUES ('155', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"peoples\",\"orderNum\":\"2\",\"menuName\":\"作者管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"writerM\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 21:26:24');
INSERT INTO `sys_oper_log` VALUES ('156', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"liefox\",\"columns\":[{\"capJavaField\":\"WriterId\",\"usableColumn\":false,\"columnId\":22,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1650893091000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1650892962000,\"tableId\":4,\"pk\":true,\"columnName\":\"writer_id\"},{\"capJavaField\":\"WriterName\",\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"作者姓名\",\"isQuery\":\"1\",\"updateTime\":1650893091000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1650892962000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"writer_name\"},{\"capJavaField\":\"WriterInfo\",\"usableColumn\":false,\"columnId\":24,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerInfo\",\"htmlType\":\"editor\",\"edit\":true,\"query\":false,\"columnComment\":\"作者详情\",\"updateTime\":1650893091000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1650892962000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"writer_info\"},{\"capJavaField\":\"WriterIntro\",\"usableColumn\":false,\"columnId\":25,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerIntro\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"作者座右铭\",\"updateTime\":1650893091000,\"sort\":4,\"list\":true,\"para', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 21:26:58');
INSERT INTO `sys_oper_log` VALUES ('157', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-04-25 21:27:03');
INSERT INTO `sys_oper_log` VALUES ('158', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-04-25 21:27:21');
INSERT INTO `sys_oper_log` VALUES ('159', '作者管理', '1', 'com.ruoyi.controller.AsqWriterController.add()', 'POST', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"writerIntro\":\"千里马常有，而伯乐。。。。\",\"params\":{},\"writerName\":\"拾柒图书1\",\"writerId\":1,\"writerInfo\":\"<p><img src=\\\"/dev-api/profile/upload/2022/04/25/c716b837-2e1c-4b16-8c67-4510c652111f.jpg\\\"></p><p>我是拾柒图书1111</p>\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 21:46:46');
INSERT INTO `sys_oper_log` VALUES ('160', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"liefox\",\"columns\":[{\"capJavaField\":\"WriterId\",\"usableColumn\":false,\"columnId\":22,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1650893218000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1650892962000,\"tableId\":4,\"pk\":true,\"columnName\":\"writer_id\"},{\"capJavaField\":\"WriterName\",\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"作者姓名\",\"isQuery\":\"1\",\"updateTime\":1650893218000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1650892962000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"writer_name\"},{\"capJavaField\":\"WriterInfo\",\"usableColumn\":false,\"columnId\":24,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerInfo\",\"htmlType\":\"editor\",\"edit\":true,\"query\":false,\"columnComment\":\"作者详情\",\"updateTime\":1650893218000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1650892962000,\"isEdit\":\"1\",\"tableId\":4,\"pk\":false,\"columnName\":\"writer_info\"},{\"capJavaField\":\"WriterIntro\",\"usableColumn\":false,\"columnId\":25,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"writerIntro\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"作者座右铭\",\"updateTime\":1650893218000,\"sort\":4,\"list\":true,\"para', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 21:47:32');
INSERT INTO `sys_oper_log` VALUES ('161', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"writerIntro\":\"千里马常有，而伯乐。。。。\",\"params\":{},\"writerPic\":\"/profile/upload/2022/04/25/4124bc61-dfc5-42d3-8bab-c52b3b549966.jpeg\",\"writerName\":\"拾柒图书1\",\"writerId\":1,\"writerInfo\":\"<p><img src=\\\"/dev-api/profile/upload/2022/04/25/c716b837-2e1c-4b16-8c67-4510c652111f.jpg\\\"></p><p>我是拾柒图书1111</p>\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 21:50:14');
INSERT INTO `sys_oper_log` VALUES ('162', '作者管理', '1', 'com.ruoyi.controller.AsqWriterController.add()', 'POST', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerPic\":\"/profile/upload/2022/04/25/b5d30292-985b-42bf-9f7e-49c7ed0e298d.jpeg\",\"writerName\":\"liefox\",\"writerId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 23:18:49');
INSERT INTO `sys_oper_log` VALUES ('163', '图书管理', '2', 'com.ruoyi.controller.AsqBooksController.edit()', 'PUT', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"拾柒工作室\",\"params\":{},\"bookCover\":\"/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg\",\"bookName\":\"前后端分离\",\"bookId\":1,\"bookPubDate\":1647878400000,\"bookUpDate\":1647878400000,\"bookWriterName\":\"liefox\",\"bookType\":\"Java\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-25 23:41:58');
INSERT INTO `sys_oper_log` VALUES ('164', '作者管理', '1', 'com.ruoyi.controller.AsqWriterController.add()', 'POST', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"writerIntro\":\"你好\",\"params\":{},\"writerPic\":\"/profile/upload/2022/04/27/e35fd0f9-4e96-4346-9ab9-0d2bf8027ccb.jpg\",\"writerName\":\"lieos\",\"writerId\":3,\"writerInfo\":\"<p>你好</p>\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-27 11:45:03');
INSERT INTO `sys_oper_log` VALUES ('165', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"writerIntro\":\"你好\",\"params\":{},\"writerPic\":\"/profile/upload/2022/04/27/e35fd0f9-4e96-4346-9ab9-0d2bf8027ccb.jpg\",\"writerName\":\"左\",\"writerId\":3,\"writerInfo\":\"<p>你好</p>\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-27 11:46:33');
INSERT INTO `sys_oper_log` VALUES ('166', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"writerIntro\":\"你好\",\"params\":{},\"writerPic\":\"/profile/upload/2022/04/27/e35fd0f9-4e96-4346-9ab9-0d2bf8027ccb.jpg\",\"writerName\":\"leisure\",\"writerId\":3,\"writerInfo\":\"<p>你好</p>\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-04-27 11:47:15');
INSERT INTO `sys_oper_log` VALUES ('167', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updatePwd()', 'PUT', '1', 'admin', null, '/system/user/profile/updatePwd', '127.0.0.1', '内网IP', '199513gG admin123', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 14:58:06');
INSERT INTO `sys_oper_log` VALUES ('168', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"education\",\"orderNum\":\"3\",\"menuName\":\"图书管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"bookM\",\"children\":[],\"createTime\":1647935022000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2015,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 15:49:29');
INSERT INTO `sys_oper_log` VALUES ('169', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"peoples\",\"orderNum\":\"3\",\"menuName\":\"作者管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"writerM\",\"children\":[],\"createTime\":1650893184000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2022,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 15:49:39');
INSERT INTO `sys_oper_log` VALUES ('170', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerPic\":\"/profile/upload/2022/04/25/b5d30292-985b-42bf-9f7e-49c7ed0e298d.jpeg\",\"writerName\":\"你好\",\"writerId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 16:07:18');
INSERT INTO `sys_oper_log` VALUES ('171', '作者管理', '1', 'com.ruoyi.controller.AsqWriterController.add()', 'POST', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerName\":\"萨达\",\"writerId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 16:07:42');
INSERT INTO `sys_oper_log` VALUES ('172', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerPic\":\"/profile/upload/2022/05/02/7cb2225f-bc7f-4757-928a-19f2e30f3e2a.jpg\",\"writerName\":\"萨达\",\"writerId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 16:09:39');
INSERT INTO `sys_oper_log` VALUES ('173', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', '1', 'admin', null, '/tool/gen/synchDb/asq_books', '127.0.0.1', '内网IP', '{tableName=asq_books}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:01:49');
INSERT INTO `sys_oper_log` VALUES ('174', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":14,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1647935152000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647917660000,\"tableId\":3,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterName\",\"usableColumn\":false,\"columnId\":15,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者姓名\",\"isQuery\":\"1\",\"updateTime\":1647935152000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_writer_name\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":16,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"updateTime\":1647935152000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":17,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"book_type\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"updateTime\":1647935152', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:02:51');
INSERT INTO `sys_oper_log` VALUES ('175', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', '1', 'admin', null, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"书籍审核状态\",\"params\":{},\"dictType\":\"book_audit\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:03:26');
INSERT INTO `sys_oper_log` VALUES ('176', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"0\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_audit\",\"dictLabel\":\"待审核\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:03:53');
INSERT INTO `sys_oper_log` VALUES ('177', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_audit\",\"dictLabel\":\"审核成功\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:04:03');
INSERT INTO `sys_oper_log` VALUES ('178', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_audit\",\"dictLabel\":\"审核失败\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:04:13');
INSERT INTO `sys_oper_log` VALUES ('179', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', '1', 'admin', null, '/tool/gen/synchDb/asq_books', '127.0.0.1', '内网IP', '{tableName=asq_books}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:20:36');
INSERT INTO `sys_oper_log` VALUES ('180', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":14,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1651482171000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647917660000,\"tableId\":3,\"pk\":true,\"columnName\":\"book_id\"},{\"capJavaField\":\"BookWriterName\",\"usableColumn\":false,\"columnId\":15,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookWriterName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍作者姓名\",\"isQuery\":\"1\",\"updateTime\":1651482171000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_writer_name\"},{\"capJavaField\":\"BookName\",\"usableColumn\":false,\"columnId\":16,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍名\",\"isQuery\":\"1\",\"updateTime\":1651482171000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647917660000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"book_name\"},{\"capJavaField\":\"BookType\",\"usableColumn\":false,\"columnId\":17,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"book_type\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookType\",\"htmlType\":\"select\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍类型\",\"isQuery\":\"1\",\"updateTime\":1651482171', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:21:38');
INSERT INTO `sys_oper_log` VALUES ('181', '图书管理', '2', 'com.ruoyi.controller.AsqBooksController.edit()', 'PUT', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"拾柒工作室\",\"params\":{},\"bookCover\":\"/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg\",\"bookName\":\"前后端分离\",\"bookId\":1,\"bookPubDate\":1647878400000,\"bookUpDate\":1647878400000,\"bookWriterName\":\"liefox\",\"bookAudit\":0,\"bookPdfUrl\":\"/profile/upload/2022/05/02/05c3662b-941b-4631-b2cd-6c7e24b518b9.pdf\",\"bookType\":\"Java\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:35:39');
INSERT INTO `sys_oper_log` VALUES ('182', '图书管理', '2', 'com.ruoyi.controller.AsqBooksController.edit()', 'PUT', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"拾柒工作室\",\"params\":{},\"bookCover\":\"/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg\",\"bookName\":\"前后端分离\",\"bookId\":1,\"bookPubDate\":1647878400000,\"bookUpDate\":1647878400000,\"bookWriterName\":\"liefox\",\"bookAudit\":0,\"bookPdfUrl\":\"/profile/upload/2022/05/02/05c3662b-941b-4631-b2cd-6c7e24b518b9.pdf\",\"bookType\":\"Java\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:36:40');
INSERT INTO `sys_oper_log` VALUES ('183', '图书管理', '2', 'com.ruoyi.controller.AsqBooksController.edit()', 'PUT', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"拾柒工作室\",\"params\":{},\"bookCover\":\"/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg\",\"bookName\":\"前后端分离\",\"bookId\":1,\"bookPubDate\":1647878400000,\"bookUpDate\":1647878400000,\"bookWriterName\":\"liefox\",\"bookAudit\":0,\"bookPdfUrl\":\"/profile/upload/2022/05/02/3c84954e-fcde-49af-ba28-338bdac2c5bb.pdf\",\"bookType\":\"Java\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:47:45');
INSERT INTO `sys_oper_log` VALUES ('184', '图书管理', '2', 'com.ruoyi.controller.AsqBooksController.edit()', 'PUT', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"拾柒工作室\",\"params\":{},\"bookCover\":\"/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg\",\"bookName\":\"前后端分离\",\"bookId\":1,\"bookPubDate\":1647878400000,\"bookUpDate\":1647878400000,\"bookWriterName\":\"liefox\",\"bookAudit\":1,\"bookPdfUrl\":\"/profile/upload/2022/05/02/3c84954e-fcde-49af-ba28-338bdac2c5bb.pdf\",\"bookType\":\"Java\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 17:49:02');
INSERT INTO `sys_oper_log` VALUES ('185', '参数管理', '2', 'com.ruoyi.web.controller.system.SysConfigController.edit()', 'PUT', '1', 'admin', null, '/system/config', '127.0.0.1', '内网IP', '{\"configName\":\"账号自助-是否开启用户注册功能\",\"configKey\":\"sys.account.registerUser\",\"createBy\":\"admin\",\"createTime\":1647869593000,\"updateBy\":\"admin\",\"configId\":5,\"remark\":\"是否开启注册用户功能（true开启，false关闭）\",\"configType\":\"Y\",\"configValue\":\"true\",\"params\":{}}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 18:10:14');
INSERT INTO `sys_oper_log` VALUES ('186', '字典数据', '2', 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"0\",\"listClass\":\"warning\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_audit\",\"dictLabel\":\"待审核\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1651482233000,\"dictCode\":103,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 18:30:20');
INSERT INTO `sys_oper_log` VALUES ('187', '字典数据', '2', 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"0\",\"listClass\":\"warning\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_audit\",\"dictLabel\":\"待审核\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1651482233000,\"dictCode\":103,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 18:30:21');
INSERT INTO `sys_oper_log` VALUES ('188', '字典数据', '2', 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"listClass\":\"success\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_audit\",\"dictLabel\":\"审核成功\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1651482243000,\"dictCode\":104,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 18:30:29');
INSERT INTO `sys_oper_log` VALUES ('189', '字典数据', '2', 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"listClass\":\"danger\",\"dictSort\":0,\"params\":{},\"dictType\":\"book_audit\",\"dictLabel\":\"审核失败\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1651482253000,\"dictCode\":105,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 18:30:35');
INSERT INTO `sys_oper_log` VALUES ('190', '作者管理', '1', 'com.ruoyi.controller.AsqWriterController.add()', 'POST', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerName\":\"佐佳豪\",\"writerId\":5}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 18:31:39');
INSERT INTO `sys_oper_log` VALUES ('191', '作者管理', '3', 'com.ruoyi.controller.AsqWriterController.remove()', 'DELETE', '1', 'admin', null, '/writer/writer/5,4,3,2', '127.0.0.1', '内网IP', '{writerIds=5,4,3,2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:05:22');
INSERT INTO `sys_oper_log` VALUES ('192', '作者管理', '1', 'com.ruoyi.controller.AsqWriterController.add()', 'POST', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerName\":\"佐佳豪\",\"writerId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:06:10');
INSERT INTO `sys_oper_log` VALUES ('193', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"图书管理\",\"params\":{},\"parentId\":2015,\"isCache\":\"1\",\"path\":\"books\",\"component\":\"bookM/books/index\",\"children\":[],\"createTime\":1647935374000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2016,\"menuType\":\"C\",\"perms\":\"bookM:books:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:09:42');
INSERT INTO `sys_oper_log` VALUES ('194', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerName\":\"佳豪\",\"writerId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:09:57');
INSERT INTO `sys_oper_log` VALUES ('195', '作者管理', '3', 'com.ruoyi.controller.AsqWriterController.remove()', 'DELETE', '1', 'admin', null, '/writer/writer/6', '127.0.0.1', '内网IP', '{writerIds=6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:10:40');
INSERT INTO `sys_oper_log` VALUES ('196', '作者管理', '1', 'com.ruoyi.controller.AsqWriterController.add()', 'POST', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerName\":\"1\",\"writerId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:11:37');
INSERT INTO `sys_oper_log` VALUES ('197', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerName\":\"2\",\"writerId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:11:52');
INSERT INTO `sys_oper_log` VALUES ('198', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"图书管理\",\"params\":{},\"parentId\":2015,\"isCache\":\"1\",\"path\":\"books\",\"component\":\"bookM/books/index\",\"children\":[],\"createTime\":1647935374000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2016,\"menuType\":\"C\",\"perms\":\"bookM:books:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:12:55');
INSERT INTO `sys_oper_log` VALUES ('199', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerName\":\"3\",\"writerId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:13:07');
INSERT INTO `sys_oper_log` VALUES ('200', '作者管理', '2', 'com.ruoyi.controller.AsqWriterController.edit()', 'PUT', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"params\":{},\"writerPic\":\"/profile/upload/2022/05/02/8d4e9a34-ae54-47e4-a564-9c3d27d320f4.jpg\",\"writerName\":\"3\",\"writerId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-05-02 19:49:18');
INSERT INTO `sys_oper_log` VALUES ('201', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_ppt', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:17:53');
INSERT INTO `sys_oper_log` VALUES ('202', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"PptId\",\"usableColumn\":false,\"columnId\":29,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pptId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1658657873000,\"tableId\":5,\"pk\":true,\"columnName\":\"ppt_id\"},{\"capJavaField\":\"PptName\",\"usableColumn\":false,\"columnId\":30,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pptName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"幻灯片名称\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658657873000,\"isEdit\":\"1\",\"tableId\":5,\"pk\":false,\"columnName\":\"ppt_name\"},{\"capJavaField\":\"PptPic\",\"usableColumn\":false,\"columnId\":31,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pptPic\",\"htmlType\":\"imageUpload\",\"edit\":true,\"query\":false,\"columnComment\":\"图片\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658657873000,\"isEdit\":\"1\",\"tableId\":5,\"pk\":false,\"columnName\":\"ppt_pic\"},{\"capJavaField\":\"PptUrl\",\"usableColumn\":false,\"columnId\":32,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pptUrl\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"跳转路径\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658657873000', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:20:54');
INSERT INTO `sys_oper_log` VALUES ('203', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":\"4\",\"menuName\":\"前台首页幻灯片管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"ppt\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:22:14');
INSERT INTO `sys_oper_log` VALUES ('204', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', '1', 'admin', null, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"幻灯片类型\",\"params\":{},\"dictType\":\"pptType\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:22:44');
INSERT INTO `sys_oper_log` VALUES ('205', '字典类型', '2', 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', '1', 'admin', null, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1658658164000,\"updateBy\":\"admin\",\"dictName\":\"幻灯片类型\",\"dictId\":102,\"params\":{},\"dictType\":\"ppt_type\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:22:57');
INSERT INTO `sys_oper_log` VALUES ('206', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"ppt_type\",\"dictLabel\":\"书籍\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:23:22');
INSERT INTO `sys_oper_log` VALUES ('207', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"ppt_type\",\"dictLabel\":\"文章\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:23:37');
INSERT INTO `sys_oper_log` VALUES ('208', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"ppt_type\",\"dictLabel\":\"第三方\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:23:45');
INSERT INTO `sys_oper_log` VALUES ('209', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"PptId\",\"usableColumn\":false,\"columnId\":29,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pptId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"ID\",\"updateTime\":1658658054000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1658657873000,\"tableId\":5,\"pk\":true,\"columnName\":\"ppt_id\"},{\"capJavaField\":\"PptName\",\"usableColumn\":false,\"columnId\":30,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pptName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"updateTime\":1658658054000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(50)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658657873000,\"isEdit\":\"1\",\"tableId\":5,\"pk\":false,\"columnName\":\"ppt_name\"},{\"capJavaField\":\"PptPic\",\"usableColumn\":false,\"columnId\":31,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pptPic\",\"htmlType\":\"imageUpload\",\"edit\":true,\"query\":false,\"columnComment\":\"图片\",\"updateTime\":1658658054000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658657873000,\"isEdit\":\"1\",\"tableId\":5,\"pk\":false,\"columnName\":\"ppt_pic\"},{\"capJavaField\":\"PptUrl\",\"usableColumn\":false,\"columnId\":32,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"pptUrl\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"跳转路径\",\"updateTime\":1658658054000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Stri', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 18:25:03');
INSERT INTO `sys_oper_log` VALUES ('210', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-07-24 18:25:19');
INSERT INTO `sys_oper_log` VALUES ('211', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":\"3\",\"menuName\":\"前台幻灯片管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"ppt\",\"children\":[],\"createTime\":1658658134000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2029,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 19:24:03');
INSERT INTO `sys_oper_log` VALUES ('212', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":\"1\",\"menuName\":\"前台幻灯片\",\"params\":{},\"parentId\":2029,\"isCache\":\"0\",\"path\":\"ppt\",\"component\":\"ppt/ppt/index\",\"children\":[],\"createTime\":1658658510000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2030,\"menuType\":\"C\",\"perms\":\"ppt:ppt:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 19:24:18');
INSERT INTO `sys_oper_log` VALUES ('213', '前台首页幻灯片', '1', 'com.ruoyi.controller.AsqPptController.add()', 'POST', '1', 'admin', null, '/ppt/ppt', '127.0.0.1', '内网IP', '{\"pptPic\":\"/profile/upload/2022/07/24/7235bfa2-d507-4f76-8ccc-fc569c1d3155.jpg\",\"params\":{},\"pptType\":3,\"pptId\":1,\"pptDesc\":1,\"pptName\":\"ppt1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 19:26:50');
INSERT INTO `sys_oper_log` VALUES ('214', '前台首页幻灯片', '2', 'com.ruoyi.controller.AsqPptController.edit()', 'PUT', '1', 'admin', null, '/ppt/ppt', '127.0.0.1', '内网IP', '{\"pptUrl\":\"https://www.zhihu.com/\",\"pptPic\":\"/profile/upload/2022/07/24/7235bfa2-d507-4f76-8ccc-fc569c1d3155.jpg\",\"params\":{},\"pptType\":3,\"pptId\":1,\"pptDesc\":1,\"pptName\":\"ppt1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 19:27:19');
INSERT INTO `sys_oper_log` VALUES ('215', '前台首页幻灯片', '1', 'com.ruoyi.controller.AsqPptController.add()', 'POST', '1', 'admin', null, '/ppt/ppt', '127.0.0.1', '内网IP', '{\"pptUrl\":\"1\",\"pptPic\":\"/profile/upload/2022/07/24/e3b491f1-9fa8-4c57-9208-928d51e40b38.jpeg\",\"params\":{},\"pptType\":1,\"pptId\":2,\"pptDesc\":2,\"pptName\":\"书籍\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 19:28:15');
INSERT INTO `sys_oper_log` VALUES ('216', '前台首页幻灯片', '2', 'com.ruoyi.controller.AsqPptController.edit()', 'PUT', '1', 'admin', null, '/ppt/ppt', '127.0.0.1', '内网IP', '{\"pptUrl\":\"1\",\"pptPic\":\"/profile/upload/2022/07/24/e3b491f1-9fa8-4c57-9208-928d51e40b38.jpeg\",\"params\":{},\"pptType\":1,\"pptId\":2,\"pptDesc\":2,\"pptName\":\"书籍撒打算打算的\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 21:22:34');
INSERT INTO `sys_oper_log` VALUES ('217', '前台首页幻灯片', '1', 'com.ruoyi.controller.AsqPptController.add()', 'POST', '1', 'admin', null, '/ppt/ppt', '127.0.0.1', '内网IP', '{\"pptUrl\":\"https://www.zhihu.com/question/543901181\",\"pptPic\":\"/profile/upload/2022/07/24/beecd828-5063-4252-9b4a-b4970b516cdf.jpg\",\"params\":{},\"pptType\":2,\"pptId\":3,\"pptDesc\":3,\"pptName\":\"空间站的文学\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-24 21:27:17');
INSERT INTO `sys_oper_log` VALUES ('218', '前台首页幻灯片', '2', 'com.ruoyi.controller.AsqPptController.edit()', 'PUT', '1', 'admin', null, '/ppt/ppt', '127.0.0.1', '内网IP', '{\"pptUrl\":\"https://www.zhihu.com/question/543901181\",\"pptPic\":\"/profile/upload/2022/07/24/beecd828-5063-4252-9b4a-b4970b516cdf.jpg\",\"params\":{},\"pptType\":2,\"pptId\":3,\"pptDesc\":3,\"pptName\":\"空间站的文学——知乎文章\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-26 08:47:25');
INSERT INTO `sys_oper_log` VALUES ('219', '前台首页幻灯片', '2', 'com.ruoyi.controller.AsqPptController.edit()', 'PUT', '1', 'admin', null, '/ppt/ppt', '127.0.0.1', '内网IP', '{\"pptUrl\":\"1\",\"pptPic\":\"/profile/upload/2022/07/24/e3b491f1-9fa8-4c57-9208-928d51e40b38.jpeg\",\"params\":{},\"pptType\":1,\"pptId\":2,\"pptDesc\":2,\"pptName\":\"著名作者哈登新书热卖\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-26 08:48:01');
INSERT INTO `sys_oper_log` VALUES ('220', '前台首页幻灯片', '2', 'com.ruoyi.controller.AsqPptController.edit()', 'PUT', '1', 'admin', null, '/ppt/ppt', '127.0.0.1', '内网IP', '{\"pptUrl\":\"https://www.zhihu.com/\",\"pptPic\":\"/profile/upload/2022/07/24/7235bfa2-d507-4f76-8ccc-fc569c1d3155.jpg\",\"params\":{},\"pptType\":3,\"pptId\":1,\"pptDesc\":1,\"pptName\":\"梅立国宇航员的一天\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-26 08:48:32');
INSERT INTO `sys_oper_log` VALUES ('221', '作者管理', '1', 'com.ruoyi.controller.AsqWriterController.add()', 'POST', '1', 'admin', null, '/writer/writer', '127.0.0.1', '内网IP', '{\"writerIntro\":\"逝者如斯夫\",\"params\":{},\"writerPic\":\"/profile/upload/2022/07/26/73e7835a-8f32-4c0f-9e25-154ca69b0f43.jpeg\",\"writerName\":\"liefox\",\"writerId\":8,\"writerInfo\":\"<p>拾柒爱阅读百大创作人</p>\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-26 11:21:53');
INSERT INTO `sys_oper_log` VALUES ('222', '图书管理', '1', 'com.ruoyi.controller.AsqBooksController.add()', 'POST', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"人民出版社\",\"params\":{},\"bookCover\":\"/profile/upload/2022/07/26/b96ee2d7-500d-4ae9-b7f0-5aa470fca2cc.png\",\"bookName\":\"JAVA修炼手册\",\"bookId\":2,\"bookPubDate\":1659024000000,\"bookUpDate\":1658764800000,\"bookWriterName\":\"liefox\",\"bookAudit\":1,\"bookPdfUrl\":\"/profile/upload/2022/07/26/386fb641-ca7f-45da-9b58-aeb62fc5bebc.pdf\",\"bookType\":\"Java\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-26 11:49:35');
INSERT INTO `sys_oper_log` VALUES ('223', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_rank_parm', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 08:57:08');
INSERT INTO `sys_oper_log` VALUES ('224', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"RankParmId\",\"usableColumn\":false,\"columnId\":35,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"rankParmId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1658969828000,\"tableId\":6,\"pk\":true,\"columnName\":\"rank_parm_id\"},{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":36,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍id\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658969828000,\"isEdit\":\"1\",\"tableId\":6,\"pk\":false,\"columnName\":\"book_id\"},{\"capJavaField\":\"ReadNum\",\"usableColumn\":false,\"columnId\":37,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"readNum\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"阅读数\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658969828000,\"isEdit\":\"1\",\"tableId\":6,\"pk\":false,\"columnName\":\"read_num\"},{\"capJavaField\":\"ApplaudNum\",\"usableColumn\":false,\"columnId\":38,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"applaudNum\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"赞同数\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":16589698280', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 09:01:11');
INSERT INTO `sys_oper_log` VALUES ('225', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-07-28 09:02:14');
INSERT INTO `sys_oper_log` VALUES ('226', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-07-28 09:04:17');
INSERT INTO `sys_oper_log` VALUES ('227', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-07-28 09:06:51');
INSERT INTO `sys_oper_log` VALUES ('228', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"ruoyi\",\"columns\":[{\"capJavaField\":\"RankParmId\",\"usableColumn\":false,\"columnId\":35,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"rankParmId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1658970071000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1658969828000,\"tableId\":6,\"pk\":true,\"columnName\":\"rank_parm_id\"},{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":36,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍id\",\"isQuery\":\"1\",\"updateTime\":1658970071000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658969828000,\"isEdit\":\"1\",\"tableId\":6,\"pk\":false,\"columnName\":\"book_id\"},{\"capJavaField\":\"ReadNum\",\"usableColumn\":false,\"columnId\":37,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"readNum\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"阅读数\",\"isQuery\":\"1\",\"updateTime\":1658970071000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1658969828000,\"isEdit\":\"1\",\"tableId\":6,\"pk\":false,\"columnName\":\"read_num\"},{\"capJavaField\":\"ApplaudNum\",\"usableColumn\":false,\"columnId\":38,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"applaudNum\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"赞同数\",\"isQuery\":\"1\",\"updateTime\":1658970071000,\"sort\":4,\"list\":true,\"params\":{},\"java', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 09:08:01');
INSERT INTO `sys_oper_log` VALUES ('229', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-07-28 09:08:10');
INSERT INTO `sys_oper_log` VALUES ('230', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-07-28 09:08:32');
INSERT INTO `sys_oper_log` VALUES ('231', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-07-28 09:11:57');
INSERT INTO `sys_oper_log` VALUES ('232', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-07-28 09:12:23');
INSERT INTO `sys_oper_log` VALUES ('233', '图书管理', '1', 'com.ruoyi.controller.AsqBooksController.add()', 'POST', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"XX出版社\",\"params\":{},\"bookCover\":\"/profile/upload/2022/07/28/5bec2273-b097-413a-bcc1-aebb7da06244.jpg\",\"bookName\":\"让富婆爱上你\",\"bookId\":3,\"bookPubDate\":1659110400000,\"bookUpDate\":1658937600000,\"bookWriterName\":\"liefox\",\"bookAudit\":1,\"bookPdfUrl\":\"/profile/upload/2022/07/28/3cc0a4f9-3eda-4d96-9caf-955e6722cc0c.pdf\",\"bookType\":\"生活\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 11:25:43');
INSERT INTO `sys_oper_log` VALUES ('234', '图书管理', '1', 'com.ruoyi.controller.AsqBooksController.add()', 'POST', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"XX出版社\",\"params\":{},\"bookCover\":\"/profile/upload/2022/07/28/e6a09893-b636-4cc0-9f98-60d56178710b.jpg\",\"bookName\":\"妇女之友\",\"bookId\":4,\"bookPubDate\":1657987200000,\"bookUpDate\":1658937600000,\"bookWriterName\":\"拾柒图书1\",\"bookAudit\":1,\"bookPdfUrl\":\"/profile/upload/2022/07/28/50a27028-7de2-4126-baef-18ed02e35e62.pdf\",\"bookType\":\"生活\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 11:27:03');
INSERT INTO `sys_oper_log` VALUES ('235', '图书管理', '1', 'com.ruoyi.controller.AsqBooksController.add()', 'POST', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"XX出版社\",\"params\":{},\"bookCover\":\"/profile/upload/2022/07/28/a864307c-c466-4fe4-8a13-cc1c846fb4be.jpg\",\"bookName\":\"鱼道\",\"bookId\":5,\"bookPubDate\":1658937600000,\"bookUpDate\":1659024000000,\"bookWriterName\":\"3\",\"bookAudit\":1,\"bookPdfUrl\":\"/profile/upload/2022/07/28/c6767c5e-304e-40a3-84fd-0d92bd1997bf.pdf\",\"bookType\":\"生活\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 11:28:30');
INSERT INTO `sys_oper_log` VALUES ('236', '图书管理', '1', 'com.ruoyi.controller.AsqBooksController.add()', 'POST', '1', 'admin', null, '/bookM/books', '127.0.0.1', '内网IP', '{\"bookPress\":\"XX出版社\",\"params\":{},\"bookCover\":\"/profile/upload/2022/07/28/cc04b4bc-1929-4623-940c-05ab8972a2fa.jpg\",\"bookName\":\"萨克的数据\",\"bookId\":6,\"bookPubDate\":1658851200000,\"bookUpDate\":1656518400000,\"bookWriterName\":\"3\",\"bookAudit\":1,\"bookPdfUrl\":\"/profile/upload/2022/07/28/946612d8-7152-4b10-9f11-805cf474e6bb.pdf\",\"bookType\":\"Java\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 11:29:16');
INSERT INTO `sys_oper_log` VALUES ('237', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":3,\"readNum\":1,\"rankParmId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 12:23:07');
INSERT INTO `sys_oper_log` VALUES ('238', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":3,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 12:23:12');
INSERT INTO `sys_oper_log` VALUES ('239', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":3,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 12:23:19');
INSERT INTO `sys_oper_log` VALUES ('240', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":4,\"readNum\":1,\"rankParmId\":8}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 12:23:30');
INSERT INTO `sys_oper_log` VALUES ('241', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":4,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 12:23:35');
INSERT INTO `sys_oper_log` VALUES ('242', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":3,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 12:23:47');
INSERT INTO `sys_oper_log` VALUES ('243', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":5,\"readNum\":1,\"rankParmId\":9}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 12:23:59');
INSERT INTO `sys_oper_log` VALUES ('244', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":6,\"readNum\":1,\"rankParmId\":10}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-07-28 12:24:11');
INSERT INTO `sys_oper_log` VALUES ('245', '用户头像', '2', 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', '1', 'admin', null, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/avatar/2022/09/13/2348d5ce-4391-4245-a952-c724ec4970f2.png\",\"code\":200}', '0', null, '2022-09-13 10:12:11');
INSERT INTO `sys_oper_log` VALUES ('246', '用户头像', '2', 'com.ruoyi.web.controller.system.SysProfileController.avatar()', 'POST', '1', 'admin', null, '/system/user/profile/avatar', '127.0.0.1', '内网IP', '', '{\"msg\":\"操作成功\",\"imgUrl\":\"/profile/avatar/2022/09/13/6ba335a3-ba97-483c-bb11-fb5c2248fc6d.png\",\"code\":200}', '0', null, '2022-09-13 10:12:11');
INSERT INTO `sys_oper_log` VALUES ('247', '个人信息', '2', 'com.ruoyi.web.controller.system.SysProfileController.updateProfile()', 'PUT', '1', 'admin', null, '/system/user/profile', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":1,\"admin\":true,\"dataScope\":\"1\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"status\":\"0\"}],\"phonenumber\":\"15888888888\",\"admin\":true,\"loginDate\":1663245267373,\"remark\":\"管理员\",\"delFlag\":\"0\",\"loginIp\":\"127.0.0.1\",\"email\":\"liefox@163.com\",\"nickName\":\"liefox\",\"sex\":\"1\",\"deptId\":103,\"avatar\":\"/profile/avatar/2022/09/13/6ba335a3-ba97-483c-bb11-fb5c2248fc6d.png\",\"dept\":{\"deptName\":\"研发部门\",\"leader\":\"liefox\",\"deptId\":103,\"orderNum\":\"1\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"admin\",\"userId\":1,\"createBy\":\"admin\",\"createTime\":1647869592000,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-09-15 20:56:22');
INSERT INTO `sys_oper_log` VALUES ('248', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-24 21:12:32');
INSERT INTO `sys_oper_log` VALUES ('249', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_applaud', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-24 21:32:51');
INSERT INTO `sys_oper_log` VALUES ('250', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"ApplaudId\",\"usableColumn\":false,\"columnId\":41,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"applaudId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1666618371000,\"tableId\":7,\"pk\":true,\"columnName\":\"applaud_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":42,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666618371000,\"isEdit\":\"1\",\"tableId\":7,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":43,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"书籍\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666618371000,\"isEdit\":\"1\",\"tableId\":7,\"pk\":false,\"columnName\":\"book_id\"},{\"capJavaField\":\"CreadData\",\"usableColumn\":false,\"columnId\":44,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"creadData\",\"htmlType\":\"datetime\",\"edit\":true,\"query\":false,\"columnComment\":\"创建时间\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666618371000,\"isEdit\":\"1\",\"tableId\":7,\"pk\":false,\"columnNam', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-24 21:36:27');
INSERT INTO `sys_oper_log` VALUES ('251', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"ApplaudId\",\"usableColumn\":false,\"columnId\":41,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"applaudId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1666618587000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1666618371000,\"tableId\":7,\"pk\":true,\"columnName\":\"applaud_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":42,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"用户\",\"updateTime\":1666618587000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666618371000,\"isEdit\":\"1\",\"tableId\":7,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":43,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":true,\"query\":false,\"columnComment\":\"书籍\",\"updateTime\":1666618587000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666618371000,\"isEdit\":\"1\",\"tableId\":7,\"pk\":false,\"columnName\":\"book_id\"},{\"capJavaField\":\"CreadData\",\"usableColumn\":false,\"columnId\":44,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"creadData\",\"htmlType\":\"datetime\",\"edit\":true,\"query\":false,\"columnComment\":\"创建时间\",\"updateTime\":1666618587000,\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datet', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-24 21:37:23');
INSERT INTO `sys_oper_log` VALUES ('252', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-10-24 21:37:41');
INSERT INTO `sys_oper_log` VALUES ('253', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666688851571,\"params\":{},\"userId\":1,\"bookId\":2}', 'null', '1', '', '2022-10-25 17:07:31');
INSERT INTO `sys_oper_log` VALUES ('254', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666690388320,\"params\":{},\"userId\":1,\"bookId\":4}', 'null', '1', '', '2022-10-25 17:33:08');
INSERT INTO `sys_oper_log` VALUES ('255', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666690397553,\"params\":{},\"userId\":1,\"bookId\":4}', 'null', '1', '', '2022-10-25 17:33:17');
INSERT INTO `sys_oper_log` VALUES ('256', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666690527266,\"params\":{},\"userId\":1,\"bookId\":4}', 'null', '1', '', '2022-10-25 17:37:53');
INSERT INTO `sys_oper_log` VALUES ('257', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666690808588,\"params\":{},\"userId\":1,\"bookId\":4}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\projects\\拾柒图书管理系统V2\\shiqi-service\\target\\classes\\mapper\\applaud\\AsqApplaudMapper.xml]\r\n### The error may involve com.ruoyi.mapper.AsqApplaudMapper.insertAsqApplaud-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into asq_applaud          ( user_id,             book_id,             cread_data )           values ( ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value\n; Field \'applaud_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value', '2022-10-25 17:40:26');
INSERT INTO `sys_oper_log` VALUES ('258', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666690847145,\"params\":{},\"userId\":1,\"bookId\":4}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\projects\\拾柒图书管理系统V2\\shiqi-service\\target\\classes\\mapper\\applaud\\AsqApplaudMapper.xml]\r\n### The error may involve com.ruoyi.mapper.AsqApplaudMapper.insertAsqApplaud-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into asq_applaud          ( user_id,             book_id,             cread_data )           values ( ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value\n; Field \'applaud_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value', '2022-10-25 17:40:49');
INSERT INTO `sys_oper_log` VALUES ('259', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666690910628,\"params\":{},\"userId\":1,\"bookId\":4}', 'null', '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\projects\\拾柒图书管理系统V2\\shiqi-service\\target\\classes\\mapper\\applaud\\AsqApplaudMapper.xml]\r\n### The error may involve com.ruoyi.mapper.AsqApplaudMapper.insertAsqApplaud-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into asq_applaud          ( user_id,             book_id,             cread_data )           values ( ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value\n; Field \'applaud_id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'applaud_id\' doesn\'t have a default value', '2022-10-25 17:41:50');
INSERT INTO `sys_oper_log` VALUES ('260', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666690939021,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 17:42:19');
INSERT INTO `sys_oper_log` VALUES ('261', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666691360896,\"params\":{},\"userId\":1,\"bookId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 17:49:20');
INSERT INTO `sys_oper_log` VALUES ('262', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666691376385,\"params\":{},\"userId\":1,\"bookId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 17:49:36');
INSERT INTO `sys_oper_log` VALUES ('263', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666694936989,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 18:48:57');
INSERT INTO `sys_oper_log` VALUES ('264', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666694980479,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 18:49:40');
INSERT INTO `sys_oper_log` VALUES ('265', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 18:51:56');
INSERT INTO `sys_oper_log` VALUES ('266', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666695120779,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 18:52:00');
INSERT INTO `sys_oper_log` VALUES ('267', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 18:52:04');
INSERT INTO `sys_oper_log` VALUES ('268', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666695130133,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 18:52:10');
INSERT INTO `sys_oper_log` VALUES ('269', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2036', '127.0.0.1', '内网IP', '{menuId=2036}', '{\"msg\":\"存在子菜单,不允许删除\",\"code\":500}', '0', null, '2022-10-25 19:26:28');
INSERT INTO `sys_oper_log` VALUES ('270', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2037', '127.0.0.1', '内网IP', '{menuId=2037}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:26:33');
INSERT INTO `sys_oper_log` VALUES ('271', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2038', '127.0.0.1', '内网IP', '{menuId=2038}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:26:36');
INSERT INTO `sys_oper_log` VALUES ('272', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2039', '127.0.0.1', '内网IP', '{menuId=2039}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:26:42');
INSERT INTO `sys_oper_log` VALUES ('273', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2040', '127.0.0.1', '内网IP', '{menuId=2040}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:26:44');
INSERT INTO `sys_oper_log` VALUES ('274', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2041', '127.0.0.1', '内网IP', '{menuId=2041}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:26:47');
INSERT INTO `sys_oper_log` VALUES ('275', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2036', '127.0.0.1', '内网IP', '{menuId=2036}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:26:51');
INSERT INTO `sys_oper_log` VALUES ('276', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"ApplaudId\",\"usableColumn\":false,\"columnId\":41,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"applaudId\",\"htmlType\":\"input\",\"edit\":false,\"query\":true,\"isQuery\":\"1\",\"updateTime\":1666618643000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1666618371000,\"tableId\":7,\"pk\":true,\"columnName\":\"applaud_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":42,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户\",\"isQuery\":\"1\",\"updateTime\":1666618643000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666618371000,\"isEdit\":\"1\",\"tableId\":7,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":43,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍\",\"isQuery\":\"1\",\"updateTime\":1666618643000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666618371000,\"isEdit\":\"1\",\"tableId\":7,\"pk\":false,\"columnName\":\"book_id\"},{\"capJavaField\":\"CreadData\",\"usableColumn\":false,\"columnId\":44,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"creadData\",\"htmlType\":\"datetime\",\"edit\":true,\"query\":true,\"columnComment\":\"创建时间\",\"isQuery\":\"1\",\"updateTime\":1666618643000,\"sort\":4,\"list\":true,\"params\":{},\"j', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:27:07');
INSERT INTO `sys_oper_log` VALUES ('277', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:49:17');
INSERT INTO `sys_oper_log` VALUES ('278', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666698935943,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:55:35');
INSERT INTO `sys_oper_log` VALUES ('279', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:55:39');
INSERT INTO `sys_oper_log` VALUES ('280', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666698948638,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:55:48');
INSERT INTO `sys_oper_log` VALUES ('281', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:55:51');
INSERT INTO `sys_oper_log` VALUES ('282', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666698963245,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:56:03');
INSERT INTO `sys_oper_log` VALUES ('283', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:56:42');
INSERT INTO `sys_oper_log` VALUES ('284', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":4,\"readNum\":1,\"rankParmId\":11}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:58:07');
INSERT INTO `sys_oper_log` VALUES ('285', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666699092957,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:58:12');
INSERT INTO `sys_oper_log` VALUES ('286', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:58:19');
INSERT INTO `sys_oper_log` VALUES ('287', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666699118789,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:58:38');
INSERT INTO `sys_oper_log` VALUES ('288', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 19:58:48');
INSERT INTO `sys_oper_log` VALUES ('289', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', 'null', '1', 'Index: 0, Size: 0', '2022-10-25 19:58:55');
INSERT INTO `sys_oper_log` VALUES ('290', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666699437133,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 20:03:57');
INSERT INTO `sys_oper_log` VALUES ('291', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-25 20:03:58');
INSERT INTO `sys_oper_log` VALUES ('292', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666764226336,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 14:03:46');
INSERT INTO `sys_oper_log` VALUES ('293', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666767385785,\"params\":{},\"userId\":1,\"bookId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 14:56:25');
INSERT INTO `sys_oper_log` VALUES ('294', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666767403805,\"params\":{},\"userId\":1,\"bookId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 14:56:43');
INSERT INTO `sys_oper_log` VALUES ('295', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/2', '127.0.0.1', '内网IP', '{bookId=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 14:56:50');
INSERT INTO `sys_oper_log` VALUES ('296', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 15:42:01');
INSERT INTO `sys_oper_log` VALUES ('297', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666770131152,\"params\":{},\"userId\":1,\"bookId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 15:42:11');
INSERT INTO `sys_oper_log` VALUES ('298', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666770908261,\"params\":{},\"userId\":1,\"bookId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 15:55:08');
INSERT INTO `sys_oper_log` VALUES ('299', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666770917761,\"params\":{},\"userId\":1,\"bookId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 15:55:17');
INSERT INTO `sys_oper_log` VALUES ('300', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666782648092,\"params\":{},\"userId\":1,\"bookId\":3}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 19:10:48');
INSERT INTO `sys_oper_log` VALUES ('301', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'as_collect', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 19:27:33');
INSERT INTO `sys_oper_log` VALUES ('302', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', '1', 'admin', null, '/tool/gen/synchDb/as_collect', '127.0.0.1', '内网IP', '{tableName=as_collect}', 'null', '1', '同步数据失败，原表结构不存在', '2022-10-26 19:28:49');
INSERT INTO `sys_oper_log` VALUES ('303', '代码生成', '3', 'com.ruoyi.generator.controller.GenController.remove()', 'DELETE', '1', 'admin', null, '/tool/gen/8', '127.0.0.1', '内网IP', '{tableIds=8}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 19:28:53');
INSERT INTO `sys_oper_log` VALUES ('304', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_collect', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 19:28:56');
INSERT INTO `sys_oper_log` VALUES ('305', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"CollectId\",\"usableColumn\":false,\"columnId\":49,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"collectId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1666783736000,\"tableId\":9,\"pk\":true,\"columnName\":\"collect_id\"},{\"capJavaField\":\"BookId\",\"usableColumn\":false,\"columnId\":50,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"bookId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"书籍id\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666783736000,\"isEdit\":\"1\",\"tableId\":9,\"pk\":false,\"columnName\":\"book_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":51,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户id\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666783736000,\"isEdit\":\"1\",\"tableId\":9,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"CreateDate\",\"usableColumn\":false,\"columnId\":52,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createDate\",\"htmlType\":\"datetime\",\"edit\":true,\"query\":true,\"columnComment\":\"创建时间\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Date\",\"queryType\":\"EQ\",\"columnType\":\"datetime\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666783736000,\"i', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 19:29:31');
INSERT INTO `sys_oper_log` VALUES ('306', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-10-26 19:29:42');
INSERT INTO `sys_oper_log` VALUES ('307', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":1,\"params\":{},\"userId\":1,\"bookId\":1,\"createDate\":1666786132337}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:08:52');
INSERT INTO `sys_oper_log` VALUES ('308', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:09:36');
INSERT INTO `sys_oper_log` VALUES ('309', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":2,\"params\":{},\"userId\":1,\"bookId\":1,\"createDate\":1666786199924}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:09:59');
INSERT INTO `sys_oper_log` VALUES ('310', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":3,\"params\":{},\"userId\":1,\"bookId\":2,\"createDate\":1666787082245}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:24:42');
INSERT INTO `sys_oper_log` VALUES ('311', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/2', '127.0.0.1', '内网IP', '{bookId=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:30:21');
INSERT INTO `sys_oper_log` VALUES ('312', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:38:46');
INSERT INTO `sys_oper_log` VALUES ('313', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":4,\"params\":{},\"userId\":1,\"bookId\":1,\"createDate\":1666787963776}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:39:23');
INSERT INTO `sys_oper_log` VALUES ('314', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:39:39');
INSERT INTO `sys_oper_log` VALUES ('315', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 20:41:30');
INSERT INTO `sys_oper_log` VALUES ('316', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":5,\"params\":{},\"userId\":1,\"bookId\":4,\"createDate\":1666789525426}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:05:25');
INSERT INTO `sys_oper_log` VALUES ('317', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:05:56');
INSERT INTO `sys_oper_log` VALUES ('318', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":4,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:05:58');
INSERT INTO `sys_oper_log` VALUES ('319', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":6,\"params\":{},\"userId\":1,\"bookId\":4,\"createDate\":1666789794773}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:09:54');
INSERT INTO `sys_oper_log` VALUES ('320', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/4', '127.0.0.1', '内网IP', '{bookId=4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:09:55');
INSERT INTO `sys_oper_log` VALUES ('321', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":4,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:09:59');
INSERT INTO `sys_oper_log` VALUES ('322', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":4,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:14:45');
INSERT INTO `sys_oper_log` VALUES ('323', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":4,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:15:00');
INSERT INTO `sys_oper_log` VALUES ('324', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:15:09');
INSERT INTO `sys_oper_log` VALUES ('325', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":4,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:41:48');
INSERT INTO `sys_oper_log` VALUES ('326', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:42:19');
INSERT INTO `sys_oper_log` VALUES ('327', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:42:30');
INSERT INTO `sys_oper_log` VALUES ('328', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:42:45');
INSERT INTO `sys_oper_log` VALUES ('329', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666792226976,\"params\":{},\"userId\":1,\"bookId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:50:27');
INSERT INTO `sys_oper_log` VALUES ('330', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":7,\"params\":{},\"userId\":1,\"bookId\":1,\"createDate\":1666792226975}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 21:50:27');
INSERT INTO `sys_oper_log` VALUES ('331', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 22:06:50');
INSERT INTO `sys_oper_log` VALUES ('332', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":3,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-26 22:16:03');
INSERT INTO `sys_oper_log` VALUES ('333', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:06:01');
INSERT INTO `sys_oper_log` VALUES ('334', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:06:17');
INSERT INTO `sys_oper_log` VALUES ('335', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":1,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:53:55');
INSERT INTO `sys_oper_log` VALUES ('336', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":8,\"params\":{},\"userId\":1,\"bookId\":2,\"createDate\":1666864458625}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:54:18');
INSERT INTO `sys_oper_log` VALUES ('337', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/2', '127.0.0.1', '内网IP', '{bookId=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:54:23');
INSERT INTO `sys_oper_log` VALUES ('338', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":9,\"params\":{},\"userId\":1,\"bookId\":1,\"createDate\":1666864480714}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:54:40');
INSERT INTO `sys_oper_log` VALUES ('339', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:54:48');
INSERT INTO `sys_oper_log` VALUES ('340', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":10,\"params\":{},\"userId\":1,\"bookId\":1,\"createDate\":1666864495857}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:54:55');
INSERT INTO `sys_oper_log` VALUES ('341', '加入书架', '3', 'com.ruoyi.controller.AsqCollectController.remove()', 'DELETE', '1', 'admin', null, '/collect/collect/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 17:55:03');
INSERT INTO `sys_oper_log` VALUES ('342', '排名参数', '1', 'com.ruoyi.controller.PubAsqRankParmController.add()', 'POST', '1', 'admin', null, '/public/rankParm/rankParm', '127.0.0.1', '内网IP', '{\"params\":{},\"bookId\":2,\"readNum\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 18:27:16');
INSERT INTO `sys_oper_log` VALUES ('343', '加入书架', '1', 'com.ruoyi.controller.AsqCollectController.add()', 'POST', '1', 'admin', null, '/collect/collect', '127.0.0.1', '内网IP', '{\"collectId\":11,\"params\":{},\"userId\":1,\"bookId\":2,\"createDate\":1666866438363}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 18:27:18');
INSERT INTO `sys_oper_log` VALUES ('344', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666866439518,\"params\":{},\"userId\":1,\"bookId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 18:27:19');
INSERT INTO `sys_oper_log` VALUES ('345', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666874069305,\"params\":{},\"userId\":1,\"bookId\":4}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 20:34:29');
INSERT INTO `sys_oper_log` VALUES ('346', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_comment', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 21:58:47');
INSERT INTO `sys_oper_log` VALUES ('347', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"CommentId\",\"usableColumn\":false,\"columnId\":53,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"commentId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1666879127000,\"tableId\":10,\"pk\":true,\"columnName\":\"comment_id\"},{\"capJavaField\":\"CommentParentId\",\"usableColumn\":false,\"columnId\":54,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"commentParentId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"父级id\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666879127000,\"isEdit\":\"1\",\"tableId\":10,\"pk\":false,\"columnName\":\"comment_parent_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":55,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户id\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666879127000,\"isEdit\":\"1\",\"tableId\":10,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"ComBodyId\",\"usableColumn\":false,\"columnId\":56,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"comBodyId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"评论体\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"crea', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:01:06');
INSERT INTO `sys_oper_log` VALUES ('348', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-10-27 22:01:37');
INSERT INTO `sys_oper_log` VALUES ('349', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', '1', 'admin', null, '/tool/gen/synchDb/asq_comment', '127.0.0.1', '内网IP', '{tableName=asq_comment}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:04:14');
INSERT INTO `sys_oper_log` VALUES ('350', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"CommentId\",\"usableColumn\":false,\"columnId\":53,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"commentId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1666879266000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1666879127000,\"tableId\":10,\"pk\":true,\"columnName\":\"comment_id\"},{\"capJavaField\":\"CommentParentId\",\"usableColumn\":false,\"columnId\":54,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"commentParentId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"父级id\",\"isQuery\":\"1\",\"updateTime\":1666879266000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666879127000,\"isEdit\":\"1\",\"tableId\":10,\"pk\":false,\"columnName\":\"comment_parent_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":55,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户id\",\"isQuery\":\"1\",\"updateTime\":1666879266000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666879127000,\"isEdit\":\"1\",\"tableId\":10,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"ComBodyId\",\"usableColumn\":false,\"columnId\":56,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"comBodyId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"评论体\",\"isQuery\":\"1\",\"updateTime\":1666879266000,\"sort\":4,\"list\":tr', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:05:47');
INSERT INTO `sys_oper_log` VALUES ('351', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":\"1\",\"menuName\":\"评论管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"comment\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:07:41');
INSERT INTO `sys_oper_log` VALUES ('352', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"CommentId\",\"usableColumn\":false,\"columnId\":53,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"commentId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1666879547000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1666879127000,\"tableId\":10,\"pk\":true,\"columnName\":\"comment_id\"},{\"capJavaField\":\"CommentParentId\",\"usableColumn\":false,\"columnId\":54,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"commentParentId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"父级id\",\"isQuery\":\"1\",\"updateTime\":1666879547000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666879127000,\"isEdit\":\"1\",\"tableId\":10,\"pk\":false,\"columnName\":\"comment_parent_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":55,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户id\",\"isQuery\":\"1\",\"updateTime\":1666879547000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666879127000,\"isEdit\":\"1\",\"tableId\":10,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"ComBodyId\",\"usableColumn\":false,\"columnId\":56,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"comBodyId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"评论体\",\"isQuery\":\"1\",\"updateTime\":1666879547000,\"sort\":4,\"list\":tr', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:08:10');
INSERT INTO `sys_oper_log` VALUES ('353', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-10-27 22:08:14');
INSERT INTO `sys_oper_log` VALUES ('354', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', '1', 'admin', null, '/tool/gen/synchDb/asq_comment', '127.0.0.1', '内网IP', '{tableName=asq_comment}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:37:32');
INSERT INTO `sys_oper_log` VALUES ('355', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"CommentId\",\"usableColumn\":false,\"columnId\":53,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"commentId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1666879689000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1666879127000,\"tableId\":10,\"pk\":true,\"columnName\":\"comment_id\"},{\"capJavaField\":\"CommentParentId\",\"usableColumn\":false,\"columnId\":54,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"commentParentId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"父级id\",\"isQuery\":\"1\",\"updateTime\":1666879689000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666879127000,\"isEdit\":\"1\",\"tableId\":10,\"pk\":false,\"columnName\":\"comment_parent_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":55,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"用户id\",\"isQuery\":\"1\",\"updateTime\":1666879690000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1666879127000,\"isEdit\":\"1\",\"tableId\":10,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"ComBodyId\",\"usableColumn\":false,\"columnId\":56,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"comBodyId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"评论体\",\"isQuery\":\"1\",\"updateTime\":1666879690000,\"sort\":4,\"list\":tr', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:39:23');
INSERT INTO `sys_oper_log` VALUES ('356', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":\"1024\",\"menuName\":\"评论管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"comment\",\"children\":[],\"createTime\":1666879661000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2036,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:40:44');
INSERT INTO `sys_oper_log` VALUES ('357', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"guide\",\"orderNum\":\"1024\",\"menuName\":\"liefox官网\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"http://cxq21.gitee.io\",\"children\":[],\"createTime\":1647869592000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:41:04');
INSERT INTO `sys_oper_log` VALUES ('358', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":\"124\",\"menuName\":\"评论管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"comment\",\"children\":[],\"createTime\":1666879661000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2036,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-27 22:41:14');
INSERT INTO `sys_oper_log` VALUES ('359', '评论', '3', 'com.ruoyi.controller.AsqCommentController.remove()', 'DELETE', '1', 'admin', null, '/comment/comment/7', '127.0.0.1', '内网IP', '{commentIds=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 13:40:57');
INSERT INTO `sys_oper_log` VALUES ('360', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"ada1\",\"comBodyId\":2,\"commentId\":11,\"comBodyType\":1,\"createDate\":1666962891258}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:14:51');
INSERT INTO `sys_oper_log` VALUES ('361', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"sadasdsa\",\"userId\":1,\"comBodyId\":2,\"commentId\":12,\"comBodyType\":1,\"createDate\":1666963027778}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:17:07');
INSERT INTO `sys_oper_log` VALUES ('362', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":12,\"params\":{},\"commentText\":\"sadas\",\"userId\":1,\"comBodyId\":2,\"commentId\":13,\"comBodyType\":1,\"createDate\":1666963494100}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:24:54');
INSERT INTO `sys_oper_log` VALUES ('363', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":12,\"params\":{},\"commentText\":\"sadas\",\"userId\":1,\"comBodyId\":2,\"commentId\":14,\"comBodyType\":1,\"createDate\":1666963496106}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:24:56');
INSERT INTO `sys_oper_log` VALUES ('364', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":12,\"params\":{},\"commentText\":\"111\",\"userId\":1,\"comBodyId\":2,\"commentId\":15,\"comBodyType\":1,\"createDate\":1666963622229}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:27:02');
INSERT INTO `sys_oper_log` VALUES ('365', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":12,\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":2,\"commentId\":16,\"comBodyType\":1,\"createDate\":1666963782857}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:29:42');
INSERT INTO `sys_oper_log` VALUES ('366', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":9,\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":2,\"commentId\":17,\"comBodyType\":1,\"createDate\":1666964058461}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:34:18');
INSERT INTO `sys_oper_log` VALUES ('367', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":9,\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":2,\"commentId\":18,\"comBodyType\":1,\"createDate\":1666964059603}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:34:19');
INSERT INTO `sys_oper_log` VALUES ('368', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"啊实打实\",\"userId\":1,\"comBodyId\":2,\"commentId\":19,\"comBodyType\":1,\"createDate\":1666964091181}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:34:51');
INSERT INTO `sys_oper_log` VALUES ('369', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":19,\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":2,\"commentId\":20,\"comBodyType\":1,\"createDate\":1666964236855}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:37:16');
INSERT INTO `sys_oper_log` VALUES ('370', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":19,\"params\":{},\"commentText\":\"阿斯达\",\"userId\":1,\"comBodyId\":2,\"commentId\":21,\"comBodyType\":1,\"createDate\":1666964408505}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:40:08');
INSERT INTO `sys_oper_log` VALUES ('371', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":19,\"params\":{},\"commentText\":\"12312\",\"userId\":1,\"comBodyId\":2,\"commentId\":22,\"comBodyType\":1,\"createDate\":1666964484894}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:41:24');
INSERT INTO `sys_oper_log` VALUES ('372', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":21,\"params\":{},\"commentText\":\"11\",\"userId\":1,\"comBodyId\":2,\"commentId\":23,\"comBodyType\":1,\"createDate\":1666964620072}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:43:40');
INSERT INTO `sys_oper_log` VALUES ('373', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":19,\"params\":{},\"commentText\":\"1231\",\"userId\":1,\"comBodyId\":2,\"commentId\":24,\"comBodyType\":1,\"createDate\":1666964972303}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:49:32');
INSERT INTO `sys_oper_log` VALUES ('374', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":19,\"params\":{},\"commentText\":\"11\",\"userId\":1,\"comBodyId\":2,\"commentId\":25,\"comBodyType\":1,\"createDate\":1666965099351}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:51:39');
INSERT INTO `sys_oper_log` VALUES ('375', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":24,\"params\":{},\"commentText\":\"11\",\"userId\":1,\"comBodyId\":2,\"commentId\":26,\"comBodyType\":1,\"createDate\":1666965104251}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:51:44');
INSERT INTO `sys_oper_log` VALUES ('376', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":26,\"params\":{},\"commentText\":\"11\",\"userId\":1,\"comBodyId\":2,\"commentId\":27,\"comBodyType\":1,\"createDate\":1666965108097}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:51:48');
INSERT INTO `sys_oper_log` VALUES ('377', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":27,\"params\":{},\"commentText\":\"11\",\"userId\":1,\"comBodyId\":2,\"commentId\":28,\"comBodyType\":1,\"createDate\":1666965111285}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:51:51');
INSERT INTO `sys_oper_log` VALUES ('378', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":28,\"params\":{},\"commentText\":\"11\",\"userId\":1,\"comBodyId\":2,\"commentId\":29,\"comBodyType\":1,\"createDate\":1666965114315}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:51:54');
INSERT INTO `sys_oper_log` VALUES ('379', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":29,\"params\":{},\"commentText\":\"11\",\"userId\":1,\"comBodyId\":2,\"commentId\":30,\"comBodyType\":1,\"createDate\":1666965117489}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:51:57');
INSERT INTO `sys_oper_log` VALUES ('380', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":30,\"params\":{},\"commentText\":\"1122\",\"userId\":1,\"comBodyId\":2,\"commentId\":31,\"comBodyType\":1,\"createDate\":1666965121078}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:52:01');
INSERT INTO `sys_oper_log` VALUES ('381', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"0\",\"userId\":1,\"comBodyId\":2,\"commentId\":32,\"comBodyType\":1,\"createDate\":1666965137098}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:52:17');
INSERT INTO `sys_oper_log` VALUES ('382', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":32,\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":2,\"commentId\":33,\"comBodyType\":1,\"createDate\":1666965140941}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:52:20');
INSERT INTO `sys_oper_log` VALUES ('383', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":33,\"params\":{},\"commentText\":\"2\",\"userId\":1,\"comBodyId\":2,\"commentId\":34,\"comBodyType\":1,\"createDate\":1666965145185}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:52:25');
INSERT INTO `sys_oper_log` VALUES ('384', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":34,\"params\":{},\"commentText\":\"3\",\"userId\":1,\"comBodyId\":2,\"commentId\":35,\"comBodyType\":1,\"createDate\":1666965149897}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:52:29');
INSERT INTO `sys_oper_log` VALUES ('385', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":33,\"params\":{},\"commentText\":\"1-0\",\"userId\":1,\"comBodyId\":2,\"commentId\":36,\"comBodyType\":1,\"createDate\":1666965164197}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:52:44');
INSERT INTO `sys_oper_log` VALUES ('386', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":36,\"params\":{},\"commentText\":\"1-1\",\"userId\":1,\"comBodyId\":2,\"commentId\":37,\"comBodyType\":1,\"createDate\":1666965182573}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:53:02');
INSERT INTO `sys_oper_log` VALUES ('387', '评论', '2', 'com.ruoyi.controller.AsqCommentController.edit()', 'PUT', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":36,\"params\":{},\"commentText\":\"1-1\",\"userId\":1,\"isVio\":0,\"likeNum\":0,\"comBodyId\":2,\"commentId\":37,\"comBodyType\":1,\"createDate\":1666965183000}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 21:53:34');
INSERT INTO `sys_oper_log` VALUES ('388', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":32,\"params\":{},\"commentText\":\"11111\",\"userId\":1,\"comBodyId\":2,\"commentId\":38,\"comBodyType\":1,\"createDate\":1666965918874}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:05:18');
INSERT INTO `sys_oper_log` VALUES ('389', '评论', '2', 'com.ruoyi.controller.AsqCommentController.edit()', 'PUT', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":0,\"params\":{},\"commentText\":\"0\",\"userId\":1,\"isVio\":1,\"likeNum\":1,\"comBodyId\":2,\"commentId\":32,\"comBodyType\":1,\"createDate\":1666965137000}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:15:58');
INSERT INTO `sys_oper_log` VALUES ('390', '评论', '2', 'com.ruoyi.controller.AsqCommentController.edit()', 'PUT', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":32,\"params\":{},\"commentText\":\"1\",\"userId\":1,\"isVio\":1,\"likeNum\":1,\"comBodyId\":2,\"commentId\":33,\"comBodyType\":1,\"createDate\":1666965141000}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:22:32');
INSERT INTO `sys_oper_log` VALUES ('391', '评论', '2', 'com.ruoyi.controller.AsqCommentController.edit()', 'PUT', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":33,\"params\":{},\"commentText\":\"2\",\"userId\":1,\"isVio\":1,\"likeNum\":1,\"comBodyId\":2,\"commentId\":34,\"comBodyType\":1,\"createDate\":1666965145000}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:22:33');
INSERT INTO `sys_oper_log` VALUES ('392', '评论', '2', 'com.ruoyi.controller.AsqCommentController.edit()', 'PUT', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":33,\"params\":{},\"commentText\":\"2\",\"userId\":1,\"isVio\":1,\"likeNum\":0,\"comBodyId\":2,\"commentId\":34,\"comBodyType\":1,\"createDate\":1666965145000}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:22:41');
INSERT INTO `sys_oper_log` VALUES ('393', '评论', '2', 'com.ruoyi.controller.AsqCommentController.edit()', 'PUT', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":32,\"params\":{},\"commentText\":\"1\",\"userId\":1,\"isVio\":1,\"likeNum\":2,\"comBodyId\":2,\"commentId\":33,\"comBodyType\":1,\"createDate\":1666965141000}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:26:47');
INSERT INTO `sys_oper_log` VALUES ('394', '评论', '2', 'com.ruoyi.controller.AsqCommentController.edit()', 'PUT', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":33,\"params\":{},\"commentText\":\"2\",\"userId\":1,\"isVio\":1,\"likeNum\":1,\"comBodyId\":2,\"commentId\":34,\"comBodyType\":1,\"createDate\":1666965145000}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:26:48');
INSERT INTO `sys_oper_log` VALUES ('395', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":39,\"comBodyType\":1,\"createDate\":1666967382882}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:42');
INSERT INTO `sys_oper_log` VALUES ('396', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":40,\"comBodyType\":1,\"createDate\":1666967385340}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:45');
INSERT INTO `sys_oper_log` VALUES ('397', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":41,\"comBodyType\":1,\"createDate\":1666967385858}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:45');
INSERT INTO `sys_oper_log` VALUES ('398', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":42,\"comBodyType\":1,\"createDate\":1666967385936}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:45');
INSERT INTO `sys_oper_log` VALUES ('399', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":43,\"comBodyType\":1,\"createDate\":1666967386017}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:46');
INSERT INTO `sys_oper_log` VALUES ('400', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":44,\"comBodyType\":1,\"createDate\":1666967386172}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:46');
INSERT INTO `sys_oper_log` VALUES ('401', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":45,\"comBodyType\":1,\"createDate\":1666967386343}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:46');
INSERT INTO `sys_oper_log` VALUES ('402', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":46,\"comBodyType\":1,\"createDate\":1666967386505}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:46');
INSERT INTO `sys_oper_log` VALUES ('403', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":2,\"commentId\":47,\"comBodyType\":1,\"createDate\":1666967386673}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-28 22:29:46');
INSERT INTO `sys_oper_log` VALUES ('404', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666972074540,\"params\":{},\"userId\":1,\"bookId\":2}', 'null', '1', 'syntax error, pos 2, line 1, column 3[com.ruoyi.domain.AsqBooks@74de4978[\r\n  bookId=2\r\n  bookWriterName=liefox\r\n  bookName=JAVA修炼手册\r\n  bookType=Java\r\n  bookPubDate=Fri Jul 29 00:00:00 CST 2022\r\n  bookUpDate=Tue Jul 26 00:00:00 CST 2022\r\n  bookPress=人民出版社\r\n  bookCover=/profile/upload/2022/07/26/b96ee2d7-500d-4ae9-b7f0-5aa470fca2cc.png\r\n], com.ruoyi.domain.AsqBooks@6d33a713[\r\n  bookId=1\r\n  bookWriterName=liefox\r\n  bookName=前后端分离\r\n  bookType=Java\r\n  bookPubDate=Tue Mar 22 00:00:00 CST 2022\r\n  bookUpDate=Tue Mar 22 00:00:00 CST 2022\r\n  bookPress=拾柒工作室\r\n  bookCover=/profile/upload/2022/03/22/be03beb9-da38-40ab-b667-558f7af26e8d.jpeg\r\n], com.ruoyi.domain.AsqBooks@257e55c5[\r\n  bookId=4\r\n  bookWriterName=拾柒图书1\r\n  bookName=妇女之友\r\n  bookType=生活\r\n  bookPubDate=Sun Jul 17 00:00:00 CST 2022\r\n  bookUpDate=Thu Jul 28 00:00:00 CST 2022\r\n  bookPress=XX出版社\r\n  bookCover=/profile/upload/2022/07/28/e6a09893-b636-4cc0-9f98-60d56178710b.jpg\r\n]]', '2022-10-28 23:47:54');
INSERT INTO `sys_oper_log` VALUES ('405', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666975275948,\"params\":{},\"userId\":1,\"bookId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 00:41:15');
INSERT INTO `sys_oper_log` VALUES ('406', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/2', '127.0.0.1', '内网IP', '{bookId=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 00:41:53');
INSERT INTO `sys_oper_log` VALUES ('407', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1666975793692,\"params\":{},\"userId\":1,\"bookId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 00:49:53');
INSERT INTO `sys_oper_log` VALUES ('408', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/2', '127.0.0.1', '内网IP', '{bookId=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 11:55:45');
INSERT INTO `sys_oper_log` VALUES ('409', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667017632425,\"params\":{},\"userId\":1,\"bookId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:27:12');
INSERT INTO `sys_oper_log` VALUES ('410', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667018134398,\"params\":{},\"userId\":1,\"bookId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:35:34');
INSERT INTO `sys_oper_log` VALUES ('411', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:35:55');
INSERT INTO `sys_oper_log` VALUES ('412', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667018156926,\"params\":{},\"userId\":1,\"bookId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:35:56');
INSERT INTO `sys_oper_log` VALUES ('413', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/1', '127.0.0.1', '内网IP', '{bookId=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:38:59');
INSERT INTO `sys_oper_log` VALUES ('414', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667018339945,\"params\":{},\"userId\":1,\"bookId\":1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:38:59');
INSERT INTO `sys_oper_log` VALUES ('415', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667018363143,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:40:16');
INSERT INTO `sys_oper_log` VALUES ('416', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667018506882,\"params\":{},\"userId\":1,\"bookId\":8}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:41:46');
INSERT INTO `sys_oper_log` VALUES ('417', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667018593310,\"params\":{},\"userId\":1,\"bookId\":8}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:43:13');
INSERT INTO `sys_oper_log` VALUES ('418', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667018629261,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:43:49');
INSERT INTO `sys_oper_log` VALUES ('419', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667018893298,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:48:13');
INSERT INTO `sys_oper_log` VALUES ('420', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667019140589,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 12:52:20');
INSERT INTO `sys_oper_log` VALUES ('421', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667022965585,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 13:56:05');
INSERT INTO `sys_oper_log` VALUES ('422', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:00:31');
INSERT INTO `sys_oper_log` VALUES ('423', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667023234756,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:00:34');
INSERT INTO `sys_oper_log` VALUES ('424', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:01:04');
INSERT INTO `sys_oper_log` VALUES ('425', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667023292626,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:01:32');
INSERT INTO `sys_oper_log` VALUES ('426', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:04:42');
INSERT INTO `sys_oper_log` VALUES ('427', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667023509525,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:05:09');
INSERT INTO `sys_oper_log` VALUES ('428', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:05:16');
INSERT INTO `sys_oper_log` VALUES ('429', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667023519308,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:05:19');
INSERT INTO `sys_oper_log` VALUES ('430', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:06:14');
INSERT INTO `sys_oper_log` VALUES ('431', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667023626029,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:07:06');
INSERT INTO `sys_oper_log` VALUES ('432', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:22:45');
INSERT INTO `sys_oper_log` VALUES ('433', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667024615871,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:23:35');
INSERT INTO `sys_oper_log` VALUES ('434', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:24:54');
INSERT INTO `sys_oper_log` VALUES ('435', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667024724333,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:25:24');
INSERT INTO `sys_oper_log` VALUES ('436', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:26:30');
INSERT INTO `sys_oper_log` VALUES ('437', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667024841163,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:27:21');
INSERT INTO `sys_oper_log` VALUES ('438', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:27:37');
INSERT INTO `sys_oper_log` VALUES ('439', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667024884625,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:28:04');
INSERT INTO `sys_oper_log` VALUES ('440', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:29:09');
INSERT INTO `sys_oper_log` VALUES ('441', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667025128029,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:32:08');
INSERT INTO `sys_oper_log` VALUES ('442', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:34:06');
INSERT INTO `sys_oper_log` VALUES ('443', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667025278661,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:34:38');
INSERT INTO `sys_oper_log` VALUES ('444', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:35:06');
INSERT INTO `sys_oper_log` VALUES ('445', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667025408439,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:36:48');
INSERT INTO `sys_oper_log` VALUES ('446', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:39:12');
INSERT INTO `sys_oper_log` VALUES ('447', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667025554569,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:39:14');
INSERT INTO `sys_oper_log` VALUES ('448', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 14:39:42');
INSERT INTO `sys_oper_log` VALUES ('449', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667027679203,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:14:39');
INSERT INTO `sys_oper_log` VALUES ('450', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:15:34');
INSERT INTO `sys_oper_log` VALUES ('451', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667027756659,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:15:56');
INSERT INTO `sys_oper_log` VALUES ('452', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:16:33');
INSERT INTO `sys_oper_log` VALUES ('453', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667028802436,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:33:22');
INSERT INTO `sys_oper_log` VALUES ('454', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:33:44');
INSERT INTO `sys_oper_log` VALUES ('455', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667029111832,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:38:31');
INSERT INTO `sys_oper_log` VALUES ('456', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:38:56');
INSERT INTO `sys_oper_log` VALUES ('457', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667029231156,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:40:31');
INSERT INTO `sys_oper_log` VALUES ('458', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:41:09');
INSERT INTO `sys_oper_log` VALUES ('459', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667029313530,\"params\":{},\"userId\":1,\"bookId\":7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:41:53');
INSERT INTO `sys_oper_log` VALUES ('460', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/7', '127.0.0.1', '内网IP', '{bookId=7}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:44:40');
INSERT INTO `sys_oper_log` VALUES ('461', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":7,\"commentId\":48,\"comBodyType\":1,\"createDate\":1667029772932}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 15:49:32');
INSERT INTO `sys_oper_log` VALUES ('462', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":48,\"params\":{},\"commentText\":\"123\",\"userId\":1,\"comBodyId\":7,\"commentId\":49,\"comBodyType\":1,\"createDate\":1667030789949}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 16:06:29');
INSERT INTO `sys_oper_log` VALUES ('463', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":49,\"params\":{},\"commentText\":\"11\",\"userId\":1,\"comBodyId\":7,\"commentId\":50,\"comBodyType\":1,\"createDate\":1667030794125}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 16:06:34');
INSERT INTO `sys_oper_log` VALUES ('464', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667034379023,\"params\":{},\"userId\":1,\"bookId\":2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 17:06:19');
INSERT INTO `sys_oper_log` VALUES ('465', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/2', '127.0.0.1', '内网IP', '{bookId=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 17:07:51');
INSERT INTO `sys_oper_log` VALUES ('466', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667036187435,\"params\":{},\"userId\":1,\"bookId\":5}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 17:36:27');
INSERT INTO `sys_oper_log` VALUES ('467', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667036229550,\"params\":{},\"userId\":1,\"bookId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 17:37:09');
INSERT INTO `sys_oper_log` VALUES ('468', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/6', '127.0.0.1', '内网IP', '{bookId=6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 17:37:30');
INSERT INTO `sys_oper_log` VALUES ('469', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667036253624,\"params\":{},\"userId\":1,\"bookId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 17:37:33');
INSERT INTO `sys_oper_log` VALUES ('470', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/6', '127.0.0.1', '内网IP', '{bookId=6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 17:38:51');
INSERT INTO `sys_oper_log` VALUES ('471', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667036391395,\"params\":{},\"userId\":1,\"bookId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 17:39:51');
INSERT INTO `sys_oper_log` VALUES ('472', '用户喜欢', '3', 'com.ruoyi.controller.AsqApplaudController.remove()', 'DELETE', '1', 'admin', null, '/applaud/applaud/6', '127.0.0.1', '内网IP', '{bookId=6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 18:02:52');
INSERT INTO `sys_oper_log` VALUES ('473', '用户喜欢', '1', 'com.ruoyi.controller.AsqApplaudController.add()', 'POST', '1', 'admin', null, '/applaud/applaud', '127.0.0.1', '内网IP', '{\"creadData\":1667038038486,\"params\":{},\"userId\":1,\"bookId\":6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 18:07:18');
INSERT INTO `sys_oper_log` VALUES ('474', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 'asq_topic', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 19:54:57');
INSERT INTO `sys_oper_log` VALUES ('475', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"TopicId\",\"usableColumn\":false,\"columnId\":63,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1667044497000,\"tableId\":11,\"pk\":true,\"columnName\":\"topic_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":64,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1667044497000,\"isEdit\":\"1\",\"tableId\":11,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"TopicContent\",\"usableColumn\":false,\"columnId\":65,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicContent\",\"htmlType\":\"editor\",\"edit\":true,\"query\":false,\"columnComment\":\"文章\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"text\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1667044497000,\"isEdit\":\"1\",\"tableId\":11,\"pk\":false,\"columnName\":\"topic_content\"},{\"capJavaField\":\"TopicCover\",\"usableColumn\":false,\"columnId\":66,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicCover\",\"htmlType\":\"imageUpload\",\"edit\":true,\"query\":false,\"columnComment\":\"封面\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1667044497000,\"isEdit\":\"1\",', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 19:58:09');
INSERT INTO `sys_oper_log` VALUES ('476', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.synchDb()', 'GET', '1', 'admin', null, '/tool/gen/synchDb/asq_topic', '127.0.0.1', '内网IP', '{tableName=asq_topic}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 19:58:14');
INSERT INTO `sys_oper_log` VALUES ('477', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"TopicId\",\"usableColumn\":false,\"columnId\":63,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1667044689000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1667044497000,\"tableId\":11,\"pk\":true,\"columnName\":\"topic_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":64,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"isQuery\":\"1\",\"updateTime\":1667044689000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1667044497000,\"isEdit\":\"1\",\"tableId\":11,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"TopicContent\",\"usableColumn\":false,\"columnId\":65,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicContent\",\"htmlType\":\"editor\",\"edit\":true,\"query\":false,\"columnComment\":\"文章\",\"updateTime\":1667044689000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"text\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1667044497000,\"isEdit\":\"1\",\"tableId\":11,\"pk\":false,\"columnName\":\"topic_content\"},{\"capJavaField\":\"TopicName\",\"usableColumn\":false,\"columnId\":68,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"话题\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnTyp', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 19:58:36');
INSERT INTO `sys_oper_log` VALUES ('478', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"documentation\",\"orderNum\":\"4\",\"menuName\":\"话题管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"topic\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 19:59:44');
INSERT INTO `sys_oper_log` VALUES ('479', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"subTableName\":\"\",\"functionAuthor\":\"zjh\",\"columns\":[{\"capJavaField\":\"TopicId\",\"usableColumn\":false,\"columnId\":63,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicId\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"updateTime\":1667044716000,\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1667044497000,\"tableId\":11,\"pk\":true,\"columnName\":\"topic_id\"},{\"capJavaField\":\"UserId\",\"usableColumn\":false,\"columnId\":64,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"userId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"isQuery\":\"1\",\"updateTime\":1667044716000,\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"int(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1667044497000,\"isEdit\":\"1\",\"tableId\":11,\"pk\":false,\"columnName\":\"user_id\"},{\"capJavaField\":\"TopicContent\",\"usableColumn\":false,\"columnId\":65,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicContent\",\"htmlType\":\"editor\",\"edit\":true,\"query\":false,\"columnComment\":\"文章\",\"updateTime\":1667044716000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"text\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1667044497000,\"isEdit\":\"1\",\"tableId\":11,\"pk\":false,\"columnName\":\"topic_content\"},{\"capJavaField\":\"TopicName\",\"usableColumn\":false,\"columnId\":68,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"topicName\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"话题\",\"isQuery\":\"1\",\"updateTime\":1667044716000,\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"q', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 20:00:11');
INSERT INTO `sys_oper_log` VALUES ('480', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', 'null', '0', null, '2022-10-29 20:00:48');
INSERT INTO `sys_oper_log` VALUES ('481', '话题', '1', 'com.ruoyi.controller.AsqTopicController.add()', 'POST', '1', 'admin', null, '/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"topicId\":1,\"topicContent\":\"<p><span style=\\\"color: rgb(230, 0, 0);\\\" class=\\\"ql-size-huge\\\">KFC，你拿什么和麦当劳比？</span></p><p><img src=\\\"https://pic3.zhimg.com/v2-de90624f55868384ea80dd109bfbc41a_b.jpg\\\" width=\\\"972\\\"></p><p><br></p><p><br></p><p><img src=\\\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\\\" width=\\\"480\\\"></p><p><br></p><p><br></p><p><img src=\\\"https://pica.zhimg.com/v2-572fdda412d59fdeef71211e013f3922_b.jpg\\\" width=\\\"1078\\\"></p>\",\"topicName\":\"kfc疯狂星期四怎么越来越拉跨了？\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 20:19:54');
INSERT INTO `sys_oper_log` VALUES ('482', '话题', '2', 'com.ruoyi.controller.AsqTopicController.edit()', 'PUT', '1', 'admin', null, '/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"topicCover\":\"/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg\",\"topicId\":1,\"topicContent\":\"<p><span class=\\\"ql-size-huge\\\" style=\\\"color: rgb(230, 0, 0);\\\">KFC，你拿什么和麦当劳比？</span></p><p><img src=\\\"https://pic3.zhimg.com/v2-de90624f55868384ea80dd109bfbc41a_b.jpg\\\" width=\\\"972\\\"></p><p><br></p><p><br></p><p><img src=\\\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\\\" width=\\\"480\\\"></p><p><br></p><p><br></p><p><img src=\\\"https://pica.zhimg.com/v2-572fdda412d59fdeef71211e013f3922_b.jpg\\\" width=\\\"1078\\\"></p>\",\"topicName\":\"kfc疯狂星期四怎么越来越拉跨了？\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 20:20:45');
INSERT INTO `sys_oper_log` VALUES ('483', '话题', '2', 'com.ruoyi.controller.AsqTopicController.edit()', 'PUT', '1', 'admin', null, '/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"topicCover\":\"/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg\",\"topicId\":1,\"topicContent\":\"<p><span class=\\\"ql-size-huge\\\" style=\\\"color: rgb(230, 0, 0);\\\">KFC，你拿什么和麦当劳比？</span></p><p><img src=\\\"https://pic3.zhimg.com/v2-de90624f55868384ea80dd109bfbc41a_b.jpg\\\" width=\\\"972\\\"></p><p><br></p><p><br></p><p><img src=\\\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\\\" width=\\\"480\\\"></p><p><br></p><p><br></p><p><img src=\\\"https://pica.zhimg.com/v2-572fdda412d59fdeef71211e013f3922_b.jpg\\\" width=\\\"1078\\\"></p>\",\"topicName\":\"kfc疯狂星期四怎么越来越拉跨了？\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 20:22:10');
INSERT INTO `sys_oper_log` VALUES ('484', '话题', '1', 'com.ruoyi.controller.AsqTopicController.add()', 'POST', '1', 'admin', null, '/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"userId\":1,\"topicId\":2,\"topicContent\":\"<p>不知道提问者现在是具体是什么基础，不过没关系，正好回答的全一点，方便浏览到这个问题的其他人。</p><p>有的同学还在读大学，代码还没写过几行；有的同学已经工作数十年，这之间的差别还是挺大的。而不同基础的人，适宜看的书是完全不一样的。因此，针对<strong>不同层次、不同语言</strong>的同学，分别推荐了不同的书。</p><p>不管是不是科班出身，每一个程序员都应该花时间了解和学习计算机科学相关的基础知识，因为<strong>所有关于如何编程的底层逻辑和原理都在那里了</strong>。</p><p>计算机基础知识汇总了包括<strong>数据结构与算法、数学、操作系统、计算机组成原理、</strong><a href=\\\"https://www.zhihu.com/search?q=%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\"><strong>计算机网络</strong></a><strong>、</strong><a href=\\\"https://www.zhihu.com/search?q=%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\"><strong>软件工程</strong></a><strong>在内的常用计算机科学知识</strong>，以帮你快速建立对计算机科学的大局观。</p><p>有很多同学问到，这些基础到底该怎么补充，顺序是怎样的，有什么资料，我索性抽时间把我这些年压箱底的学习资料倒腾了一下，精选了几本我认为最优质的，<strong>学习资料在于精，不在于多，多反而不是好事，最为一名程序员，大家的学习时间都太宝贵了，我们要用有限的时间学习最有价值的内容</strong>，具体内容可以这篇资料贴。</p><p><br></p><p><span style=\\\"background-color: transparent;\\\"><img src=\\\"https://pic2.zhimg.com/80/v2-cee3efd664ce1503a914bb103dd44317_720w.webp\\\" height=\\\"528\\\" width=\\\"720\\\"></span></p><h2>针对入门的趣味书</h2><p>入门的同学，我建议你不要过度追求上去就看经典书。像《算法导论》《算法》这些书，虽然比较经典、比较权威，但是非常厚。初学就去啃这些书肯定会很费劲。而一旦啃不下来，挫败感就会很强。所以，入门的同学，我建议你找一些比较容易看的书来看，比如《大话数据结构》和《<a href=\\\"https://www.zhihu.com/search?q=%E7%AE%97%E6%B3%95%E5%9B%BE%E8%A7%A3&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\" style=\\\"color: rgb(23, 81, 153);\\\">算法图解</a>》。你不要太在意书写得深浅，重要的是能不能坚持看完。</p><p>《大话数据结构》这本书最大的特点是，它把理论讲得很有趣，不枯燥。而且每个数据结构和算法，作者都结合生活中的例子进行了讲解， 能让你有非常直观的感受。虽然这本书有400多页，但是花', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 20:26:03');
INSERT INTO `sys_oper_log` VALUES ('485', '话题', '2', 'com.ruoyi.controller.AsqTopicController.edit()', 'PUT', '1', 'admin', null, '/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"userId\":1,\"topicCover\":\"/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg\",\"topicId\":1,\"topicContent\":\"<p><span class=\\\"ql-size-huge\\\" style=\\\"color: rgb(230, 0, 0);\\\">KFC，你拿什么和麦当劳比？</span></p><p><br></p><p><img src=\\\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\\\" width=\\\"480\\\"></p>\",\"topicCreatedate\":1666972800000,\"topicName\":\"kfc疯狂星期四怎么越来越拉跨了？\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 20:54:12');
INSERT INTO `sys_oper_log` VALUES ('486', '话题', '2', 'com.ruoyi.controller.AsqTopicController.edit()', 'PUT', '1', 'admin', null, '/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"userId\":1,\"topicId\":1,\"topicContent\":\"<p>不知道提问者现在是具体是什么基础，不过没关系，正好回答的全一点，方便浏览到这个问题的其他人。</p><p>有的同学还在读大学，代码还没写过几行；有的同学已经工作数十年，这之间的差别还是挺大的。而不同基础的人，适宜看的书是完全不一样的。因此，针对<strong>不同层次、不同语言</strong>的同学，分别推荐了不同的书。</p><p>不管是不是科班出身，每一个程序员都应该花时间了解和学习计算机科学相关的基础知识，因为<strong>所有关于如何编程的底层逻辑和原理都在那里了</strong>。</p><p>计算机基础知识汇总了包括<strong>数据结构与算法、数学、操作系统、计算机组成原理、</strong><a href=\\\"https://www.zhihu.com/search?q=%E8%AE%A1%E7%AE%97%E6%9C%BA%E7%BD%91%E7%BB%9C&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\"><strong>计算机网络</strong></a><strong>、</strong><a href=\\\"https://www.zhihu.com/search?q=%E8%BD%AF%E4%BB%B6%E5%B7%A5%E7%A8%8B&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\"><strong>软件工程</strong></a><strong>在内的常用计算机科学知识</strong>，以帮你快速建立对计算机科学的大局观。</p><p>有很多同学问到，这些基础到底该怎么补充，顺序是怎样的，有什么资料，我索性抽时间把我这些年压箱底的学习资料倒腾了一下，精选了几本我认为最优质的，<strong>学习资料在于精，不在于多，多反而不是好事，最为一名程序员，大家的学习时间都太宝贵了，我们要用有限的时间学习最有价值的内容</strong>，具体内容可以这篇资料贴。</p><p class=\\\"ql-indent-1 ql-align-center\\\"><span style=\\\"background-color: transparent;\\\"><img src=\\\"https://pic2.zhimg.com/80/v2-cee3efd664ce1503a914bb103dd44317_720w.webp\\\" height=\\\"528\\\" width=\\\"720\\\"></span></p><h2>针对入门的趣味书</h2><p>入门的同学，我建议你不要过度追求上去就看经典书。像《算法导论》《算法》这些书，虽然比较经典、比较权威，但是非常厚。初学就去啃这些书肯定会很费劲。而一旦啃不下来，挫败感就会很强。所以，入门的同学，我建议你找一些比较容易看的书来看，比如《大话数据结构》和《<a href=\\\"https://www.zhihu.com/search?q=%E7%AE%97%E6%B3%95%E5%9B%BE%E8%A7%A3&amp;search_source=Entity&amp;hybrid_search_source=Entity&amp;hybrid_search_extra=%7B%22sourceType%22%3A%22answer%22%2C%22sourceId%22%3A%22688744392%22%7D\\\" rel=\\\"noopener noreferrer\\\" target=\\\"_blank\\\" style=\\\"color: rgb(23, 81, 153);\\\">算法图解</a>》。你不要太在意书写得深浅，重要的是能不能坚持看完。</p><p>《大话数据结构》这本书最大的特点是，它把理论讲得很有趣，不枯燥。而且每个数据结构和算法，作者都结合生活中的例子进行了讲解， ', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 22:22:12');
INSERT INTO `sys_oper_log` VALUES ('487', '话题', '2', 'com.ruoyi.controller.AsqTopicController.edit()', 'PUT', '1', 'admin', null, '/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"userId\":1,\"topicCover\":\"/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg\",\"topicId\":2,\"topicContent\":\"<p><img src=\\\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\\\" width=\\\"480\\\"></p><p><span style=\\\"color: rgb(230, 0, 0);\\\" class=\\\"ql-size-huge\\\">KFC，你拿什么和麦当劳比？</span></p>\",\"topicCreatedate\":1666972800000,\"topicName\":\"kfc疯狂星期四怎么越来越拉跨了？\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 22:32:41');
INSERT INTO `sys_oper_log` VALUES ('488', '话题', '2', 'com.ruoyi.controller.AsqTopicController.edit()', 'PUT', '1', 'admin', null, '/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"userId\":1,\"topicCover\":\"/profile/upload/2022/10/29/4ccdfe61-3e32-44ae-a873-e7290aa20f63.jpg\",\"topicId\":2,\"topicContent\":\"<p><span class=\\\"ql-size-huge\\\" style=\\\"color: rgb(230, 0, 0);\\\">KFC，你拿什么和麦当劳比？</span></p><p class=\\\"ql-align-center\\\"><span class=\\\"ql-size-small\\\"><img src=\\\"https://pic3.zhimg.com/v2-b55a59f70a82745b731b1e7dbde44fa8_b.jpg\\\" width=\\\"480\\\"></span></p>\",\"topicCreatedate\":1666972800000,\"topicName\":\"kfc疯狂星期四怎么越来越拉跨了？\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 22:34:05');
INSERT INTO `sys_oper_log` VALUES ('489', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":1,\"commentId\":51,\"comBodyType\":2,\"createDate\":1667058533522}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 23:48:53');
INSERT INTO `sys_oper_log` VALUES ('490', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"commentParentId\":51,\"params\":{},\"commentText\":\"阿达\",\"userId\":1,\"comBodyId\":1,\"commentId\":52,\"comBodyType\":2,\"createDate\":1667058579869}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 23:49:39');
INSERT INTO `sys_oper_log` VALUES ('491', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"阿斯达\",\"userId\":1,\"comBodyId\":1,\"commentId\":53,\"comBodyType\":2,\"createDate\":1667059036650}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-29 23:57:16');
INSERT INTO `sys_oper_log` VALUES ('492', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":2,\"commentId\":54,\"comBodyType\":2,\"createDate\":1667059283112}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-30 00:01:23');
INSERT INTO `sys_oper_log` VALUES ('493', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"12\",\"userId\":1,\"comBodyId\":1,\"commentId\":55,\"comBodyType\":2,\"createDate\":1667104296452}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-30 12:31:36');
INSERT INTO `sys_oper_log` VALUES ('494', '话题', '1', 'com.ruoyi.controller.PubAsqTopicController.add()', 'POST', '1', 'dev', null, '/public/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"userId\":3,\"commentNum\":0,\"topicId\":9,\"topicContent\":\"<p>1</p>\",\"topicCreatedate\":1667115525610,\"topicName\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-30 15:38:45');
INSERT INTO `sys_oper_log` VALUES ('495', '话题', '2', 'com.ruoyi.controller.PubAsqTopicController.edit()', 'PUT', '1', 'dev', null, '/public/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"userId\":3,\"commentNum\":0,\"topicId\":9,\"topicContent\":\"<p class=\\\"ql-align-center\\\">啊啊啊啊，腻害，Holland我的</p><p class=\\\"ql-align-center\\\"><img src=\\\"/dev-api/profile/upload/2022/10/30/941ae05f-4b2f-40ca-a66e-efb85371567c.png\\\"></p>\",\"topicCreatedate\":1667115526000,\"topicName\":\"这是我的第一骗话题\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-30 15:41:32');
INSERT INTO `sys_oper_log` VALUES ('496', '话题', '3', 'com.ruoyi.controller.PubAsqTopicController.remove()', 'DELETE', '1', 'dev', null, '/public/topic/topic/9', '127.0.0.1', '内网IP', '{topicIds=9}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-30 15:43:13');
INSERT INTO `sys_oper_log` VALUES ('497', '话题', '1', 'com.ruoyi.controller.PubAsqTopicController.add()', 'POST', '1', 'dev', null, '/public/topic/topic', '127.0.0.1', '内网IP', '{\"params\":{},\"userId\":3,\"commentNum\":0,\"topicId\":10,\"topicContent\":\"<p>12321321</p>\",\"topicCreatedate\":1667115799456,\"topicName\":\"阿斯达\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-30 15:43:19');
INSERT INTO `sys_oper_log` VALUES ('498', '评论', '1', 'com.ruoyi.controller.AsqCommentController.add()', 'POST', '1', 'admin', null, '/comment/comment', '127.0.0.1', '内网IP', '{\"params\":{},\"commentText\":\"1\",\"userId\":1,\"comBodyId\":1,\"commentId\":56,\"comBodyType\":2,\"createDate\":1667120265033}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-10-30 16:57:45');

-- ----------------------------
-- Table structure for sys_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1', 'ceo', '董事长', '1', '0', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_post` VALUES ('2', 'se', '项目经理', '2', '0', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_post` VALUES ('3', 'hr', '人力资源', '3', '0', 'admin', '2022-03-21 21:33:12', '', null, '');
INSERT INTO `sys_post` VALUES ('4', 'user', '普通员工', '4', '0', 'admin', '2022-03-21 21:33:12', '', null, '');

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) DEFAULT '1' COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) DEFAULT '1' COMMENT '部门树选择项是否关联显示',
  `status` char(1) NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'admin', '1', '1', '1', '1', '0', '0', 'admin', '2022-03-21 21:33:12', '', null, '超级管理员');
INSERT INTO `sys_role` VALUES ('2', '普通角色', 'common', '2', '2', '1', '1', '0', '0', 'admin', '2022-03-21 21:33:12', '', null, '普通角色');

-- ----------------------------
-- Table structure for sys_role_dept
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('2', '100');
INSERT INTO `sys_role_dept` VALUES ('2', '101');
INSERT INTO `sys_role_dept` VALUES ('2', '105');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('2', '1');
INSERT INTO `sys_role_menu` VALUES ('2', '2');
INSERT INTO `sys_role_menu` VALUES ('2', '3');
INSERT INTO `sys_role_menu` VALUES ('2', '4');
INSERT INTO `sys_role_menu` VALUES ('2', '100');
INSERT INTO `sys_role_menu` VALUES ('2', '101');
INSERT INTO `sys_role_menu` VALUES ('2', '102');
INSERT INTO `sys_role_menu` VALUES ('2', '103');
INSERT INTO `sys_role_menu` VALUES ('2', '104');
INSERT INTO `sys_role_menu` VALUES ('2', '105');
INSERT INTO `sys_role_menu` VALUES ('2', '106');
INSERT INTO `sys_role_menu` VALUES ('2', '107');
INSERT INTO `sys_role_menu` VALUES ('2', '108');
INSERT INTO `sys_role_menu` VALUES ('2', '109');
INSERT INTO `sys_role_menu` VALUES ('2', '110');
INSERT INTO `sys_role_menu` VALUES ('2', '111');
INSERT INTO `sys_role_menu` VALUES ('2', '112');
INSERT INTO `sys_role_menu` VALUES ('2', '113');
INSERT INTO `sys_role_menu` VALUES ('2', '114');
INSERT INTO `sys_role_menu` VALUES ('2', '115');
INSERT INTO `sys_role_menu` VALUES ('2', '116');
INSERT INTO `sys_role_menu` VALUES ('2', '500');
INSERT INTO `sys_role_menu` VALUES ('2', '501');
INSERT INTO `sys_role_menu` VALUES ('2', '1000');
INSERT INTO `sys_role_menu` VALUES ('2', '1001');
INSERT INTO `sys_role_menu` VALUES ('2', '1002');
INSERT INTO `sys_role_menu` VALUES ('2', '1003');
INSERT INTO `sys_role_menu` VALUES ('2', '1004');
INSERT INTO `sys_role_menu` VALUES ('2', '1005');
INSERT INTO `sys_role_menu` VALUES ('2', '1006');
INSERT INTO `sys_role_menu` VALUES ('2', '1007');
INSERT INTO `sys_role_menu` VALUES ('2', '1008');
INSERT INTO `sys_role_menu` VALUES ('2', '1009');
INSERT INTO `sys_role_menu` VALUES ('2', '1010');
INSERT INTO `sys_role_menu` VALUES ('2', '1011');
INSERT INTO `sys_role_menu` VALUES ('2', '1012');
INSERT INTO `sys_role_menu` VALUES ('2', '1013');
INSERT INTO `sys_role_menu` VALUES ('2', '1014');
INSERT INTO `sys_role_menu` VALUES ('2', '1015');
INSERT INTO `sys_role_menu` VALUES ('2', '1016');
INSERT INTO `sys_role_menu` VALUES ('2', '1017');
INSERT INTO `sys_role_menu` VALUES ('2', '1018');
INSERT INTO `sys_role_menu` VALUES ('2', '1019');
INSERT INTO `sys_role_menu` VALUES ('2', '1020');
INSERT INTO `sys_role_menu` VALUES ('2', '1021');
INSERT INTO `sys_role_menu` VALUES ('2', '1022');
INSERT INTO `sys_role_menu` VALUES ('2', '1023');
INSERT INTO `sys_role_menu` VALUES ('2', '1024');
INSERT INTO `sys_role_menu` VALUES ('2', '1025');
INSERT INTO `sys_role_menu` VALUES ('2', '1026');
INSERT INTO `sys_role_menu` VALUES ('2', '1027');
INSERT INTO `sys_role_menu` VALUES ('2', '1028');
INSERT INTO `sys_role_menu` VALUES ('2', '1029');
INSERT INTO `sys_role_menu` VALUES ('2', '1030');
INSERT INTO `sys_role_menu` VALUES ('2', '1031');
INSERT INTO `sys_role_menu` VALUES ('2', '1032');
INSERT INTO `sys_role_menu` VALUES ('2', '1033');
INSERT INTO `sys_role_menu` VALUES ('2', '1034');
INSERT INTO `sys_role_menu` VALUES ('2', '1035');
INSERT INTO `sys_role_menu` VALUES ('2', '1036');
INSERT INTO `sys_role_menu` VALUES ('2', '1037');
INSERT INTO `sys_role_menu` VALUES ('2', '1038');
INSERT INTO `sys_role_menu` VALUES ('2', '1039');
INSERT INTO `sys_role_menu` VALUES ('2', '1040');
INSERT INTO `sys_role_menu` VALUES ('2', '1041');
INSERT INTO `sys_role_menu` VALUES ('2', '1042');
INSERT INTO `sys_role_menu` VALUES ('2', '1043');
INSERT INTO `sys_role_menu` VALUES ('2', '1044');
INSERT INTO `sys_role_menu` VALUES ('2', '1045');
INSERT INTO `sys_role_menu` VALUES ('2', '1046');
INSERT INTO `sys_role_menu` VALUES ('2', '1047');
INSERT INTO `sys_role_menu` VALUES ('2', '1048');
INSERT INTO `sys_role_menu` VALUES ('2', '1049');
INSERT INTO `sys_role_menu` VALUES ('2', '1050');
INSERT INTO `sys_role_menu` VALUES ('2', '1051');
INSERT INTO `sys_role_menu` VALUES ('2', '1052');
INSERT INTO `sys_role_menu` VALUES ('2', '1053');
INSERT INTO `sys_role_menu` VALUES ('2', '1054');
INSERT INTO `sys_role_menu` VALUES ('2', '1055');
INSERT INTO `sys_role_menu` VALUES ('2', '1056');
INSERT INTO `sys_role_menu` VALUES ('2', '1057');
INSERT INTO `sys_role_menu` VALUES ('2', '1058');
INSERT INTO `sys_role_menu` VALUES ('2', '1059');
INSERT INTO `sys_role_menu` VALUES ('2', '1060');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) DEFAULT '' COMMENT '手机号码',
  `sex` char(1) DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) DEFAULT '' COMMENT '密码',
  `status` char(1) DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '103', 'admin', 'liefox', '00', 'liefox@163.com', '15888888888', '1', '/profile/avatar/2022/09/13/6ba335a3-ba97-483c-bb11-fb5c2248fc6d.png', '$2a$10$f4.FI/KBJDYdXF4Q1eqLQe/asWsH43vwXu9jwYSNF3YCBwlRGPTBe', '0', '0', '127.0.0.1', '2022-10-30 16:33:03', 'admin', '2022-03-21 21:33:12', '', '2022-10-30 16:33:03', '管理员');
INSERT INTO `sys_user` VALUES ('2', '105', 'ry', 'liefox', '00', 'ry@qq.com', '15666666666', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-03-21 21:33:12', 'admin', '2022-03-21 21:33:12', '', null, '测试员');
INSERT INTO `sys_user` VALUES ('3', null, 'dev', 'dev', '00', '', '', '0', '', '$2a$10$eDeIfvIKlma9IvrVZ1P5GePxt6WPpJL3W3XnfpJ7fEmkzW0OPUaH6', '0', '0', '127.0.0.1', '2022-10-30 16:32:51', '', '2022-05-03 08:00:06', '', '2022-10-30 16:32:50', null);
INSERT INTO `sys_user` VALUES ('4', null, 'zjh', 'zjh', '00', '', '', '0', '', '$2a$10$9nhiGwIfHkwosqIQXF0wcuAq94GmZM/3Yb9oxq41x8G2.stICAEES', '0', '0', '127.0.0.1', '2022-10-29 18:28:07', '', '2022-07-24 16:23:06', '', '2022-10-29 18:28:07', null);
INSERT INTO `sys_user` VALUES ('5', null, '测试', '测试', '00', '', '', '0', '', '$2a$10$C98uMO77pAsINrKk/c13Mu8nW6PZ0FwxUCbcILCJ.gLIJFciIcoie', '0', '0', '127.0.0.1', '2022-07-24 17:29:51', '', '2022-07-24 17:29:39', '', '2022-07-24 17:29:51', null);

-- ----------------------------
-- Table structure for sys_user_post
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES ('1', '1');
INSERT INTO `sys_user_post` VALUES ('2', '2');

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('2', '2');
