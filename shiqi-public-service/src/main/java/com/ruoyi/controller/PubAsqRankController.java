package com.ruoyi.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.domain.AsqBooks;
import com.ruoyi.domain.AsqBooksRank;
import com.ruoyi.framework.websocket.WebSocketServer;
import com.ruoyi.service.IAsqBooksService;
import com.ruoyi.service.IAsqRankParmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 前台首页幻灯片Controller
 *
 * @author ruoyi
 * @date 2022-07-24
 */
@RestController
@RequestMapping("/public/rank")
public class PubAsqRankController extends BaseController {
    @Autowired
    private IAsqBooksService asqBooksService;
    @Autowired
    private IAsqRankParmService asqRankParmService;

    /**
     * 查询拾柒书库阅读总榜
     */
    @GetMapping("/readAllRank")
    public TableDataInfo list() {
        startPage();
        List<AsqBooksRank> list = asqBooksService.readAllRank();
        return getDataTable(list);
    }

    /**
     * 查询拾柒热榜
     */
    @GetMapping("/hotRank")
    public TableDataInfo listHot() {
        startPage();
        List<AsqBooks> asqBooks = asqRankParmService.selectAsqRankParmByHotList();
        return getDataTable(asqBooks);
    }

    /**
     * 猜你喜欢
     */
    @GetMapping("/GYWLike")
    public TableDataInfo GYWLike() {
        startPage();
        List<AsqBooks> list = asqBooksService.GYWLike();
        return getDataTable(list);
    }

}
