<div align="center" >
<img src="https://gitee.com/cxq21/book/raw/v2.0/sq-ui/public/%E6%8B%BE%E6%9F%921.jpg"/>
</div>

<h1 align="center">拾柒爱阅读 v2.0</h1>

<p align="center">
 <a href="https://gitee.com/cxq21" target="__blank"><img alt="JDK-1.8" src="https://img.shields.io/badge/JDK-%201.8-brightgreen"></a>
 <a href="https://gitee.com/cxq21" target="__blank"><img alt="SpringBoot-2.2.13.RELEASE" src="https://img.shields.io/badge/SpringBoot-2.2.13.RELEASE-blue"></a>
<a href="https://gitee.com/cxq21" target="__blank"><img alt="/Mysql-5.7.0" src="https://img.shields.io/badge/Mysql-5.7.0-red"></a>
<a href="https://gitee.com/cxq21" target="__blank"><img alt="/Redis-3.0" src="https://img.shields.io/badge/Redis-3.0-ff69b4"></a>
<a href="https://gitee.com/cxq21" target="__blank"><img alt="/Maven-3.0" src="https://img.shields.io/badge/Maven-3.0-orange"></a>
<a href="https://gitee.com/cxq21" target="__blank"><img alt="/Node-12" src="https://img.shields.io/badge/Node-12-9cf"></a>
<a href='https://gitee.com/cxq21/book/stargazers'><img src='https://gitee.com/cxq21/book/badge/star.svg?theme=dark' alt='star'></img></a>
</p>

# 300赞更新功能  :star:
<div align="center">
<img src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/gitee.png"/>
</div>

# 入门版请 coder 切换 master 分支  :heartpulse:

# :tw-1f4dc: 项目介绍

如果你停止，就是谷底。如果你还在继续，就是上坡。这是我听过关于人生低谷最好的阐述。借此我做了一个在线阅读平台，
使现代人可以利用碎片化来阅读。并且设计加入了社区功能，使得用户可以一起讨论阅读心得，并发表在社区里，产生一个生态闭环，实现用户快速裂变。

* 所有使用到的框架或者组件都是基于开源项目，代码保证100%开源。
*   **_目前应该可以称为是全网同类型开源项目中最美的 UI，设计细节满满_**
* 系统功能通用，无论是个人还是企业都可以利用该系统快速搭建一个属于自己的阅读 or 社区系统。

#### 该项目，分为三个平台

1. 后台管理平台 (图书管理模块  :heavy_plus_sign: 读者管理模块 :heavy_plus_sign: 创作者管理模块 :heavy_plus_sign: 社区管理模块）
2. 前台阅读平台 (阅读模块  :heavy_plus_sign: 社区模块 :heavy_plus_sign: 创作者模块）
3. 小程序社区平台  (阅读模块  :heavy_plus_sign: 社区模块 ）

## :tw-1f4ab: 演示图
<table>
<h3 align="center">后台管理平台(更新中）</h3>
    <tr>
        <td><img src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img_3.png"/></td>
        <td><img src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img.png"/></td>
    </tr>
    <tr>
        <td><img src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img_1.png"/></td>
        <td><img src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img_2.png"/></td>
    </tr>
</table>

<table>
<h3 align="center">前台阅读平台(更新中）</h3>
    <tr>
        <td><img style="height:50%" src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img4.png"/></td>
        <td><img style="height:50%" src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img9.jpg"/></td>
    </tr>
      <tr>
        <td><img style="height:50%" src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/shequ.png"/></td>
        <td><img style="height:50%" src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/mine.png"/></td>
    </tr>
</table>
<table>
<h3 align="center">小程序社区平台(更新中）</h3>
   <tr>
        <td><img src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img5.jpg"/></td>
        <td><img style="height:50%" src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img8.jpg"/></td>
         <td><img style="height:50%" src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img7.jpg"/></td>
        <td><img style="height:50%" src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img6.jpg"/></td>
    </tr>
</table>

# :tw-1f4cf: 准备工作

    JDK >= 1.8 (推荐1.8版本)
    Mysql >= 5.7.0 (推荐5.7版本)
    Redis >= 3.0
    Maven >= 3.0
    Node >= 12

# :tw-1f4cb: 运行流程

1. 把仓库 v2.0 分支 clone下来</br>
   `git clone -b v2.0 https://gitee.com/cxq21/book.git`(指定v2.0分支)
   或fork到你自己的仓库

2. 使用idea打开项目，并加载Maven依赖

3. 创建数据库sq_book并依次导入数据脚本
    1. sq_book.sql
   
4. 将 assets 文件夹下的 shiqi 文件夹复制到 D 盘下 （该路径可以在 application.yml 中的 ruoyi.profile 修改） 

4. 启动 redis 服务 （redis免安装版链接：https://pan.baidu.com/s/1MYmNxiY8JIOuXjVr0W_-5A 提取码：1234）

5. 配置 application.yml 里的 redis 信息，和 application-druid.yml 里的数据库信息

6. 启动项目，启动成功后访问 http://localhost:8080/

7. 再用idea单独打开sq-ui 执行</br>

   `npm install`

   `npm run dev`

8. 访问 http://localhost:80 默认账户/密码 admin/admin123

# :tw-1f4cb: 小程序运行流程

### 准备工作

如果使用小程序项目，在此基础上希望得到最大化的效率工具支持，那么HBuilderX是你的首选。

1、首先访问HBuilderX的官网网址：(https://www.dcloud.io/hbuilderx.html (opens new window))

2、打开上面的HBuilderX下载网址后点击页面上download，在弹出的对话框里选择适合自己电脑的HBuilderX版本下载。

3、在Windows10环境下下载后的文件是一个压缩的.zip文件，解压后双击运行HBuilderX.exe即可启动编辑器。

### 运行系统

`前置条件，需要先启动后端。`

1、点击HBuilderx，菜单 文件 -> 打开目录，然后选择sq-app项目，点击选择文件夹按钮，即可成功导入。

2、点击HBuilderx，菜单 运行 -> 运行到浏览器，然后选择浏览器类型，即可在浏览器里面访问。(也可小程序运行，自行百度)

3、运行成功可以通过(http://localhost:9090 (opens new window))访问，出现如下图页面表示成功运行。

<div align="center">
<img src="https://gitee.com/cxq21/book/raw/v2.0/assets/img/img5.jpg"  height="330" width="190"/>
</div>

#### :tw-1f525: 最后

**记得给我一个星星喔，星星是我开源的动力，有技术上的问题欢迎进群讨论**

##### :tw-1f528: 有技术上的问题欢迎进群讨论（群里定期发放红包福利）

点击链接加入群聊【LIEFox技术交流群】：https://jq.qq.com/?_wv=1027&k=XKRmblB9

哔站同步教程：https://www.bilibili.com/video/bv1Tr4y1q7Xx
