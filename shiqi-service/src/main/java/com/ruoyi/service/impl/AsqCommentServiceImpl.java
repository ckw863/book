package com.ruoyi.service.impl;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.domain.AsqCommentTree;
import com.ruoyi.system.mapper.SysUserMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.mapper.AsqCommentMapper;
import com.ruoyi.domain.AsqComment;
import com.ruoyi.service.IAsqCommentService;

/**
 * 评论Service业务层处理
 *
 * @author zjh
 * @date 2022-10-27
 */
@Service
public class AsqCommentServiceImpl implements IAsqCommentService {
    @Autowired
    private AsqCommentMapper asqCommentMapper;
    @Autowired
    private SysUserMapper userMapper;

    /**
     * 查询评论
     *
     * @param commentId 评论主键
     * @return 评论
     */
    @Override
    public AsqComment selectAsqCommentByCommentId(Long commentId) {
        return asqCommentMapper.selectAsqCommentByCommentId(commentId);
    }

    /**
     * 查询评论列表
     *
     * @param asqComment 评论
     * @return 评论
     */
    @Override
    public List<AsqCommentTree> selectAsqCommentList(AsqComment asqComment) {

        List<AsqComment> asqComments = asqCommentMapper.selectAsqCommentList(asqComment);

        ArrayList<AsqCommentTree> asqCommentTrees = new ArrayList<>();

        for (AsqComment comment : asqComments) {
            if (comment.getCommentParentId() == 0) {
                AsqCommentTree asqCommentTree = new AsqCommentTree();
                asqCommentTree.setSysUser(userMapper.selectUserById(comment.getUserId()));
                BeanUtils.copyProperties(comment, asqCommentTree);
                asqCommentTrees.add(asqCommentTree);
            }
        }

        for (AsqCommentTree asqCommentTree : asqCommentTrees) {
            add(asqCommentTree, asqComments);
        }

        return asqCommentTrees;
    }

    private void add(AsqCommentTree rootViewComment, List<AsqComment> commentList) {
        for (AsqComment comment : commentList) {
            // 找到匹配的 parentId
            if (rootViewComment.getCommentId().equals(comment.getCommentParentId())) {
                AsqCommentTree viewComment = new AsqCommentTree();
                viewComment.setSysUser(userMapper.selectUserById(comment.getUserId()));
                BeanUtils.copyProperties(comment, viewComment);
                rootViewComment.getChildren().add(viewComment);
                //递归调用
                add(viewComment, commentList);
            }
        }
    }

    /**
     * 新增评论
     *
     * @param asqComment 评论
     * @return 结果
     */
    @Override
    public int insertAsqComment(AsqComment asqComment) {
        return asqCommentMapper.insertAsqComment(asqComment);
    }

    /**
     * 修改评论
     *
     * @param asqComment 评论
     * @return 结果
     */
    @Override
    public int updateAsqComment(AsqComment asqComment) {
        return asqCommentMapper.updateAsqComment(asqComment);
    }

    /**
     * 批量删除评论
     *
     * @param commentIds 需要删除的评论主键
     * @return 结果
     */
    @Override
    public int deleteAsqCommentByCommentIds(Long[] commentIds) {
        return asqCommentMapper.deleteAsqCommentByCommentIds(commentIds);
    }

    /**
     * 删除评论信息
     *
     * @param commentId 评论主键
     * @return 结果
     */
    @Override
    public int deleteAsqCommentByCommentId(Long commentId) {
        return asqCommentMapper.deleteAsqCommentByCommentId(commentId);
    }
}
