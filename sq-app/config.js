// 应用全局配置
module.exports = {
  baseUrl: 'http://localhost:8080',
  // 应用信息
  appInfo: {
    // 应用名称
    name: "拾柒爱阅读-app",
    // 应用版本
    version: "1.0.0",
    // 应用logo
    logo: "/static/logo.png",
    // 官方网站
    site_url: "http://拾柒爱阅读.vip",
    // 政策协议
    agreements: [{
        title: "隐私政策",
        url: "https://拾柒爱阅读.vip/protocol.html"
      },
      {
        title: "用户服务协议",
        url: "https://拾柒爱阅读.vip/protocol.html"
      }
    ]
  }
}
